FROM node:12-alpine

# Create app directory
WORKDIR /usr/src/app

# Copy dependencies files and install
COPY package.json yarn.lock ./
RUN yarn install
RUN npm install -g pm2

# Copy sources files
COPY . .

# Build app
RUN yarn build

# Remove unusual files for running
RUN rm -rf ./src ./public

# Expose app port
#EXPOSE 3006

CMD ["pm2-runtime", "index.js"]