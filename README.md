===================================

RUNNING THE APPLICATION AND DEPLOYMENT

===================================

STEP 1: There will be three environment: Dev, Stating and Prod.
Each environment will have their own .env files to be used respectively, hence,
we need to run the following npm/yarn commands:

DEV: npm run start:dev

STAGING: npm run start:staging

PROD: npm run start:prod

Therefore, we can use (as far as I know) the following command to be added to the Dockerfile for each repository/branch

DEV: yarn build:dev

STAGING: yarn build:staging

PROD: yarn build:prod

STEP 2: On the mongoDB, run the following command for each instances on each environment. The command was send via personal message as it contains sensitive information
