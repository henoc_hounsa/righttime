import React, { Suspense, useState, useContext } from "react";
import {
  Redirect,
  Switch,
  Route,
  useHistory,
  useLocation,
} from "react-router-dom";
import { connect } from "react-redux";
import Topnavbar from "../Topnavbar/Topnavbar";
import axios from "axios";
import apiUrl from "../../datas/apiUrl";
import { TOP_NAVBAR_HEIGHT, steps, routes } from "./_constants";
import { withTranslation } from "react-i18next";
import { ContextLayout, Layout } from "./layouts/Layout";
import AgencySearch from "./pages/search-agency";
import AgencyListing from "./pages/agency-listing";
import AgencyService from "./components/agency-service";
import ServiceProvider from "./components/service-provider";
import Scheduler from "./components/scheduler";
import TimePicker from "./components/time-picker";
import UserInfos from "./components/user-infos";
import Confirmation from "./components/confirmation";
import moment from "moment";
import { toast,ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';


const RouteConfig = ({ component: Component, fullLayout, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => {
        return (
          <ContextLayout.Consumer>
            {(context) => {
              let LayoutTag =
                fullLayout === true
                  ? context.fullLayout
                  : context.bookingLayout;
              return (
                <LayoutTag {...rest} {...props}>
                  <Component {...props} {...rest} />
                </LayoutTag>
              );
            }}
          </ContextLayout.Consumer>
        );
      }}
    />
  );
};
const mapStateToProps = (state) => {
  return { ...state };
};
const ContentRoute = connect(mapStateToProps)(RouteConfig);

function Appointment(props) {
  let history = useHistory();
  let location = useLocation();
  const appContext = useContext(ContextLayout);
  const [disabledNext, setDisabledNext] = useState(true);
  const [disabledPrev, setDisabledPrev] = useState(false);

/**
 *   React.useEffect(() => {
    if (location.pathname == "/booking") {
      setDisabledPrev(true);
    }
  });
 */

  const customFlow = {
    "/booking": 0,
    "/providers": parseInt(localStorage.getItem("provider_id")),
    "/schedule": parseInt(localStorage.getItem("calendar_id")),
    "/pick-time": 3,
    "/user-infos": 4,
    "/confirm": 5,
  };
  const navigate = (to, data) => {
    history.push({
      pathname: to,
      state: data,
    });
  };
  const navigateToNext = () => {
    const currentPathname = location.pathname;
    if (currentPathname == "/user-infos") {
      submitAppointment();
      return;
    }
    let currentPathnameId = customFlow[currentPathname];
    let nextPathnameId = parseInt(currentPathnameId) + 1;
    let nextRoute = Object.keys(customFlow).find(
      (key) => customFlow[key] == nextPathnameId
    );
    history.push(nextRoute);
    setDisabledPrev(false);
    setDisabledNext(true);
  };

  const submitAppointment = () => {
    setDisabledNext(true);
    const req = {};
    req.schedule = appContext.bookingData.schedule;
    req.xpUserAlias = appContext.bookingData.alias || "";
    req.companyName = appContext.bookingData.companyName;
    req.accountUrl = appContext.bookingData.schedule.url;
    req.service = appContext.currentService;
    req.locale = appContext.configs.lang;
    req.emailAddress = appContext.Email || "";
    req.firstName = appContext.Firstname || "";
    req.lastName = appContext.Lastname || "";
    req.phoneNumber = appContext.Phone || "";
    req.slots = [];
    req.timeSlot = `${appContext.slot.breakTimeStart} - ${appContext.slot.breakTimeEnd}`;
    req.agent = appContext.provider && appContext.provider.userId;
    req.agentFirstName = appContext.provider && appContext.provider.firstname;
    req.agentLastName = appContext.provider && appContext.provider.lastname;
    req.agentEmailAddress = appContext.provider && appContext.provider.email;
    req.appointmentDateRequest =
      appContext.provider && moment(appContext.appointmantDate).toISOString();
    req.appointmentForm =
      appContext.bookingData &&
      appContext.bookingData.schedule &&
      appContext.bookingData.schedule.customDisplayFields
        ? appContext.bookingData.schedule.customDisplayFields.map((x) => {
            let y = { ...x };
            if (appContext[x["en"]]) {
              y.text = appContext[x["en"]];
            }
            return y;
          })
        : [];
    axios
      .post(apiUrl.serverUrl + apiUrl.book,req, apiUrl.config)
      .then(({ data }) => {
        const currentPathname = location.pathname;
        let currentPathnameId = customFlow[currentPathname];
        let nextPathnameId = parseInt(currentPathnameId) + 1;
        let nextRoute = Object.keys(customFlow).find(
          (key) => customFlow[key] == nextPathnameId
        );
        history.push(nextRoute);
        setDisabledPrev(false);
        setDisabledNext(true);
      })
      .catch(console.log);

  };

  const navigateToPrevious = () => {
    const currentPathname = location.pathname;
    let currentPathnameId = customFlow[currentPathname];
    let previousPathId =
      currentPathnameId && currentPathnameId > 0
        ? parseInt(currentPathnameId) - 1
        : 0;
    let previousRoute = Object.keys(customFlow).find(
      (key) => customFlow[key] == previousPathId
    );
    history.push(previousRoute);
    if (currentPathnameId == 0) {
      //setDisabledPrev(true);
      history.push('/locations');
    }
  };

  return (
    <Switch>
      {<Redirect exact from="/" to="/home" />}
      <ContentRoute
        path="/home"
        fullLayout
        component={AgencySearch}
        navigateToNext={navigateToNext}
        navigateToPrevious={navigateToPrevious}
        disabledNext={disabledNext}
        disabledPrev={disabledPrev}
        setDisabledNext={setDisabledNext}
        setDisabledPrev={setDisabledPrev}
        {...props}
      />
      <ContentRoute
        path="/locations"
        navigate={navigate}
        fullLayout
        component={AgencyListing}
        navigateToNext={navigateToNext}
        navigateToPrevious={navigateToPrevious}
        disabledNext={disabledNext}
        disabledPrev={disabledPrev}
        setDisabledNext={setDisabledNext}
        setDisabledPrev={setDisabledPrev}
        {...props}
      />
      <ContentRoute
        path="/booking"
        navigate={navigate}
        component={AgencyService}
        navigateToNext={navigateToNext}
        navigateToPrevious={navigateToPrevious}
        disabledNext={disabledNext}
        disabledPrev={disabledPrev}
        setDisabledNext={setDisabledNext}
        setDisabledPrev={setDisabledPrev}
        {...props}
      />

      <ContentRoute
        path="/providers"
        navigate={navigate}
        component={ServiceProvider}
        navigateToNext={navigateToNext}
        navigateToPrevious={navigateToPrevious}
        disabledNext={disabledNext}
        disabledPrev={disabledPrev}
        setDisabledNext={setDisabledNext}
        setDisabledPrev={setDisabledPrev}
        {...props}
      />
      <ContentRoute
        path="/schedule"
        navigate={navigate}
        component={Scheduler}
        navigateToNext={navigateToNext}
        navigateToPrevious={navigateToPrevious}
        disabledNext={disabledNext}
        disabledPrev={disabledPrev}
        setDisabledNext={setDisabledNext}
        setDisabledPrev={setDisabledPrev}
        {...props}
      />
      <ContentRoute
        path="/pick-time"
        navigate={navigate}
        component={TimePicker}
        navigateToNext={navigateToNext}
        navigateToPrevious={navigateToPrevious}
        disabledNext={disabledNext}
        disabledPrev={disabledPrev}
        setDisabledNext={setDisabledNext}
        setDisabledPrev={setDisabledPrev}
        {...props}
      />
      <ContentRoute
        path="/user-infos"
        navigate={navigate}
        component={UserInfos}
        navigateToNext={navigateToNext}
        navigateToPrevious={navigateToPrevious}
        disabledNext={disabledNext}
        disabledPrev={disabledPrev}
        setDisabledNext={setDisabledNext}
        setDisabledPrev={setDisabledPrev}
        {...props}
      />
      <Route path="/confirm" component={Confirmation} {...props} />
      <Redirect to="error/404" />
    </Switch>
  );
}
export default Appointment;
