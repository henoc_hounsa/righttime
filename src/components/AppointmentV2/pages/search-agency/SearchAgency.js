import React, { useContext } from "react";
import styled from "styled-components";
import bgWrapper from "../../assets/teacher.jpg";
import RightComLogo from "../../assets/RightCom.svg";
import axios from "axios";
import { TOP_NAVBAR_HEIGHT } from "../../_constants";
import { useHistory } from "react-router-dom";
import { ContextLayout } from "../../layouts/Layout";
import apiUrl from "../../../../datas/apiUrl";
import { Dropdown, Row, Col } from "react-bootstrap";
import { CustomMenu, CustomToggle } from "../../components/custom-toggle";
import { GrMapLocation, GrLocation } from "react-icons/gr";
import _ from "lodash";

const groupBy = (array, key) => {
  return array.reduce((result, currentValue) => {
    (result[currentValue[key]] = result[currentValue[key]] || []).push(
      currentValue
    );
    return result;
  }, {});
};

function SearchAgency(props) {
  const [locations, setLocations] = React.useState([]);
  const [data, setData] = React.useState([]);
  const [isLoading, setLoading] = React.useState(true);
  const appContext = useContext(ContextLayout);
  const history = useHistory();
  const definedLan = localStorage.getItem("rightTime_lang");
  const brandLogo = apiUrl.xpCdn.replace(
    "{{imagepath}}",
    appContext.bookingData &&
      appContext.bookingData.schedule &&
      appContext.bookingData.schedule.customDisplaySettings
      ? appContext.bookingData.schedule.customDisplaySettings.brandImagePath
      : ""
  );

  React.useEffect(() => {
    getLocations();
    setTimeout(() => {
      if (isLoading == true) {
        setLoading(false);
      }
    }, 1500);
  }, []);
  const getLocations = () => {
    axios
      .get(`${apiUrl.serverUrl}/${apiUrl.getLocations}`, apiUrl.config)
      .then(async (response) => {
        setData(response);
        let groupByCountry = groupBy(
          response.data.filter(
            (x) =>
              x.country !== "" && x.country !== undefined && x.country !== null
          ),
          "country"
        );
        let formatedLocations = await Promise.all(
          Object.keys(groupByCountry).map(async (country) => {
            let { data } = await axios.get(
              `${apiUrl.getCountryDetails}/${country}`
            );
            return { apiData: groupByCountry[country], ...data };
          })
        );
        setLocations(formatedLocations);
      })
      .catch((error) => {
        console.log(error);
        let errorMessage = error.name + ": " + error.message;
        if (error.response != null) {
          errorMessage = "Error: " + error.response.data.message;
        }
      });
  };
  const handleSearch = (e) => {
    console.log("PREPARE___");
    axios
      .get(`${apiUrl.serverUrl}/${apiUrl.getLocations}`, apiUrl.config)
      .then((response) => {
        console.log(response.data);
      })
      .catch((error) => {
        console.log(error);
        let errorMessage = error.name + ": " + error.message;
        if (error.response != null) {
          errorMessage = "Error: " + error.response.data.message;
        }
      });
  };

  const navigateToResult = () => {
    history.push({
      pathname: "/locations",
      state: {
        currentCountry: appContext.currentCountry,
        currentCity: appContext.currentCity,
        locations: locations,
      },
    });
  };
  const instructionText = {
    en: `To find a convenient {{companyName}} office, please enter your preferred location in the box below and click on the ‘SEARCH’.`,
    fr: `Pour trouver un bureau {{companyName}}, veuillez entrer votre emplacement préféré dans la case ci-dessous et cliquez sur «RECHERCHER».`,
  };

 
  return (
    <ContextLayout.Consumer>
      {(context) => {
        return (
          <Container>
            <SearchBarbContainer>
              <RoundHeadingContainer bgImage={brandLogo}>
                <HeadingBox>
                  <h2>
                    {appContext.bookingData &&
                      appContext.bookingData !== null &&
                      appContext.bookingData.schedule.customCompanyDetails[
                        definedLan
                      ].headingText}
                  </h2>
                  <p>
                    {" "}
                    {appContext.bookingData &&
                      appContext.bookingData !== null &&
                      appContext.bookingData.schedule.customCompanyDetails[
                        definedLan
                      ].subHeadingText}
                  </p>
                </HeadingBox>
              </RoundHeadingContainer>
              <MainContent>
                <Content>
                  <h2>
                    {context &&
                      context.bookingData &&
                      context.bookingData.companyName &&
                      context.bookingData.companyName.toUpperCase()}
                  </h2>
                  <p>
                    {instructionText[context.configs.lang].replace(
                      "{{companyName}}",
                      (context &&
                        context.bookingData &&
                        context.bookingData.companyName) ||
                        ""
                    )}
                  </p>
                  <SearchBarV2>
                    <Row className="m-0 w-100 h-100 pl-1 pr-1">
                      <Col
                        className="d-flex align-items-center w-100"
                        style={{ borderRight: "1px solid #808080" }}
                        md={5}
                        xs={5}
                        lg={5}
                      >
                        <Dropdown className="w-100">
                          <Dropdown.Toggle as={CustomToggle}>
                            <GrMapLocation
                              className="mr-1"
                              style={{ opacity: 0.4 }}
                            />{" "}
                            {context.currentCountry &&
                            context.currentCountry !== null
                              ? context.currentCountry.name
                              : "Select country"}
                          </Dropdown.Toggle>

                          <Dropdown.Menu className="mt-2" as={CustomMenu}>
                            {locations &&
                              locations.length > 0 &&
                              locations.map((location, k) => {
                                return (
                                  <Dropdown.Item
                                    onClick={() =>
                                      context.changeCountry(location)
                                    }
                                    key={k}
                                    eventKey={k + 1}
                                  >
                                    <div className={"pt-2 pb-2"}>
                                      {location.name || ""}{" "}
                                    </div>
                                  </Dropdown.Item>
                                );
                              })}
                            {isLoading == false &&
                              (!locations || locations.length < 1) && (
                                <span className="text-center w-100 d-flex justify-content-center">
                                  {definedLan == "fr"
                                    ? "Aucune donnée"
                                    : "No data"}
                                </span>
                              )}
                          </Dropdown.Menu>
                        </Dropdown>
                      </Col>
                      <Col
                        className="d-flex align-items-center"
                        xs={5}
                        md={5}
                        lg={5}
                      >
                        <Dropdown className="w-100">
                          <Dropdown.Toggle className="w-100" as={CustomToggle}>
                            <GrLocation
                              className="mr-1"
                              style={{ opacity: 0.4 }}
                            />{" "}
                            {context.currentCity && context.currentCity !== null
                              ? context.currentCity
                              : "Select city"}
                          </Dropdown.Toggle>

                          <Dropdown.Menu className="mt-2" as={CustomMenu}>
                            {context.cities &&
                              context.cities.length > 0 &&
                              context.cities.map((city, k) => {
                                return (
                                  <Dropdown.Item
                                    onClick={() => {
                                      localStorage.setItem(
                                        "curr_city_id",
                                        city
                                      );
                                      context.changeCity(city);
                                    }}
                                    key={k}
                                    eventKey={k + 1}
                                  >
                                    <div className={"pt-2 pb-2"}>
                                      {city}
                                    </div>
                                  </Dropdown.Item>
                                );
                              })}
                            {/**
                               * {(!context.cities ||
                              (context.cities && context.cities.length < 1)) &&
                              data && data.data && data.data.length > 0 &&
                              data.data.map((city, k) => {
                                return (
                                  <Dropdown.Item
                                    onClick={() => {
                                      localStorage.setItem(
                                        "curr_city_id",
                                        city.id
                                      );
                                      context.changeCity({ name: city.city });
                                    }}
                                    key={k}
                                    eventKey={k + 1}
                                  >
                                    <div className={"pt-2 pb-2"}>
                                      {city.city}
                                    </div>
                                  </Dropdown.Item>
                                );
                              })}
                               */}
                          </Dropdown.Menu>
                        </Dropdown>
                      </Col>
                      <Col
                        className="d-flex align-items-center p-0 m-0 pr-2"
                        md={2}
                        xs={2}
                        lg={2}
                      >
                        <SearchBtnV2 onClick={navigateToResult}>
                          Search
                        </SearchBtnV2>
                      </Col>
                    </Row>
                  </SearchBarV2>
                  {/**
                 * <SearchBar>
                <Input type="text" onChange={handleSearch} placeholder="Enter a location" />
                <Link to="/locations" style={{textDecoration:'none'}}>
                  <SearchBtn>SEARCH</SearchBtn>
                </Link>
              </SearchBar>
                 */}
                </Content>
              </MainContent>
            </SearchBarbContainer>
            <Wrapper></Wrapper>
            <FooterLogo>
              <span>
                A product of <img className="ml-1" src={RightComLogo} />
              </span>
            </FooterLogo>
          </Container>
        );
      }}
    </ContextLayout.Consumer>
  );
}

export default SearchAgency;

const Container = styled.div`
  padding: 0;
  margin: 0;
  display: flex;
  height: 100vh;
  width: 100%;
  overflow: hidden;
  @media (max-width: 600px) {
    display: flex;
    justify-content: center;
  }
`;

const SearchBarbContainer = styled.div`
  width: 70%;
  height: 100%;
  background: #0089e1;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0;
  margin: 0;
  padding-top: ${TOP_NAVBAR_HEIGHT + 10}px;
  @media (max-width: 1320px) {
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    padding-bottom: 60px;
    padding-top: 60px;
  }
  @media (max-width: 360px) {
    justify-content: space-around;
  }
`;

const SearchBar = styled.div`
  display: flex;
  @media (max-width: 600px) {
    flex-direction: column;
    align-items: center;
  }
`;

const SearchBarV2 = styled.div`
  display: flex;
  height: 60px;
  align-items: center;
  background: #ffffff 0% 0% no-repeat padding-box;
  box-shadow: 0px 3px 10px #00000024;
  font: normal normal normal 14px/14px Source Sans Pro;
  letter-spacing: 0px;
  color: #373737;
  border-radius: 5px;
`;

const SearchBtn = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background: transparent;
  height: 50px;
  width: 120px;
  border: 1px solid #ffffff;
  border-radius: 5px;
  opacity: 1;
  margin-left: 4px;
  font: normal normal bold 12px/17px Source Sans Pro;
  letter-spacing: 0px;
  color: #ffffff;
  text-transform: uppercase;
  cursor: pointer;
  @media (max-width: 600px) {
    margin-top: 5px;
  }
`;
const SearchBtnV2 = styled.button`
  height: 40px;
  width: 100%;
  background: #0089e1 0% 0% no-repeat padding-box;
  border-radius: 5px;
  outline: none;
  border: none;
  display: flex;
  align-items: center;
  justify-content: center;
  font: normal normal bold 12px/17px Source Sans Pro;
  letter-spacing: 0px;
  color: #ffffff;
  text-transform: uppercase;
`;

const Content = styled.div`
  font: normal normal normal 30px/40px Manrope3;
  letter-spacing: 0px;
  width: 438px;

  h2 {
    font: normal normal normal 30px/40px Manrope3;
    letter-spacing: 0px;
    font-size: 25px;
  }
  p {
    font-size: 14px;
    letter-spacing: 0px;
    text-align: left;
    font: normal normal normal 14px/22px Source Sans Pro;
    opacity: 1;
    @media (max-width: 600px) {
      text-align: center;
    }
  }

  @media (max-width: 1320px) {
    margin-top: 124px;
    h2 {
      text-align: center;
    }
    p {
      font-size: 14px;
      text-align: center;
    }
  }
  @media (max-width: 760px) {
    width: 438px;
    text-align: center;

    h2 {
      font-size: 18px;
      display: none;
    }
    p {
      font-size: 14px;
    }
  }
  @media (max-width: 600px) {
    width: 100%;
    padding: 10px;
    h2 {
      font-size: 20px;
      display: block;
      justify-content: center;
    }
    p {
      font-size: 10px;
    }
  }
`;
const MainContent = styled.div`
  font: normal normal normal 30px/40px Manrope3;
  position: relative;
  letter-spacing: 0px;
  color: #ffffff;
  display: flex;
  align-items: center;
  @media (max-width: 760px) {
    flex-direction: column;
    align-items: center;
  }
`;

const RoundHeadingContainer = styled.div`
  border-radius: 50%;
  width: 407px;
  height: 396px;
  position: absolute;
  right: 16%;
  background: transparent url(${(props) => props.bgImage}) no-repeat center;
  background-size: cover;
  @media (max-width: 1320px) {
    right: 0;
    position: relative;
  }
  @media (max-width: 360px) {
    width: 249px;
    height: 242px;
  }
`;

const HeadingBox = styled.div`
  width: 293px;
  height: 84px;
  background: #ffffff 0% 0% no-repeat padding-box;
  box-shadow: 0px 3px 6px #00000029;
  border-radius: 5px;
  opacity: 0.88;
  text-align: left;
  padding: 15px;
  font: normal normal 600 24px/21px Source Sans Pro;
  letter-spacing: 0px;
  color: #0089e1;
  position: absolute;
  right: 14%;
  bottom: -6%;
  h2 {
    font: normal normal 600 18px/21px Source Sans Pro;
    letter-spacing: 0px;
    color: #0089e1;
    opacity: 1;
  }
  p {
    font: normal normal normal 18px/22px Source Sans Pro;
    letter-spacing: 0px;
    color: #0089e1;
    opacity: 1;
  }
  @media (max-width: 360px) {
    width: 249px;
    height: 72px;
    bottom: -5%;
  }
`;

const Input = styled.input`
  background: #ffffff 0% 0% no-repeat padding-box;
  height: 50px;
  border: 1px solid #e4e4e4;
  border-radius: 5px;
  opacity: 1;
  width: 319px;
  padding-left: 10px;
  font: normal normal normal 14px/22px Source Sans Pro;
  letter-spacing: 0px;
  color: #808080;
`;

const Wrapper = styled.div`
  width: 30%;
  height: 100%;
  background: transparent url(${bgWrapper}) center no-repeat;
  background-size: cover;
  background-position: center;
  mix-blend-mode: luminosity;
  opacity: 0.1;

  @media (max-width: 1320px) {
    display: none;
  }
`;

const FooterLogo = styled.span`
  bottom: 20px;
  right: 20px;
  opacity: 1;
  position: absolute;
  width: 200px;
  font: normal normal normal 11px/25px Source Sans Pro;
  letter-spacing: 0px;
  color: #6c6c6c;

  @media (max-width: 760px) {
    background: #fafafa 0% 0% no-repeat padding-box;
    border: 0.75px solid #e4e4e4;
    opacity: 1;
    display: none;
    height: 84px;
    width: 100%;
    bottom: 0;
    right: 0;
    position: "";
    justify-content: flex-end;
    align-items: flex-end;
    padding-right: 25px;
    padding-bottom: 25px;
  }
  @media (max-width: 600px) {
    background: #fafafa 0% 0% no-repeat padding-box;
    border: 0.75px solid #e4e4e4;
    opacity: 1;
    display: flex;
    height: 84px;
    width: 100%;
    bottom: 0;
    right: 0;
    position: absolute;
    justify-content: flex-end;
    align-items: flex-end;
    padding-right: 25px;
    padding-bottom: 25px;
    span: {
      position: absolute;
      bottom: 0;
      right: 0;
    }
  }
`;
