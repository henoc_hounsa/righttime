import React, { useContext } from "react";
import styled from "styled-components";
import bgWrapper from "../../assets/teacher.jpg";
import { TOP_NAVBAR_HEIGHT } from "../../_constants";
import RightComLogo from "../../assets/RightCom.svg";
import { useLocation } from "react-router-dom";
import { ContextLayout, Layout } from "../../layouts/Layout";
import { DualRing } from "react-awesome-spinners";
import { CustomMenu, CustomToggle } from "../../components/custom-toggle";
import { GrMapLocation, GrLocation } from "react-icons/gr";
import { Dropdown, Row, Col, Button } from "react-bootstrap";
import apiUrl from "../../../../datas/apiUrl";
import axios from "axios";

const groupBy = (array, key) => {
  return array.reduce((result, currentValue) => {
    (result[currentValue[key]] = result[currentValue[key]] || []).push(
      currentValue
    );
    return result;
  }, {});
};

function shortedText(text) {
  return (text =
    "" || !/\S/.test(text)
      ? text
      : `${text.substr(0, 50)} ${text.length > 50 ? "..." : ""}`);
}


function AgencyListing(props) {
  const location = useLocation();
  console.log("location.state", location.state)
  const appContext = useContext(ContextLayout);
  const [locations, setLocations] = React.useState([]);
  const [isLoading, setLoading] = React.useState(true);
  const [data, setData] = React.useState([]);

  React.useEffect(() => {
    getLocations()
    if(location.state &&
      location.state.currentCountry &&
      location.state.currentCountry.apiData &&  location.state.currentCountry.apiData.length > 0){
       // console.log("currentCity", appContext.currentCity,location.state.currentCountry.apiData)
          setLocations(location.state.currentCountry.apiData.filter(_location => location.state.currentCity !== null ? _location.city ==  location.state.currentCity : true))
      }
    setTimeout(() => {
      if (isLoading == true) {
        setLoading(false);
      }
    }, 1500);
  }, []);

  const getLocations = () => {

    axios
      .get(`${apiUrl.serverUrl}/${apiUrl.getLocations}`, apiUrl.config)
      .then(async (response) => {
        //setData(response);
        //console.log("locations", response)
        let groupByCountry = groupBy(
          response.data.filter(
            (x) =>
              x.country !== "" && x.country !== undefined && x.country !== null
          ),
          "country"
        );
        let formatedLocations = await Promise.all(
          Object.keys(groupByCountry).map(async (country) => {
            let { data } = await axios.get(
              `${apiUrl.getCountryDetails}/${country}`
            );
            return { apiData: groupByCountry[country], ...data };
          })
        );
        setData(formatedLocations);
      })
      .catch((error) => {
       
        let errorMessage = error.name + ": " + error.message;
        if (error.response != null) {
          errorMessage = "Error: " + error.response.data.message;
        }
      });
  };

  const handleSearch = (e) => {
    setLocations(
      locations.filter((x) => {
        return (
          x.businessLocationName.search(new RegExp(e.target.value, "i")) >= 0 &&
          x.country == appContext.currentCountryCode
        );
      })
    );
  };
  const navigateToResult = (location) => {
    //localStorage.setItem('selected_location', location.id)
    props.navigate(`/booking`, location);
  };

  const submitSearch = () => {
    if (appContext.currentCountryCode !== null) {
      setLocations(
        appContext.currentCountry &&
          appContext.currentCountry.apiData &&
          appContext.currentCountry.apiData.filter(
            (x) => x.country == appContext.currentCountryCode && appContext.currentCity !== null ? x.city == appContext.currentCity : true
          )
      );
    } else {
      setLocations([]);
    }
  };


  return (
    <ContextLayout.Consumer>
      {(context) => {
        return (
          <Container className="w-100">
            <div className="w-100 d-flex p-0 p-md-4 justify-content-center">
              <SearchBarV2>
                <Row className="m-0 w-100 h-100 pl-1 pr-1">
                  <Col
                    className="d-flex align-items-center w-100"
                    style={{ borderRight: "1px solid #e4e4e4" }}
                    md={5}
                    xs={5}
                    lg={5}
                  >
                    <Dropdown className="w-100">
                      <Dropdown.Toggle as={CustomToggle}>
                        <GrMapLocation
                          className="mr-1"
                          style={{ opacity: 0.4 }}
                        />{" "}
                        {context.currentCountry &&
                        context.currentCountry !== null
                          ? context.currentCountry.name
                          : "Select country"}
                      </Dropdown.Toggle>

                      <Dropdown.Menu className="mt-2" as={CustomMenu}>
                        {data &&
                          Array.isArray(data) &&
                          data.length > 0 &&
                          data.map((location, k) => {
                            return (
                              <Dropdown.Item
                                onClick={() => context.changeCountry(location)}
                                key={k}
                                eventKey={k + 1}
                              >
                                <div className={"pt-2 pb-2"}>
                                  {location.name || ""}{" "}
                                </div>
                              </Dropdown.Item>
                            );
                          })}
                      </Dropdown.Menu>
                    </Dropdown>
                  </Col>
                  <Col
                    className="d-flex align-items-center"
                    xs={5}
                    md={5}
                    lg={5}
                  >
                    <Dropdown className="w-100">
                      <Dropdown.Toggle className="w-100" as={CustomToggle}>
                        <GrLocation className="mr-1" style={{ opacity: 0.4 }} />{" "}
                        {context.currentCity && context.currentCity !== null
                          ? context.currentCity
                          : "Select city"}
                      </Dropdown.Toggle>

                      <Dropdown.Menu className="mt-2" as={CustomMenu}>
                        {context.cities &&
                          context.cities.length > 0 &&
                          context.cities.map((city, k) => {
                            return (
                              <Dropdown.Item
                                onClick={() => {
                                  //localStorage.setItem("curr_city_id", city.id);
                                  //context.changeCity({ name: city.city,id: city.id });
                                  context.changeCity(city)
                                }}
                                key={k}
                                eventKey={k + 1}
                              >
                                <div className={"pt-2 pb-2"}>{city}</div>
                              </Dropdown.Item>
                            );
                          })}
                      </Dropdown.Menu>
                    </Dropdown>
                  </Col>
                  <Col
                    className="d-flex align-items-center p-0 m-0 pr-2"
                    md={2}
                    xs={2}
                    lg={2}
                  >
                    <SearchBtnV2 onClick={submitSearch}>Search</SearchBtnV2>
                  </Col>
                </Row>
              </SearchBarV2>
             {/** <input onChange={handleSearch} type="text" placeholder="Search" /> */}
            </div>

            <Wrapper></Wrapper>

            <ListContainer className="mt-4">
              <h3 className="mt-4 mb-4">
                {" "}
                {locations && locations.length ? locations.length : 0}{" "}
                {locations && locations.length > 1
                  ? " locations "
                  : " location "}
                found
              </h3>
              {isLoading == true && (!locations || locations.length < 1) && (
                <DualRing />
              )}

              {locations &&
                locations.length > 0 &&
                locations.map((location, k) => {
                  return (
                    <Item
                      key={k}
                      className="my-2 p-3 pl-4 pr-4  position-relative"
                    >
                      <div
                        onClick={() => {
                          context.changeCurrentLocation(location.id);
                          navigateToResult(location);
                        }}
                      >
                        <div className="d-flex justify-content-between align-items-center">
                          <h4>{shortedText(location.businessLocationName || "")}</h4>
                          {/*<div className="rs-search-result-location-item-time-slot d-flex align-items-center">*/}
                          {/*  <OpenMark className="mr-2"></OpenMark>{" "}*/}
                          {/*  <span>Open until</span>*/}
                          {/*  <span className="rs-search-result-location-item-date-time ml-1">*/}
                          {/*    6:00PM*/}
                          {/*  </span>*/}
                          {/*</div>*/}
                        </div>
                        <span>{location.address || ""}</span>
                      </div>
                      <div
                        className="position-absolute"
                        style={{ right: 15, marginTop: 10 }}
                      >
                        <Button
                          size={"sm"}
                          onClick={() =>
                            window.open(
                              "https://www.google.com/maps/search/" +
                                location.businessLocationName +
                                " " +
                                location.address +
                                " " +
                                location.city +
                                " " +
                                location.country,
                              "_blank"
                            )
                          }
                        >
                          view on map
                        </Button>
                      </div>
                    </Item>
                  );
                })}
            </ListContainer>
            <Footer>
              <div className="pr-4">
                <span className="">A product of</span>{" "}
                <span>
                  <img src={RightComLogo} />
                </span>
              </div>
            </Footer>
          </Container>
        );
      }}
    </ContextLayout.Consumer>
  );
}

export default AgencyListing;

const Container = styled.div`
  padding: 0;
  margin: 0;
  display: flex;
  flex-direction: column;
  height: 100vh;
  overflow: hidden;
  padding-bottom: 85px ;
  input {
    display: none;
    background: #ffffff 0% 0% no-repeat padding-box;
    border: 1px solid #e4e4e4;
    border-radius: 5px;
    width: 563px;
    height: 60px;
    position: absolute;
    top: calc((320px - ${TOP_NAVBAR_HEIGHT}px) / 2);
    font: normal normal normal 20px/22px Source Sans Pro;
    letter-spacing: 0px;
    color: #808080 !important;
    font-weight: 700;
    opacity: 1;
    z-index: 1;
    padding-left: 10px;
    @media (max-width: 600px) {
      width: 90%;
      top: calc((270px - ${TOP_NAVBAR_HEIGHT}px) / 2);
      z-index: initial;
      display: block;
    }
  }
  h3 {
    font: normal normal normal 20px/21px Source Sans Pro;
    letter-spacing: 0px;
    color: #373737;
    text-align: center;
    @media (max-width: 600px) {
      text-align: left;
      padding: 10px;
      letter-spacing: 0px;
      color: #373737;
      font-size:font: normal normal 600 24px/21px Source Sans Pro;
    }
  }
  @media (max-width: 600px) {
    background: #f0f4f6;
    min-height: 100vh;
  }
`;
const SearchBtnV2 = styled.button`
  height: 40px;
  width: 100%;
  background: #0089e1 0% 0% no-repeat padding-box;
  border-radius: 5px;
  outline: none;
  border: none;
  display: flex;
  align-items: center;
  justify-content: center;
  font: normal normal bold 12px/17px Source Sans Pro;
  letter-spacing: 0px;
  color: #ffffff;
  text-transform: uppercase;
`;
const SearchBarV2 = styled.div`
  display: flex;
  height: 70px;
  width: 60%;
  align-items: center;
  background: #ffffff 0% 0% no-repeat padding-box;
  box-shadow: 0px 3px 10px #00000024;
  font: normal normal normal 14px/14px Source Sans Pro;
  letter-spacing: 0px;
  color: #373737;
  opacity: 1;
  border-radius: 5px;
  z-index: 1;
  position: absolute;
  top: calc((320px - ${TOP_NAVBAR_HEIGHT}px) / 2);
  @media (max-width: 600px) {
    width: 80%;
    position: static;
    margin:0;
    padding:0;
    margin-top: 100px;
    top:0;
  }
  @media (max-width: 500px) {
    width: 90%;
  }
`;
const Wrapper = styled.div`
  height: calc(270px - ${TOP_NAVBAR_HEIGHT - 100}px);
  background: transparent url(${bgWrapper}) center no-repeat;
  background-size: cover;
  background-position: center;
  mix-blend-mode: luminosity;
  opacity: 0.1;
  display: flex;
  justify-content: center;

  @media (max-width: 600px) {
    display: none;
  }
`;

const ListContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  overflow-y: scroll;
  height: 100%;
  @media (max-width: 600px) {
   
  }
`;

const Item = styled.div`
  display: flex;
  flex-direction: column;
  align-items: space-between;
  cursor: pointer;
  width: 40%;
  background: #ffffff 0% 0% no-repeat padding-box;
  box-shadow: 0px 3px 8px #0000001a;
  border: 1px solid #e4e4e4;
  border-radius: 5px;
  h4 {
    font: normal normal bold 16px/22px Source Sans Pro;
    letter-spacing: 0px;
    color: #303030;
    font-weight: bold;
  }
  @media (max-width: 760px) {
    width: 85%;
  }
  @media (max-width: 600px) {
    width: 90%;
  }
  &:hover {
    border: 2px solid #0089e1;
  }
`;

const Footer = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  width: 100%;
  background: #fafafa 0% 0% no-repeat padding-box;
  border: 0.75px solid #e4e4e4;
  border-bottom: none;
  display: flex;
  height: 84px;
  position: absolute;
  bottom: 0;
`;

const OpenMark = styled.div`
  width: 10px;
  height: 10px;
  background: #32c832 0% 0% no-repeat padding-box;
  border-radius: 50%;
`;
