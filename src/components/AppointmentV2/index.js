import React from 'react'
import AppointmentV2 from './AppointmentV2'
import { ContextLayout, Layout } from "./layouts/Layout";
function App (props){
   return(
    <Layout>
       <AppointmentV2
           companyLanguage={props.companyLanguage}
           language={props.language}
           handleLanguage={props.handleLanguage}
           companyLogoPath={props.companyLogoPath}
       />
    </Layout>
   )
}

export default App;
