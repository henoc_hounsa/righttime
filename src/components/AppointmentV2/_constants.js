export const steps = {
  0: 'AGENCY_SERVICE',
  1: 'SERVICE_PROVIDER',
  2: 'CALENDAR',
  3: 'TIME_PICKER',
  5: 'USER_INFOS',
  5: 'CONFIRMATION',
}; 

export const match = {
  '/booking':'AGENCY_SERVICE',
  '/providers':'SERVICE_PROVIDER',
  '/schedule':'CALENDAR',
  '/pick-time':'TIME_PICKER',
  '/user-infos':'USER_INFOS',
  '/confirm':'CONFIRMATION'

}
export const labels = {
  '/booking':{
    en:'Select a service',
    fr: 'Choisir un service'
  },
  '/providers':{
    en:'Select a service provider',
    fr: 'Choisir un prestataire'
  },
  '/schedule':{
    en:'Pick an date',
    fr: 'Choisir une date'
  },
  '/pick-time':{
    en:'Pick a time slot ',
    fr: 'Choisir un interval de temps'
  },
  '/user-infos':{
    en:'Enter your details',
    fr: 'Entrer vos informations'
  },
  '/confirm':{
    en:'Confirm',
    fr: 'Confirmer'
  }

}
export const defaultFlow = {
 'AGENCY_SERVICE':0,
 'SERVICE_PROVIDER':1,
 'CALENDAR': 2,
 'TIME_PICKER':3,
 'USER_INFOS':4,
 'CONFIRMATION': 5,
}; 

export const TOP_NAVBAR_HEIGHT = 64

export const routes = {
  AGENCY_SERVICE: '/booking',
  SERVICE_PROVIDER: '/providers',
  CALENDAR: '/schedule',
  TIME_PICKER: '/pick-time',
  USER_INFOS: '/user-infos',
  CONFIRMATION: '/confirm'
}