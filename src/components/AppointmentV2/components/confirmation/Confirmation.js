import React,{useState} from "react";
import styled from "styled-components";
import IconCheck from '../../assets/icon-check.svg'
import {Toast} from 'react-bootstrap'
import {useHistory} from 'react-router-dom'

function Confirmation() {
  const [show, setShow] = useState(true);
  const history = useHistory()

  const definedLan = localStorage.getItem("rightTime_lang")
    ? localStorage.getItem("rightTime_lang")
    : "en";
  React.useEffect(() => {
    setTimeout(()=> history.push('/') , 3000)
  })
  return <Container>

  <Toast onClose={() => setShow(false)} show={show} delay={3000} autohide>

  <Toast.Body className="d-flex"><img src={IconCheck} className="mr-2" /> {definedLan == 'en' ? 'Congratulations! You RightQ services has been imported into RightTime' : 'Félicitations ! Votre rendez-vous a bien été pris en compte' } </Toast.Body>
</Toast>
  </Container>;
}

export default Confirmation;

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100vh;
  border-radius: 5px;
`;
const MessageBox = styled.div`
  width: 753px;
  height: 80px;
  background: #dbf5d6 0% 0% no-repeat padding-box;
  border-radius: 5px;
  text-align: left;
  font: normal normal bold 14px/20px Source Sans Pro;
  letter-spacing: 0px;
  color: #40c620;
  padding: 30px;
`;
