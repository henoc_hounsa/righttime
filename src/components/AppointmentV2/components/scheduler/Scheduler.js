import React, { useState,useContext } from "react";
//import Calendar from '../calendar'
import DayPicker from "react-day-picker";
import "react-day-picker/lib/style.css";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useLocation } from "react-router-dom";
import "./index.css";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Button from "react-bootstrap/Button";
import Loader from "react-loader-spinner";
import { withTranslation } from "react-i18next";
import MomentLocaleUtils from "react-day-picker/moment";
import "moment/locale/en-au";
import Helmet from "react-helmet";
import axios from "axios";
import apiUrl from "../../../../datas/apiUrl";
import { ContextLayout, Layout } from "../../layouts/Layout";
import styled from "styled-components";


let moment = require('moment')

const currentYear = new Date().getFullYear();
const fromMonth = new Date(currentYear, new Date().getMonth());
const toMonth = new Date(currentYear + 10, 11);

function Weekday({ weekday, className, localeUtils, locale }) {
  const weekdayName = localeUtils.formatWeekdayLong(weekday, locale);
  return (
    <div className={className} title={weekdayName}>
      {weekdayName.slice(0, 3)}
    </div>
  );
}

function YearMonthForm({ date, localeUtils, onChange }) {
  const handleChange = function handleChange(e) {
    const { year, month } = e.target.form;
    onChange(new Date(year.value, month.value));
  };

  const month = date.toLocaleString(localStorage.getItem("rightTime_lang"), {
    month: "long",
  });

  return (
    <form className="DayPicker-Caption">
      {month}{" "}
      <span className="day-picker-select-year">{date.getFullYear()}</span>
    </form>
  );
}

const selectedStyle = `.DayPicker-Day--selected{
    background-color: #8FBC8F !important;
    color: white !important;
  }`;

function Scheduler(props) {
  const location = useLocation();
  const [value, onChange] = useState(new Date());
  const [selectedDate, setSelectedDate] = useState(null);
  const [blockedDates, setBlockedDates] = useState([]);
  const [slots, setSlots] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [appointmantDateRequest, setAppointmentDateRequest] = useState(null);
  const [hasErr, setHasErr] = useState(false);
  const [month, setMonth] = useState(null);
  const [timeSlot, setSlot] = useState(null);
  const [isValidatingSlots, setIsValidatingSlots] = useState(false);
  const [monthYear, setMonthYear] = useState(new Date());
 const appContext = useContext(ContextLayout);
  const definedLan = localStorage.getItem("rightTime_lang")
    ? localStorage.getItem("rightTime_lang")
    : "en";

  React.useEffect(() => {
    if (appContext.appointmantDate && appContext.appointmantDate !==null) {
        props.setDisabledNext(false);
        setSelectedDate(appContext.appointmantDate);
      }
    //calendarChange();
  });
  const calendarChange = (day) => {
    var date = new Date();
    var month = day ? day.getMonth() + 1 : date.getMonth() + 1;
    var year = day ? day.getFullYear() : date.getFullYear();
    var appointment = appContext.bookingData;
    setIsValidatingSlots(true);
    setMonthYear(new Date(year, month - 1));
    axios
      .post(
        apiUrl.serverUrl +
          apiUrl.blockedDayGetAll +
          "?month=" +
          ("0" + month).slice(-2) +
          "&year=" +
          year,
        appointment,
        apiUrl.config
      )
      .then((days) => {
        console.log("Got days blocked", days)
        setBlockedDates(days.data);
        setIsValidatingSlots(false);
      })
      .catch((error) => {
        console.log(error);
        let errorMessage = error.name + ": " + error.message;
        if (error.response != null) {
          errorMessage = "Error: " + error.response.data.message;
        }
        toast.error(errorMessage);
        setIsLoading(false);
        setHasErr(true);
        setIsValidatingSlots(false);
      });
  };

  const handleDateSelection = (date) => {
    setSelectedDate(date);
    props.setDisabledNext(false);

   // appContext.setAppointmentDate(date)
    /* props.appointmentDetails.slots = [];
    props.appointmentDetails.appointmentDateRequest = date;
    setIsLoading(true);
    console.log("this.props.appointmentDetails", props.appointmentDetails);
    const slotResponse = axios
      .post(
        apiUrl.serverUrl + apiUrl.bookingTimeSlot,
        this.props.appointmentDetails,
        apiUrl.config
      )
      .then((slots) => {
        props.appointmentDetails.slots = slots.data;
        setSlots(slots.data);
        setAppointmentDateRequest(date);
        setSelectedDate(date);
        setIsLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });*/
  };
  const selectSlot = (time, date, slots) => {
    if (props.appointmentDetails.timeSlot == time) {
      props.appointmentDetails.timeSlot = "";
    } else {
      props.appointmentDetails.timeSlot = time;
    }
    setSlot(props.appointmentDetails.timeSlot);

    //props.handleTimeSlotUpdate();
  };

  const handleYearMonthChange = (month) => {
    setMonth(month);
  };
  const handleDayClick = (day, { selected }) => {
    setSelectedDate(selected ? day : undefined);
  };

  const modifiers = {
    selected: selectedDate,
  };


  var bookingDayLimit = appContext.bookingData
  && appContext.bookingData.schedule.bookingDayLimit;
  var allowScheduleLimit =appContext.bookingData
  && appContext.bookingData.schedule.allowScheduleLimit;
  var bookingDayLimitType = appContext.bookingData
  && appContext.bookingData.schedule.bookingDayLimitType;

  var limitDate = null;
  if (allowScheduleLimit && bookingDayLimitType && bookingDayLimit > 0) {
    if (bookingDayLimitType && bookingDayLimitType.startsWith("Month")) {
      bookingDayLimit = bookingDayLimit * 30;
    } else if (bookingDayLimitType && bookingDayLimitType.startsWith("Week")) {
      bookingDayLimit = bookingDayLimit * 7;
    }
    limitDate = new Date();
    limitDate.setDate(limitDate.getDate() + bookingDayLimit);
  }
/*
  // setup blocked days days to be disable from calendar
  var disabledDayList = [];
  for (let i = 0; i < blockedDates.length; i++) {
    var date = blockedDates[i].split("/");
    disabledDayList.push(new Date(date[2], date[1] - 1, date[0]));
  }*/
  // console.log("location", location)
  return (
    <ContextLayout.Consumer>
      {(context) => {
          console.log("context ",context )
        return (
          <Container className="w-100 mt-5 mt-md-0 d-flex justify-content-center">
            <div>
                <div>
                    <h3>Choose a Date</h3>
                    <span className="mt-3">
                        {
                            context.appointmantDate &&
                            context.appointmantDate !== null &&
                            `${ moment(context.appointmantDate).locale(definedLan).format('dddd')}, ${moment(context.appointmantDate).locale('en').format('MMMM')} ${moment(context.appointmantDate).locale(definedLan).format('d')}, ${moment(context.appointmantDate).locale(definedLan).format('y')}`
                        }
                    </span>
                    {/*<p className="mt-4">Select a time on</p>*/}
                </div>
              <style>{`${selectedStyle}`}</style>
              <DayPicker
                className="appointment-calendar"
                localeUtils={MomentLocaleUtils}
                locale={localStorage.getItem("rightTime_lang")}
                //month={monthYear}
                modifiers={modifiers}
                weekdayElement={<Weekday />}
                disabledDays={[
                  { daysOfWeek: context.bookingData
                  && context.bookingData.schedule ?
                  context.bookingData.schedule.offWorkDays : []
                },
                  { before: new Date() },
                  { after: limitDate },
                ].concat([])}
                selectedDays={selectedDate}
                //value={selectedDate}
                toMonth={limitDate}
                onDayClick={
                    (date) =>{
                        handleDateSelection(date)
                        context.setAppointmentDate(date)
                    }
                }
                //onMonthChange={calendarChange}
                captionElement={({ date, localeUtils }) => (
                  <YearMonthForm date={date} localeUtils={localeUtils} />
                )}
              />
            </div>
          </Container>
        );
      }}
    </ContextLayout.Consumer>
  );
}

export default Scheduler;


const Container = styled.div`
   padding-bottom: 110px;
`;