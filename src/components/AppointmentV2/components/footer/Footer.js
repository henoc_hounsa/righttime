import React from "react";
import styled from "styled-components";
import RightComLogo from "../../assets/RightCom.svg";
import { FaAngleRight, FaAngleLeft } from "react-icons/fa";

export default function Footer(props) {
  const navigateToNext = () => {
    props.navigateToNext()
  }
  const navigateToPrevious = () => {
    props.navigateToPrevious()
  }

  return (
    <Container>
      <ButtonStepContainer>
        <ButtonBack disabled={props.disabledPrev} onClick={props.disabledPrev == true ? null : navigateToPrevious} className="mr-2 px-3">
          <FaAngleLeft className="mr-4" /> BACK
        </ButtonBack>
        <ButtonNext disabled={props.disabledNext} onClick={props.disabledNext == true ? null : navigateToNext} className="mr-2 px-3">
          NEXT STEP <FaAngleRight className="ml-4" />
        </ButtonNext>
      </ButtonStepContainer>
      <BottomLogo>
        A product of <img className="ml-1" src={RightComLogo} />
      </BottomLogo>
    </Container>
  );
}

const Container = styled.div`
  position: absolute;
  height: 110px;
  right: 0;
  bottom: 0;
  width: 100%;
  background: #ffffff 0% 0% no-repeat padding-box;
  box-shadow: 0px -9px 6px #0000000d;
  @media (max-width: 600px) {
    background: #fafafa 0% 0% no-repeat padding-box;
    border: 0.75px solid #e4e4e4;
    opacity: 1;
    display: flex;
    height: 84px;
    width: 100%;
    bottom: 0;
    right: 0;
    position: "";
    justify-content: flex-end;
    align-items: flex-end;
    padding-right: 25px;
    padding-bottom: 25px;
  }
`;

const BottomLogo = styled.span`
  bottom: 20px;
  right: 20px;
  opacity: 1;
  position: absolute;
  width: 200px;
  font: normal normal normal 11px/25px Source Sans Pro;
  letter-spacing: 0px;
  color: #6c6c6c;
  @media (max-width: 500px) {
    bottom: 0px;
  }
`;

const ButtonStepContainer = styled.div`
  display: flex;
  bottom: 12px;
  right: 250px;
  position: absolute;
  @media (max-width: 500px) {
    position: static;
  }
`;

const ButtonBack = styled.button`
  background: #E4E4E4;
  border: none;
  height: 50px;
  color: #303030;
  cursor: ${(props) => props.disabled == true ? 'not-allowed' : 'pointer' } !important;
  letter-spacing: 0px;
  font: normal normal bold 12px/15px Source Sans Pro;
  border-radius:5px;
`;

const ButtonNext = styled.button`
  background:#0089E1;
  border: none;
  height: 50px;
  cursor: ${(props) => props.disabled == true ? 'not-allowed' : 'pointer' } !important;
  color: #FAFBFE;
  letter-spacing: 0px;
  font: normal normal bold 12px/15px Source Sans Pro;
  border-radius:5px;
`;
