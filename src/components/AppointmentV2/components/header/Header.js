import React from "react";
import logoUrl from "../../../../logo.png";
import flag from "../../assets/united-kingdom.png";
import { Form } from "react-bootstrap";
import "./index.css";

function Header() {
  return (
    <div className="rs-header-container">
      <span className="d-flex align-items-center h-100 flex-column ">
        <img src={logoUrl} className="rs-header-logo" />
        <p className="rs-header-logoDesc">Company Logo</p>
      </span>
      <span className="d-flex align-items-center">
        <img src={flag} />
        <Form.Control className="rs-header-language-selector  ml-1" as="select" size="sm">
          <option>ENGLISH</option>
        </Form.Control>
      </span>
    </div>
  );
}

export default Header;
