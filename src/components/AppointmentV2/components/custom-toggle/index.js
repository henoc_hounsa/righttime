import React, { useState } from "react";
import { FormControl } from "react-bootstrap";
import { FaAngleDown } from "react-icons/fa";
import styled from "styled-components";

const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
  <a
    href=""
    style={{ textDecoration: "none", color: "white" }}
    ref={ref}
    onClick={(e) => {
      e.preventDefault();
      onClick(e);
    }}
  >
    <div  className="w-100">
      <LabeContainer className="w-100">
        <div>{children}</div>
        <FaAngleDown />
      </LabeContainer>
    </div>
  </a>
));

const CustomMenu = React.forwardRef(
  ({ children, style, className, "aria-labelledby": labeledBy }, ref) => {
    const [value, setValue] = useState("");

    return (
      <div
        ref={ref}
        style={style}
        className={className}
        aria-labelledby={labeledBy}

      >
        <FormControl
          autoFocus
          className="w-auto ml-2 mr-2"
          placeholder="Type to filter..."
          onChange={(e) => setValue(e.target.value.toLowerCase())}
          value={value}
        />
        <ul className="list-unstyled mt-2">
          {React.Children.toArray(children).filter(
            (child) =>
             {
               return  !value || child.props.children[0].toLowerCase().startsWith(value)
             }
          )}
        </ul>
      </div>
    );
  }
);

export { CustomToggle, CustomMenu };

const LabeContainer = styled.div`
  font: normal normal normal 14px/14px Source Sans Pro;
  letter-spacing: 0px;
  color: #373737;
  width: 100%;
  display: flex;
  justify-content: space-between;
`;
