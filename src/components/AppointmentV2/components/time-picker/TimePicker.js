import React from "react";
import styled from "styled-components";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { ContextLayout, Layout } from "../../layouts/Layout";
import { useHistory } from "react-router-dom";
import moment from "moment";
import { DualRing } from "react-awesome-spinners";
import axios from "axios";
import apiUrl from "../../../../datas/apiUrl";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
}));
const hours = [
  { value: "09:30 - 11:00 (GMT)", label: "12:00 AM" },
  { value: "11:00 - 11:30 (GMT)", label: "01:00 AM" },
  { value: "11:30 - 12:00 (GMT)", label: "02:00 AM" },
  { value: "12:00 - 12:30 (GMT)", label: "03:00 AM" },
  { value: "13:30 - 14:00 (GMT)", label: "04:00 AM" },
  { value: "14:30 - 15:00 (GMT)", label: "05:00 AM" },
  { value: "15:00 - 15:30 (GMT)", label: "06:00 AM" },
  { value: "15:30 - 16:00 (GMT)", label: "07:00 AM" },
  { value: "16:00 - 16:30 (GMT)", label: "08:00 AM" },
  { value: "16:30 - 17:00 (GMT)", label: "09:00 AM" },
  { value: "17:00 - 17:30 (GMT)", label: "10:00 AM" },
  { value: "17:30 - 18:00 (GMT)", label: "11:00 AM" },
];
function TimePicker(props) {
  const classes = useStyles();
  const [picked, setPicked] = React.useState(null);
  const [selected, setSelected] = React.useState(null);
  const history = useHistory();
  const [isLoading, setLoading] = React.useState(true);
  const definedLan = localStorage.getItem("rightTime_lang")
    ? localStorage.getItem("rightTime_lang")
    : "en";
  const appContext = React.useContext(ContextLayout);
  React.useEffect( () => {
    if (appContext.slot && appContext.slot !== null) {
      //setPicked(appContext.slot)
      props.setDisabledNext(false);
    }
    const req = {};
    req.schedule = appContext.bookingData ? appContext.bookingData.schedule : {};
    req.xpUserAlias =  appContext.bookingData ? appContext.bookingData.alias : "";
    req.companyName =  appContext.bookingData ? appContext.bookingData.companyName : '';
    req.accountUrl =  appContext.bookingData ? appContext.bookingData.schedule.url : '';
    req.slots = [];
    req.agent = appContext.provider ? appContext.provider.userId : '';
    req.appointmentDateRequest =
      appContext.provider && moment(appContext.appointmantDate).toISOString();
      console.log("apreter ___")
      axios.get(
        apiUrl.serverUrl + apiUrl.bookingTimeSlot,
        apiUrl.config
      ).then(({data})=>{
        console.log("data", data)
      }).catch((err) => {
        console.log("err", err)
      })
    setTimeout(() => {
      if (isLoading == true) {
        setLoading(false);
      }
    }, 1500);


  }, []);

  const pick = (time) => {
    props.setDisabledNext(false);
    setPicked(time);
  };

 
  return (
    <ContextLayout.Consumer>
      {(context) => {
        // console.log("moment(context.appointmantDate).locale('en').format('dddd')", moment(context.appointmantDate).locale('en').format('dddd'))
        return (
          <Container className="">
            <h3>Schedule an appointment with Adamanah</h3>
            <h5 className="mt-3">
              {context.appointmantDate &&
                context.appointmantDate !== null &&
                `${moment(context.appointmantDate)
                  .locale(definedLan)
                  .format("dddd")}, ${moment(context.appointmantDate)
                  .locale("en")
                  .format("MMMM")} ${moment(context.appointmantDate)
                  .locale(definedLan)
                  .format("d")}, ${moment(context.appointmantDate)
                  .locale(definedLan)
                  .format("y")}`}
              <a
                style={{ cursor: "pointer" }}
                onClick={(e) => {
                  e.preventDefault();
                  history.push("/schedule");
                }}
                className="ml-2"
              >
                Change Date
              </a>
            </h5>
            <p className="mt-4">Select a time on</p>
            <p>
              Time zone :{" "}
              {context.bookingData &&
                context.bookingData.schedule &&
                context.bookingData.schedule.rtTimezone &&
                `${context.bookingData.schedule.rtTimezone.capital} / ${context.bookingData.schedule.rtTimezone.country} (${context.bookingData.schedule.rtTimezone.timeoffset})`}{" "}
            </p>

            <div className="mt-3">
              {isLoading == true && (
                <div className="w-100 text-center my-4">
                  {" "}
                  <DualRing />
                </div>
              )}
              <Grid container spacing={2}>
                {context.bookingData &&
                  context.bookingData.schedule &&
                  context.bookingData.schedule.workDays &&
                  context.bookingData.schedule.workDays[
                    moment(context.appointmantDate)
                      .locale(definedLan)
                      .format("dddd")
                  ] &&
                  context.bookingData.schedule.workDays[
                    moment(context.appointmantDate)
                      .locale(definedLan)
                      .format("dddd")
                  ].breakTime &&
                  Object.keys(
                    context.bookingData.schedule.workDays[
                      moment(context.appointmantDate)
                        .locale(definedLan)
                        .format("dddd")
                    ].breakTime
                  ).map((x, k) => {
                    return (
                      <Grid
                        onClick={() => {
                          context.setTimeSlot(
                            context.bookingData.schedule.workDays[
                              moment(context.appointmantDate)
                                .locale(definedLan)
                                .format("dddd")
                            ].breakTime[x]
                          );
                          pick(
                            context.bookingData.schedule.workDays[
                              moment(context.appointmantDate)
                                .locale(definedLan)
                                .format("dddd")
                            ].breakTime[x].breakId
                          );
                        }}
                        key={k}
                        item
                        xs={4}
                      >
                        <Item
                          selected={
                            picked ==
                            context.bookingData.schedule.workDays[
                              moment(context.appointmantDate)
                                .locale(definedLan)
                                .format("dddd")
                            ].breakTime[x].breakId
                          }
                          className={classes.paper}
                        >
                          {`${
                            context.bookingData.schedule.workDays[
                              moment(context.appointmantDate)
                                .locale(definedLan)
                                .format("dddd")
                            ].breakTime[x].breakTimeStart
                          } : ${
                            context.bookingData.schedule.workDays[
                              moment(context.appointmantDate)
                                .locale(definedLan)
                                .format("dddd")
                            ].breakTime[x].breakTimeEnd
                          }`}
                        </Item>
                      </Grid>
                    );
                  })}
              </Grid>
            </div>
          </Container>
        );
      }}
    </ContextLayout.Consumer>
  );
}

export default TimePicker;

const Container = styled.div`
  text-align: left;
  font: normal normal bold 14px/14px Source Sans Pro;
  letter-spacing: 0px;
  padding-left: 80px;
  padding-right: 30px;
  padding-bottom: 120px;
  h3 {
    text-align: left;
    font: normal normal 600 24px/19px Source Sans Pro;
    letter-spacing: 0px;
    color: #373737;
  }
  h5 {
    text-align: left;
    font: normal normal bold 16px/19px Source Sans Pro;
    letter-spacing: 0px;
    color: #0089e1;
  }

  p {
    font: normal normal normal 18px/19px Source Sans Pro;
    letter-spacing: 0px;
    color: #373737;
  }
  a {
    text-decoration: underline;
    font: normal normal normal 14px/19px Source Sans Pro;
    letter-spacing: 0px;
    color: #0089e1;
  }
  @media (max-width: 769px) {
    padding-left: 0;
    padding-right: 0;
    margin-top: 50px;
  }
`;

const Item = styled.div`
  border-radius: 5px;
  border: 1px solid #e4e4e4;
  background: ${(props) => (props.selected == true ? "#0089E1" : "#FFFFFF")};
  color: ${(props) => (props.selected == true ? "#FFFFFF" : "#808080")};
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
`;
