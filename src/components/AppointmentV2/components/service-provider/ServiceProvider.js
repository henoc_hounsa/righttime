import React from "react";
import styled from "styled-components";
import bgWrapper from "../../assets/teacher.jpg";
import { TOP_NAVBAR_HEIGHT } from "../../_constants";
import RightComLogo from "../../assets/RightCom.svg";
import Image from "../../assets/image.png";
import axios from "axios";
import apiUrl from "../../../../datas/apiUrl";
import { useLocation, useHistory } from "react-router-dom";
import { FaCheck } from "react-icons/fa";
import { ContextLayout, Layout } from "../../layouts/Layout";
import { DualRing } from "react-awesome-spinners";

const fakeProviders = [
  {
    userId: 1,
    firstname: "Hdsc",
    lastname: "Jjbdcjdj",
  },
  {
    userId: 2,
    firstname: "GHhjnj",
    lastname: "BHjhbjbhj",
  },
  {
    userId: 3,
    firstname: "NJKNkjn",
    lastname: "JNJKnknkjn",
  },
  {
    userId: 4,
    firstname: "KJNJKnkj",
    lastname: "KJNKnk",
  },
];

function ServiceProvider(props) {
  const location = useLocation();
  const history = useHistory();
  const appContext = React.useContext(ContextLayout);
  const [providers, setProviders] = React.useState(null);
  const [selected, setSelected] = React.useState(null);
  const [isLoading, setLoading] = React.useState(true);
  const definedLan = localStorage.getItem("rightTime_lang")
    ? localStorage.getItem("rightTime_lang")
    : "en";

  React.useEffect(() => {
    setTimeout(() => {
      if (isLoading == true) {
        setLoading(false);
      }
    }, 1500);

    if (appContext.provider !== null) {
      setSelected(appContext.provider.userId);
      props.setDisabledNext(false);
    }
    let selectedLocation = appContext.currentLocation;
    let selectedService = appContext.service;
    if (
      selectedLocation &&
      selectedLocation !== null &&
      selectedService &&
      selectedService !== null
    ) {
      axios
        .get(
          `${apiUrl.serverUrl}/${apiUrl.getProviders}?url=${apiUrl.alias}&alias=${apiUrl.alias}&location_id=${selectedLocation}&service_id=${selectedService}`,
          apiUrl.config
        )
        .then(({ data }) => {
          setProviders(data);
        })
        .catch(console.log);
    }
  }, []);
  const navigateToSchedule = (provider) => {
    props.setDisabledNext(false);
    setSelected(provider.userId);
    // appContext.setCurrentProvider(provider)
  };
  return (
    <ContextLayout.Consumer>
      {(context) => {
        return (
          <Container>
            <h4>Service Providers</h4>
            <h5>Choose a Service Provider</h5>
            <ListContainer>
              {isLoading == false && (!providers || providers.length < 1) ? (
                <div className="mt-5">
                  {definedLan == "fr"
                    ? "Aucun fournisseur trouvé !"
                    : "No provider found !"}
                </div>
              ) : null}
              {providers &&
                providers.length > 0 &&
                providers.map((item, k) => {
                  return (
                    <Item
                      selected={selected == item.userId}
                      onClick={() => {
                        context.setCurrentProvider(item);
                        navigateToSchedule(item);
                      }}
                      className="mt-4 mb-2 d-flex p-3 align-items-center"
                      key={k}
                    >
                      <div className="d-flex justify-content-between w-100">
                        <div>
                          {/*<ServiceImage*/}
                          {/*  style={{*/}
                          {/*    background: `transparent url(${Image}) no-repeat`,*/}
                          {/*  }}*/}
                          {/*/>*/}
                          <div className={"d-flex align-items-center"}>
                            <div
                              style={{
                                width: 52,
                                height: 52,
                                background: "#0089e1",
                                color: "white",
                                borderRadius: "50%",
                                display: "flex",
                                alignItems: "center",
                                justifyContent: "center",
                                fontWeight: "bold",
                              }}
                            >
                              {item.firstname[0] + "" + item.lastname[0]}
                            </div>
                            <div
                              style={{ height: "81px" }}
                              className="d-flex ml-3 flex-columnx justify-content-center align-items-center"
                            >
                              <div>
                                <div
                                  style={{
                                    fontSize: "16px",
                                    fontWeight: "bold",
                                  }}
                                >
                                  {`${item.firstname || ""} ${
                                    item.lastname || ""
                                  }`}
                                </div>
                                <div
                                  style={{ fontSize: "12px", color: "#707070" }}
                                >
                                  Service provider
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        {selected == item.userId && (
                          <FaCheck style={{ color: "#007bff" }} />
                        )}
                      </div>
                    </Item>
                  );
                })}
              {isLoading == true && (
                  <div className="w-100 text-center my-4">
                    {" "}
                    <DualRing />
                  </div>
              )}
            </ListContainer>
          </Container>
        );
      }}
    </ContextLayout.Consumer>
  );
}

export default ServiceProvider;

const Container = styled.div`
  padding-left: 80px;
  padding-right: 30px;
  overflow: auto;
  overflow-x: hidden;
  @media (max-width: 600px) {
  }

  h4 {
    text-align: left;
    font: normal normal 600 24px/19px Source Sans Pro;
    letter-spacing: 0px;
    color: #373737;
  }
  h5 {
    text-align: left;
    font: normal normal normal 18px/19px Source Sans Pro;
    letter-spacing: 0px;
    color: #373737;
  }
  @media (max-width: 769px) {
    padding-left: 0;
    padding-right: 0;
    margin-top: 50px;
  }
`;

const ListContainer = styled.div`
  padding-bottom: 110px;
`;

const Item = styled.div`
  cursor: pointer;
  height: 98px;
  border-radius: 5px;
  background: #ffffff 0% 0% no-repeat padding-box;
  border: ${(props) =>
    props.selected == true ? "1px solid #0089E1" : "1px solid #e4e4e4"};
`;

const ServiceImage = styled.span`
  width: 60px;
  height: 60px;
  border-radius: 50%;
  background-size: cover;
`;
