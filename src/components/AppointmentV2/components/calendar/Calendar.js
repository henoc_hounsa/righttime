import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { FaCaretRight, FaCaretLeft } from "react-icons/fa";
const { datesGenerator } = require("dates-generator");

const months = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];
const days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

const Container = styled.div`
  width: 490px;
  margin: 0 auto;
  h4{
    text-align: left;
    font: normal normal 600 24px/19px Source Sans Pro;
    letter-spacing: 0px;
    color: #373737;
  }
`;

const MonthText = styled.div`
  font-size: 14px;
  color:'#E4E4E4',
  font-weight: bold;
  text-transform: uppercase;
`;

const Calendar = (props) => {
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [dates, setDates] = useState([]);
  const [calendar, setCalendar] = useState({
    month: selectedDate.getMonth(),
    year: selectedDate.getFullYear(),
  });

  useEffect(() => {
    const body = {
      month: calendar.month,
      year: calendar.year,
    };
    const {
      dates,
      nextMonth,
      nextYear,
      previousMonth,
      previousYear,
    } = datesGenerator(body);

    setDates([...dates]);
    setCalendar({
      ...calendar,
      nextMonth,
      nextYear,
      previousMonth,
      previousYear,
    });
  }, []);

  const onClickNext = () => {
    const body = { month: calendar.nextMonth, year: calendar.nextYear };
    const {
      dates,
      nextMonth,
      nextYear,
      previousMonth,
      previousYear,
    } = datesGenerator(body);

    setDates([...dates]);
    setCalendar({
      ...calendar,
      month: calendar.nextMonth,
      year: calendar.nextYear,
      nextMonth,
      nextYear,
      previousMonth,
      previousYear,
    });
  };

  const onClickPrevious = () => {
    const body = { month: calendar.previousMonth, year: calendar.previousYear };
    const {
      dates,
      nextMonth,
      nextYear,
      previousMonth,
      previousYear,
    } = datesGenerator(body);

    setDates([...dates]);
    setCalendar({
      ...calendar,
      month: calendar.previousMonth,
      year: calendar.previousYear,
      nextMonth,
      nextYear,
      previousMonth,
      previousYear,
    });
  };

  const onSelectDate = (date) => {
    setSelectedDate(new Date(date.year, date.month, date.date));
    props.navigate(4)
  };

  return (
    <div style={{ width: "100%" }}>
      <Container>
        <div className="pl-4 pr-4 mb-2">
          <h4>Schedule an appointment with {props.provider ? `${props.provider.firstname } ${props.provider.lastname}`: ''} </h4>
          <span className="mt-2">Choose a date</span>
        </div>
        <div className="d-flex align-items-center justify-content-between pl-4 pr-4">
          <div className="d-flex align-items-center">
            <MonthText>{months[calendar.month]}</MonthText>
            <MonthText style={{ color: "#0089E1" }} className="ml-2">
              {calendar.year}
            </MonthText>{" "}
          </div>
          <div>
            <span className="mr-4" onClick={onClickPrevious}>
              <FaCaretLeft style={{ color: "#E4E4E4" }} />
            </span>
            <span onClick={onClickNext}>
              <FaCaretRight style={{ color: "#E4E4E4" }} />
            </span>
          </div>
        </div>

        <div className="pl-2 pr-2">
        <table style={{ width: "100%" }}>
          <tbody>
            <tr>
              {days.map((day) => (
                <td key={day} style={{ padding: "5px 0" }}>
                  <div style={{ textAlign: "center", padding: "5px 0" }}>
                    {day}
                  </div>
                </td>
              ))}
            </tr>

            {dates.length > 0 &&
              dates.map((week) => (
                <tr key={JSON.stringify(week[0])}>
                  {week.map((each) => ( 
                    <td
                      key={JSON.stringify(each)}
                      style={{ }}
                    >
                      <div
                      className="m-2"
                        onClick={() => onSelectDate(each)}
                        style={{ textAlign: "center", padding: "5px 0", background: '#BBBBBB',borderTop:'2px solid #0089E1',cursor:'pointer' }}
                      >
                        {each.date}
                      </div>
                    </td>
                  ))}
                </tr>
              ))}
          </tbody>
        </table>
      </div>
      </Container>
    </div>
  );
};

export default Calendar;
