import React, { useContext } from "react";
import styled from "styled-components";
import { Form, Col, Row } from "react-bootstrap";
import { ContextLayout, Layout } from "../../layouts/Layout";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function UserInfos(props) {
  const appContext = useContext(ContextLayout);
  //console.log("appContext",appContext)
  const definedLan = localStorage.getItem("rightTime_lang")
    ? localStorage.getItem("rightTime_lang")
    : "en";

  const [state, setState] = React.useState({});

  const handleChange = (e) => {
    appContext.onUserInfosChange(e);
    if (!/\S/.test(e.target.value)) {
      appContext.setErr(
        e.target.name,
        true,
        definedLan == "fr"
          ? `${e.target.name.toLowerCase()} invalide`
          : `${e.target.name.toLowerCase()} invalid`,
        e.target.value
      );
    } else {
      appContext.setErr(e.target.name, false, "", e.target.value);
    }
    if (e.target.name == "Email") {
      if (
        !(
          /\S/.test(e.target.value) &&
          /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(e.target.value)
        )
      ) {
        appContext.setErr(
          e.target.name,
          true,
          definedLan == "fr"
            ? `${e.target.name.toLowerCase()} invalide`
            : `${e.target.name.toLowerCase()} invalid`,
          e.target.value
        );
      } else {
        appContext.setErr(e.target.name, false, "", e.target.value);
      }
    }
    //setState({ [e.target.name]: e.target.value,...state });
    validateForm();
  };

  const validateForm = () => {
    let fields =
      appContext.bookingData &&
      appContext.bookingData.schedule &&
      appContext.bookingData.schedule.customDisplayFields &&
      Array.isArray(appContext.bookingData.schedule.customDisplayFields) &&
      appContext.bookingData.schedule.customDisplayFields.length > 0
        ? appContext.bookingData.schedule.customDisplayFields.map(
            (x) => x[definedLan]
          )
        : [];
    const errors =
      appContext.errors !== null ? Object.keys(appContext.errors) : [];

    if (
      fields.length == errors.length &&
      appContext.errors !== null &&
      !Object.keys(appContext.errors).some(
        (x) => appContext.errors[x].hasErr == true
      )
    ) {
      props.setDisabledNext(false);
    }
  };

  return (
    <ContextLayout.Consumer>
      {(context) => {
      
        return (
          <Container className="pl-5 pr-4">
            <h4>
              {definedLan == "en" ? "Your Information" : "Vos informations"}{" "}
            </h4>
            <h5 className="mt-3">
              {definedLan == "en" ? "Enter your details" : "Entrez vos détails"}
            </h5>

            <Form className="mt-3">
              {context.bookingData &&
                context.bookingData.schedule &&
                context.bookingData.schedule.customDisplayFields &&
                Array.isArray(
                  context.bookingData.schedule.customDisplayFields
                ) &&
                context.bookingData.schedule.customDisplayFields.length > 0 &&
                context.bookingData.schedule.customDisplayFields.map(
                  (field, k) => {
                   
                    return (
                      <Form.Group key={k}>
                        <Form.Label>{field[definedLan]}</Form.Label>
                        <Form.Control
                          onChange={handleChange}
                          name={field["en"]}
                          type="text"
                          size={"lg"}
                          placeholder={field[definedLan]}
                          className="mb-2"
                        />
                        {context.errors !== null &&
                          context.errors[field["en"]] &&
                          context.errors[field["en"]].hasErr == true && (
                            <span style={{ color: "red", fontSize: 14 }}>
                              {context.errors[field["en"]].msg}
                            </span>
                          )}
                      </Form.Group>
                    );
                  }
                )}
            </Form>
          </Container>
        );
      }}
    </ContextLayout.Consumer>
  );
}

export default UserInfos;

const Container = styled.div`
  text-align: left;
  font: normal normal 600 24px/19px Source Sans Pro;
  letter-spacing: 0px;
  overflow: auto;
  overflow-x: hidden;
  color: #373737;
  padding-bottom: 110px;
  // max-height: 90%;
  @media (max-width: 769px) {
    padding-left: 0;
    padding-right: 0;
    margin-top: 70px !important;
  }
  h4 {
    text-align: left;
    font: normal normal 600 22px/19px Source Sans Pro;
    letter-spacing: 0px;
    color: #373737;
  }
  h5 {
    font: normal normal normal 18px/19px Source Sans Pro;
    letter-spacing: 0px;
    color: #373737;
  }

  label {
    font: normal normal 600 14px/24px Source Sans Pro;
    letter-spacing: 0px;
    color: #373737;
  }
`;
