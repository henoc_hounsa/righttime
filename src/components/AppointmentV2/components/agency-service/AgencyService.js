import React,{useContext} from "react";
import styled from "styled-components";
import bgWrapper from "../../assets/teacher.jpg";
import { TOP_NAVBAR_HEIGHT } from "../../_constants";
import RightComLogo from "../../assets/RightCom.svg";
import { ContextLayout, Layout } from "../../layouts/Layout";
import Image from "../../assets/image.png";
import axios from "axios";
import apiUrl from "../../../../datas/apiUrl";
import { useLocation, useHistory } from "react-router-dom";
import { FaCheck } from "react-icons/fa";
import { DualRing } from "react-awesome-spinners";


const services = [
  {
    id:1,
    name:{
      en:'sdcsd',
      fr:'adsdscsdc'
    },
    description:'BHJBbjhbi  bjb hj',
    serviceCostCurrency:'$',
    serviceCost: 1000
  },
  {
    id:2,
    name:{
      en:'sdcsd',
      fr:'adsdscsdc'
    },
    description:'nkNK jkjnjkn',
    serviceCostCurrency:'$',
    serviceCost: 400
  },
  {
    id:3,
    name:{
      en:'sdcsd',
      fr:'adsdscsdc'
    },
    description:'NKnkjnjk jbjk',
    serviceCostCurrency:'$',
    serviceCost: 100
  },
  {
    id:4,
    name:{
      en:'sdcsd',
      fr:'adsdscsdc'
    },
    description:'nkNK jkjnjkn',
    serviceCostCurrency:'$',
    serviceCost: 245
  },
  {
    id:5,
    name:{
      en:'sdcsd',
      fr:'adsdscsdc'
    },
    description:'MNn kjn jbjk',
    serviceCostCurrency:'$',
    serviceCost: 100
  }
]

function AgencyService(props) {
  const location = useLocation();
  const history = useHistory();
  const appContext = useContext(ContextLayout);
  const [serviceOffering, setServiceOffering] = React.useState(null);
  const [_location, setLocation] = React.useState(null);
  const [_services, setServices] = React.useState(null);
  const [selected, setSelected] = React.useState(null);
  const [isLoading, setLoading] = React.useState(true);
  const definedLan = localStorage.getItem("rightTime_lang")
    ? localStorage.getItem("rightTime_lang")
    : "en";

  React.useEffect(() => {
    setTimeout(() => {
      if (isLoading == true) {
        setLoading(false);
      }
    }, 1500);
    if(appContext.service !== null){
      props.setDisabledNext(false);
      setSelected(appContext.service);
    }
 
   axios
    .get(
      `${apiUrl.serverUrl}/${apiUrl.getLocationServices}?url=${apiUrl.alias}&location_id=${appContext.currentLocation}`,
      apiUrl.config
    )
    .then(({ data }) => {
     
      setServices(data);
    })
    .catch(console.log);

  }, []);

  const changeLocation = () => {
    history.push("/locations");
  };
  const navigateToProvider = (service) => {
    props.setDisabledNext(false);
    setSelected(service.id);

  };

  //console.log("_services", _services);
  return (
    <ContextLayout.Consumer>
      {(context) => {
        //console.log("context",context)
        return (
          <Container>
            <h4>
              {context.bookingData &&
                context.bookingData.schedule &&
                context.bookingData.schedule.customServiceOffering &&
                context.bookingData.schedule.customServiceOffering.serviceHeadingText &&
                context.bookingData.schedule.customServiceOffering.serviceHeadingText[definedLan]}
            </h4>
            <div className="d-flex justify-content-between align-items-center mt-4">
              <span>Choose a Service</span>
              <span>
                Location:{" "}
                {(_location &&
                  _location !== null &&
                  _location.businessLocationName) ||
                  ""}{" "}
                <a
                  onClick={changeLocation}
                  style={{ textDecoration: "underline" }}
                  href="#"
                >
                  Change
                </a>
              </span>
            </div>
            <ListContainer>
            {
              isLoading == false &&
              (!_services ||
             _services &&
              _services.length  < 1) ?
              <div className="d-flex justify-content-center mt-5">{definedLan == 'fr' ? 'Aucun service' : 'No service found'}</div> : null
            }
              {_services &&
                _services.length > 0 &&
                _services.map((item, k) => {
                  const image = apiUrl.xpCdn.replace(
                    "{{imagepath}}",
                    item.imagePath || ''
                  );
                  return (
                    <Item
                      selected={  (context.service !== null && context.service ==  item.id) || item.id == selected   }
                      onClick={() => {
                        context.setCurrentService(item)
                        navigateToProvider(item)
                      }}
                      className="mt-4 mb-2 d-flex p-3 align-items-center"
                      key={k}
                    >
                      <div className="d-flex justify-content-between w-100">
                        <div className="d-flex">
                          <ServiceImage
                            style={{
                              background: item.imagePath?`transparent url(${ image}) no-repeat`:`transparent url('/assets/service.png') no-repeat`,
                                backgroundSize:'contain'
                            }}
                          />
                          <div
                            style={{ height: "81px" }}
                            className="d-flex ml-3 flex-column justify-content-center"
                          >
                            <span
                              style={{ fontSize: "16px", fontWeight: "bold" }}
                            >
                              {item.name[definedLan]}
                            </span>
                            <span style={{ color: "#808080" }}>
                              {item.description}
                            </span>
                            <span style={{ color: "#0087E4" }}>{`${
                              item.serviceCostCurrency
                            }${item.serviceCost || 0}`}</span>
                          </div>
                        </div>
                        { ((context.service !== null && context.service ==  item.id) || item.id == selected ) && (
                          <FaCheck style={{ color: "#007bff" }} />
                        )}
                      </div>
                    </Item>
                  );
                })}
                {
                    isLoading == true &&
                    <div className="w-100 text-center my-4"> <DualRing/></div>
                }
            </ListContainer>
          </Container>
        );
      }}
    </ContextLayout.Consumer>
  );
}

export default AgencyService;

const Container = styled.div`
  padding-left: 80px;
  padding-right: 30px;
  height: calc(70vh);
  padding-bottom: 10px;
  @media (max-width: 600px) {
  }

  h4 {
    text-align: left;
    font: normal normal 600 24px/35px Source Sans Pro;
    letter-spacing: 0px;
    color: #373737;
  }
  @media (max-width: 769px) {
    padding-left: 0;
    padding-right: 0;
    margin-top: 50px;
  }
`;

const ListContainer = styled.div`
  padding-bottom: 110px;
  min-height: 100%;
`;

const Item = styled.div`
  cursor: pointer;
  height: 130px;
  background: #ffffff 0% 0% no-repeat padding-box;
  border-radius: 5px;
  border: ${(props) =>
    props.selected == true ? "1px solid #0089E1" : "1px solid #e4e4e4"};
`;

const ServiceImage = styled.div`
  width: 123px;
  height: 81px;
  border-radius: 5px;
  background-size: cover;
`;
