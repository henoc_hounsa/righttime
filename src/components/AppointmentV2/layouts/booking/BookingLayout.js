import React, { Suspense, useState,useContext } from "react";
import Topnavbar from "../../../Topnavbar/Topnavbar";
import axios from "axios";
import apiUrl from "../../../../datas/apiUrl";
import { TOP_NAVBAR_HEIGHT } from "../../_constants";
import { Col, Row, Button } from "react-bootstrap";
import {FaAngleRight,FaAngleLeft} from 'react-icons/fa'
import { ContextLayout, Layout } from "../../layouts/Layout";
import styled from "styled-components";
import ViewerImg from "../../assets/viewer.png";
import Footer from '../../components/footer'
import {labels} from '../../_constants'
import {useLocation} from 'react-router-dom'

export default function BookingLayout(props) {
 
  const appContext = useContext(ContextLayout)
  const [companyDetails, setDetails] = React.useState(null);
  const definedLan = localStorage.getItem("rightTime_lang")
  ? localStorage.getItem("rightTime_lang")
  : "en";
  let location = useLocation()
  const brandLogo = apiUrl.xpCdn.replace(
    "{{imagepath}}",
    appContext.bookingData && appContext.bookingData.schedule && appContext.bookingData.schedule.customDisplaySettings ?  appContext.bookingData.schedule.customDisplaySettings.brandImagePath : ''
  );
  //console.log("brandLogo",brandLogo)
  return (
    <div>
      <Topnavbar
        scpage={true}
        companyLogoPath={props.companyLogoPath}
        companyLanguage={props.companyLanguage}
        language={props.language}
        height={TOP_NAVBAR_HEIGHT}
      />
      <Container>
        <MainContent>
          <Row className="m-0 p-0 d-flex justify-content-center h-100 w-100">
            <Col className="mt-5" md={10}>
              <Row>
                <Col md={5} xs={12}>
                  <Stepper className="w-100">
                    <h4>{appContext.bookingData && appContext.bookingData !==null && appContext.bookingData.schedule.customCompanyDetails[definedLan].headingText}</h4>
                    <h5>
                    {appContext.bookingData && appContext.bookingData !==null && appContext.bookingData.schedule.customCompanyDetails[definedLan].subHeadingText}
                    </h5>
                    <div style={{position:'relative',width:420,margin:'auto'}} className="mt-5 d-flex align-items-center">
                      <ViewerImgContainer brandLogo={brandLogo} />
                      <StepList>
                        <li>{definedLan == 'fr' ? 'Étape 1 à ' : 'Step 1 of '} {Object.keys(appContext.customFlow).length - 1}</li>
                        {
                          Object.entries(appContext.customFlow).sort((a,b) => a[1]-b[1])
                          .map(el => el[0])
                          .filter(elm => elm !=='/confirm')
                          .map((item,k) => {
                            return  <li style={{color: location.pathname == item ? '#0089E1' : '#88BEE4',fontWeight: location.pathname == item ? 'bold' : 'normal'}} key={k}>{labels[item][definedLan]}</li>
                          })

                        }

                      </StepList>
                    </div>
                  </Stepper>
                </Col>
                <Col md={7} xs={12}>{props.children}</Col>
              </Row>
            </Col>
          </Row>
        </MainContent>
        <Footer {...props} />
      </Container>
    </div>
  );
}

const Container = styled.div`
  padding: 0;
  margin: 0;
  display: flex;
  height: 100vh;
  overflow: scroll;
  flex-direction: column;
  width: 100%;
  @media (max-width: 600px) {
  }
`;

const MainContent = styled.div`
  padding-bottom: 300px;
  margin: 0;
  min-height: 100%;
  width: 100%;
  padding-top: ${TOP_NAVBAR_HEIGHT + 10}px;
`;

const Stepper = styled.div`
  color: #0089e1;
  width: 100%;
  display: flex;
  flex-direction: column;

  h4 {
    text-align: left;
    font: normal normal 600 24px/21px Source Sans Pro;
    letter-spacing: 0px;
    color: #0089e1;
  }

  h5 {
    text-align: left;
    font: normal normal normal 18px/22px Source Sans Pro;
    letter-spacing: 0px;
    color: #0089e1;
    margin-top: 10px;
  }
`;

const ViewerImgContainer = styled.div`
  border-radius: 50%;
  border: 1px solid #e4e4e4;
  width: 212px;
  height: 212px;
  padding: 0;
  background: transparent url(${(props) => props.brandLogo}) no-repeat;
  background-size: cover;
`;

const StepList = styled.div`
  display: flex;
  flex-direction: column;
  border-left: 1px solid #e4e4e4;
  justify-content: center;
  padding-left: 26px;
  position: absolute;
  right: 10%;
  color: #373737;
  letter-spacing: 0px;
  background: white;
  min-height: 250px;

  li {
    list-style-type: none;
  }
`;
