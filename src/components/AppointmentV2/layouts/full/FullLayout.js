import React, { Suspense, useState } from "react";
import Topnavbar from "../../../Topnavbar/Topnavbar";
import axios from "axios";
import apiUrl from "../../../../datas/apiUrl";
import { TOP_NAVBAR_HEIGHT } from "../../_constants";

export default function FullLayout(props) {

  return (
    <div>
      <Topnavbar
        scpage={true}
        companyLogoPath={props.companyLogoPath}
        companyLanguage={props.companyLanguage}
        language={props.language}
        height={TOP_NAVBAR_HEIGHT}
      />
      {props.children}
    </div>
  );
}
