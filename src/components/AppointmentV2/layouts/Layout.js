import React from "react";
import BookingLayout from "./booking";
import FullLayout from "./full";
import themeConfig from "../configs/themes";
import axios from "axios";
import apiUrl from "../../../datas/apiUrl";

const ContextLayout = React.createContext();

const groupBy = (array, key) => {
  return array.reduce((result, currentValue) => {
    (result[currentValue[key]] = result[currentValue[key]] || []).push(
      currentValue
    );
    return result;
  }, {});
};

const definedLan = localStorage.getItem("rightTime_lang")
  ? localStorage.getItem("rightTime_lang")
  : "en";

class Layout extends React.Component {
  constructor(props) {
    super(props);
    this.setError = (field,hasErr,msg='',value) => {
      this.setState({ errors: {...this.state.errors,[field]: {msg,hasErr,value} } });
    }
    this.changeCurrentLocation = (location) => {
      //localStorage.setItem('selected_location',location)
      this.setState({ currentLocation: location });
    };
    this.setCurrentService = (service) => {
      //localStorage.setItem('selected_service',service)
      this.setState({ service: service.id,currentService: service });
    };
    this.setCurrentProvider = (provider) => {
      //localStorage.setItem('selected_provider',provider)
      this.setState({ provider: provider });
    };
    this.setTimeSlot = (slot) => {
      //console.log('slot',slot)
      this.setState({ slot: slot });
    };
    this.setAppointmentDate = (date) => {
      this.setState({ appointmantDate: date });
     // localStorage.setItem('selected_appointment_date',date)
    };
    this.changeCity = (city) => {
      //localStorage.setItem('selected_city',city)
      this.setState({ currentCity: city });
    };
    this.setCities = (cities) => {
      this.setState({ cities: cities });
    };
    this.onUserInfosChange = (e) => {
      this.setState({[e.target.name]: e.target.value})

    };
    this.changeCountry = (country) => {
      this.setState({ currentCity: null });
      //.map(k => groupBy(country.apiData,'city')[k][0] )
      this.setCities(Object.keys(groupBy(country.apiData,'city')) || []);
      //localStorage.setItem('curr_country_id',country.id)
      //localStorage.setItem('curr_country_code',country.alpha2Code) 
      this.setState({ currentCountry: country,currentCountryCode: country.alpha2Code});
    };
    this.setSubmited = (val) => {
      this.setState({setSubmited:val })
    }
    this.setErrors = ()=>{
      this.setState({errors :{}})
    }
    this.state = {
      bookingData: null,
      currentLocation: null,
      currentCity: null,
      currentCityId: localStorage.getItem('curr_city_id'),
      currentCountry: null,
      currentCountryId: localStorage.getItem('curr_country_id'),
      currentCountryCode: null,
      currentService: null,
      errors:null,
      cities: [],
      service: null,
      provider: null,
      slot: null,
      appointmantDate:  null,
      submited: false,
      userInfos: {},
      customFlow:  {
        '/booking':0,
        '/providers': parseInt(localStorage.getItem("provider_id")),
        '/schedule': parseInt( localStorage.getItem("calendar_id")),
        '/pick-time':3,
        '/user-infos':4,
        '/confirm': 5,
       },
      changeCurrentLocation: this.changeCurrentLocation,
      changeCity: this.changeCity,
      changeCountry: this.changeCountry,
      setCities: this.setCities,
      setCurrentService: this.setCurrentService,
      setCurrentProvider: this.setCurrentProvider,
      setTimeSlot: this.setTimeSlot,
      setAppointmentDate: this.setAppointmentDate,
      onUserInfosChange:this.onUserInfosChange,
      setSubmited: this.setSubmited,
      setErrors: this.setErrors,
      setErr: this.setError
    };
  }

  componentDidMount() {
    axios
      .get(
        `${apiUrl.serverUrl}${apiUrl.getBookingData}?url=${apiUrl.alias}`,
        apiUrl.config
      )
      .then(({ data }) => {
        this.setState({ bookingData: data });
        if(data &&  data.schedule && 
          data.schedule.customDisplaySettings &&
          data.schedule.customDisplaySettings &&
          data.schedule.customDisplaySettings.customizeFlow){
            let calendarId = Object.keys(data.schedule.customDisplaySettings.customizeFlow)
            .find(key => data.schedule.customDisplaySettings.customizeFlow[key] == 'CALENDAR')
            let providerId = Object.keys(data.schedule.customDisplaySettings.customizeFlow)
            .find(key => data.schedule.customDisplaySettings.customizeFlow[key] == 'SERVICE_PROVIDER')
            localStorage.setItem('provider_id', providerId && providerId!==null && providerId !==false ? providerId : 1)
            localStorage.setItem('calendar_id', calendarId && calendarId!==null && calendarId !==false ? calendarId : 1)
          }
      })
      .catch(console.log);
  }

  render() {
    const { children } = this.props;
    return (
      <ContextLayout.Provider
        value={{
          configs: {
             ...themeConfig,
              lang: definedLan
             },
          ...this.state,
          fullLayout: FullLayout,
          bookingLayout: BookingLayout,
        }}
      >
        {children}
      </ContextLayout.Provider>
    );
  }
}

export { Layout, ContextLayout };
