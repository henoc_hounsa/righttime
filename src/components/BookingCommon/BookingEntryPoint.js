import React, { Component, useState } from "react";
import axios from "axios";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Loader from "react-loader-spinner";
import "../BookingCommon/Main.css";
import apiUrl from "../../datas/apiUrl";
import BookingService from "../BookingService/BookingService";
import ToastMessage from "../BookingCommon/ToastMessage";
import { withTranslation } from "react-i18next";

class BookingEntryPoint extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false,
      loading: true,
      appointmentDetails: this.getAppointmentDetails(),
    };
  }

  componentDidMount() {
    var accountUrl = apiUrl.alias;
    console.log("domain: ", accountUrl);
    axios
      .get(
        apiUrl.serverUrl + apiUrl.getBookingData + "?url=" + accountUrl,
        apiUrl.config
      )
      .then((response) => {
        
        const datas = {
          scheduleData: response.data.schedule,
          alias: response.data.alias,
          companyName: response.data.companyName,
        };
        this.setState({
          loading: false,
          appointmentDetails: this.getAppointmentDetails(datas),
        });
      })
      .catch((error) => {
        console.log(error);
        let errorMessage = error.name + ": " + error.message;
        if (error.response != null) {
          errorMessage = "Error: " + error.response.data.message;
        }
        toast.error(
          <ToastMessage msg={errorMessage} icon="fa fa-times-circle" />
        );
        this.setState({ loading: false, hasError: true });
      });
  }

  getAppointmentDetails(datas = null) {
    return {
      schedule: datas ? datas.scheduleData : "",
      xpUserAlias: datas ? datas.alias : "",
      companyName: datas ? datas.companyName : "",
      accountUrl: apiUrl.alias,
      service: "",
      agent: "",
      agentFirstName: "",
      agentLastName: "",
      agentEmailAddress: "",
      appointmentDateRequest: new Date(),
      timeSlot: "",
      slots: [],
      firstName: "",
      lastName: "",
      phoneNumber: "",
      emailAddress: "",
    };
  }

  render() {
    const { t } = this.props;
    let { loading, hasError } = this.state;

    return (
      <div>
        {loading ? (
          <div className="loading-container main-message">
            <Loader type="ThreeDots" color="#00BFFF" height={100} width={100} />
          </div>
        ) : hasError ? (
          <div className="main-message">
            <label>
              <strong>{t("site_unavailable_msg_one")}</strong>
            </label>
            <br />
            <label>{t("site_unavailable_msg_two")}</label>
            <br />
            <small>{t("site_unavailable_msg_three")}</small>
            <br />
            <small>{t("site_unavailable_msg_four")}</small>
          </div>
        ) : (
          <BookingService
            appointmentDetails={this.state.appointmentDetails}
            handleLanguage={this.props.handleLanguage}
          />
        )}
      </div>
    );
  }
}

export default withTranslation()(BookingEntryPoint);
