import React, { Component } from "react";
import {Container } from "react-bootstrap";
import apiUrl from "../../datas/apiUrl";
import "./CompanyHeader.css";
import { withTranslation } from 'react-i18next';

class CompanyHeader extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { t } = this.props;
        var headerText = "";
        var subHeaderText = "";
        var showSubText = true;
        if(this.props.locale) {
            localStorage.setItem("rightTime_lang", this.props.locale);
        }
        if(this.props.schedule && this.props.schedule.customCompanyDetails) {
            if(localStorage.getItem("rightTime_lang") === "fr") {
                headerText = this.props.schedule.customCompanyDetails.fr.headingText ? this.props.schedule.customCompanyDetails.fr.headingText : this.props.schedule.customCompanyDetails.en.headingText;
                subHeaderText = this.props.schedule.customCompanyDetails.fr.subHeadingText ? this.props.schedule.customCompanyDetails.fr.subHeadingText : this.props.schedule.customCompanyDetails.en.subHeadingText;
            } else {
                headerText = this.props.schedule.customCompanyDetails.en.headingText ? this.props.schedule.customCompanyDetails.en.headingText : this.props.schedule.customCompanyDetails.fr.headingText;
                subHeaderText = this.props.schedule.customCompanyDetails.en.subHeadingText ? this.props.schedule.customCompanyDetails.en.subHeadingText : this.props.schedule.customCompanyDetails.fr.subHeadingText;
            }
        }

        if(headerText === "") {
            headerText = t('company_header.heading') + this.props.schedule.owner.company;
        }

        if(subHeaderText === "") {
            subHeaderText = t('company_header.sub_heading');
            showSubText = this.props.page != "cancel";
        }

        var alignmentClass = this.props.alignment === "left" ? "company-header-panel-left" : "company-header-panel";
        return(
            <Container>
                <div id={this.props.id} className={alignmentClass}>
                    <h5 className="company-header-text">{headerText}</h5>
                    {showSubText ? <div className="company-sub-header-container"><span className="company-sub-header-text">{subHeaderText}</span></div> : "" }
                </div>
            </Container>
        );

    }

}

export default withTranslation()(CompanyHeader);