import React, { Component } from "react";
import {Container } from "react-bootstrap";
import "./CompanyFooter.css";

class CompanyFooter extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const footer = this.props.schedule.customCompanyDetails && this.props.schedule.customCompanyDetails.en.footerText ? this.props.schedule.customCompanyDetails.en.footerText : "";
        return(
            <Container>
                <div className="company-footer">
                   {footer}
                </div>
            </Container>
        );

    }

}

export default CompanyFooter;