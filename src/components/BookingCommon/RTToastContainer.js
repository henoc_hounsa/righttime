import React, { Component } from "react";
import {css} from "glamor";
import {toast, ToastContainer} from "react-toastify";

class RTToastContainer extends Component {

    render() {
        return(
            <div>
                <ToastContainer
                            autoClose={5000}
                            position="top-right"
                            hideProgressBar
                            className="rt-toast-container"
                            bodyClassName={css({
                                background: "rgba(0,0,0,0)",
                                backgroundColor: "rgba(0,0,0,0)",
                            })}
                            progressClassName={css({
                                background: "black",
                                height: 2
                            })}
                            closeButton={<svg style={{position: 'absolute', right: 20, top: 22, marginLeft: "50px"}} xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                <path id="Icon_material-cancel" data-name="Icon material-cancel" d="M11,3a8,8,0,1,0,8,8A7.993,7.993,0,0,0,11,3Zm4,10.872L13.872,15,11,12.128,8.128,15,7,13.872,9.872,11,7,8.128,8.128,7,11,9.872,13.872,7,15,8.128,12.128,11Z" transform="translate(-3 -3)" fill="#fea128"/>
                            </svg>}
                        />
            </div>
        );
    }
}


export default RTToastContainer;

