import React, { Component } from "react";
import { Col, Row, Container } from "react-bootstrap";
import apiUrl from "../../datas/apiUrl";
import bookingConstants from "../../constants/bookingConstants";
import "./Brand.css";
import { withTranslation } from 'react-i18next';

class Brand extends Component {

    render() {
        const { t } = this.props;
        var step = this.props.step;
        var brandImagePath = "/assets/booking/brand-img.png";
        var serviceProviderPageIsFirst = true;
        if(this.props.schedule) {
            if(this.props.schedule.customDisplaySettings && this.props.schedule.customDisplaySettings.customFlow === true) {
                serviceProviderPageIsFirst = this.props.schedule.customDisplaySettings.customizeFlow[1] === bookingConstants.SERVICE_PROVIDER;
            }
            if(this.props.schedule.customDisplaySettings && this.props.schedule.customDisplaySettings.brandImagePath) {
                brandImagePath = apiUrl.xpCdn.replace("{{imagepath}}", this.props.schedule.customDisplaySettings.brandImagePath);
            }
         }

        return(
            <Container className="brand-container">
                <Row className="brand-panel">
                    <Col md={6} className={'brandColFirst'}>
                        <div className={'step-image'}>
                            <img className="brand-image" src={brandImagePath} />
                        </div>
                    </Col>
                    <Col md={6} className="step-panel-origin">
                        <div className="step-panel">
                            <span className="step-panel-span">
                            <label><span className="step-label">{t('service.step_start_title')}</span> <label className="steps">{step} {t('service.step_end_title')} 4</label></label><br />
                            <label id="stepProgressLabelId" className={this.props.step == "1" ? "step-panel-current-step" : this.props.step > 1 ? "step-panel-past-step" : ""} >{t('service.step_one')}</label><br />
                            <label id="stepProgressLabelId" className={this.props.step == "2" ? "step-panel-current-step" : this.props.step > 2 ? "step-panel-past-step" : ""}>
                                    {serviceProviderPageIsFirst ? t('service.step_two') : t('service.step_three') }
                             </label><br />
                            <label id="stepProgressLabelId" className={this.props.step == "3" ? "step-panel-current-step" : this.props.step > 3 ? "step-panel-past-step" : ""}>
                                     {serviceProviderPageIsFirst ? t('service.step_three') : t('service.step_two') }
                            </label><br />
                            <label id="stepProgressLabelId" className={this.props.step == "4" ? "step-panel-current-step" : this.props.step > 4 ? "step-panel-past-step" : ""}>{t('service.step_four')}</label><br />
                            </span>
                        </div>
                    </Col>
                </Row>
            </Container>
        );
    }

}

export default withTranslation()(Brand);
