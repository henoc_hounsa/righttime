import React from "react";
import styles from "./Footer.module.css";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Image from "react-bootstrap/Image";
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withTranslation } from "react-i18next";

const Footer = props => (
  <div id="footerId" className={styles.Footer} data-testid="Footer">
    <Navbar
      bg="light"
      expand="lg"
      sticky={props.static}
      style={{
        minHeight: 84,
        border: "solid .75px #E4E4E4",
        backgroundColor: "#FAFAFA",
        fontSize: 13,
        color: "#808080"
      }}
    >
      <Navbar.Brand href="/" style={{ paddingTop: 0 }}>
        <span style={{ fontSize: 13, color: "#0089E1" }}>RightCom © 2020</span>
      </Navbar.Brand>
      <Nav className="mr-auto footerlinks">
      </Nav>
      <Nav className="mr-0">
        <Nav.Link href={"http://right-com.com"}>
          {props.t("prod_of")}{" "}
          <Image
            style={{ marginLeft: 5, marginBottom: 3 }}
            src="/assets/rightcom.svg"
            fluid
          />
        </Nav.Link>
      </Nav>
    </Navbar>
  </div>
);

Footer.propTypes = {};

Footer.defaultProps = {};

const mapDispatchToProps = dispatch => {
    return {
        disableLanguageButton: () => {
            dispatch({type: "UPDATE_LANGUAGE_BUTTON", value: true});
        }
    };
};

export default  compose(withTranslation(), connect(null, mapDispatchToProps))(Footer);
