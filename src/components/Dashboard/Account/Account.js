import React, { useCallback, useState } from "react";
import "./Account.module.css";
import {
  Button,
  Card,
  FormControl,
  InputGroup,
  Row,
  Col,
  Form,
  DropdownButton,
  Dropdown,
  Table,
  Tab,
  Nav
} from "react-bootstrap";
import "react-toastify/dist/ReactToastify.css";
import AccountDetails from "../account/AccountDetails/AccountDetails";
import AccountManagers from "../account/AccountManagers/AccountManagers";
import AccountScheduleUrl from "../account/AccountScheduleUrl/AccountScheduleUrl";
function Account(props) {
  return (
    <div className={"Account"} data-testid="Account">
      <h4>Account Settings</h4>
      <div className={"d-block d-sm-block d-md-none"}>
        <Tab.Container id="accountTabs" defaultActiveKey="first">
          <Row>
            <Col style={{ overflow: "auto" }}>
              <Nav
                variant="tabs"
                className="justify-content-center"
                style={{ width: "max-Content", margin: "auto" }}
              >
                <Nav.Item>
                  <Nav.Link eventKey="first">Account Details</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="second">Account Managers</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="third">Scheduling Page URL</Nav.Link>
                </Nav.Item>
              </Nav>
            </Col>
          </Row>
          <Row
            style={{
              paddingTop: 15,
              borderTop: "0.75px solid rgb(228, 228, 228)"
            }}
          >
            <Col>
              <Tab.Content>
                <Tab.Pane eventKey="first">
                  <AccountDetails />
                </Tab.Pane>
                <Tab.Pane eventKey="second">
                  <AccountManagers />
                </Tab.Pane>
                <Tab.Pane eventKey="third">
                  <AccountScheduleUrl />
                </Tab.Pane>
              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </div>
      <div className={"d-none d-sm-none d-md-block"}>
        <AccountDetails />
        <AccountManagers />
        <AccountScheduleUrl />
      </div>
    </div>
  );
}

Account.propTypes = {};

Account.defaultProps = {};

export default Account;
