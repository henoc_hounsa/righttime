import React, {useState} from "react";
import "./AccountDetails.css";
import {Button, Card, Col, Dropdown, DropdownButton, Form, FormControl, InputGroup, Row} from "react-bootstrap";
import countries from "../../../../datas/coutries.json";
import Select from "react-select";

function AccountDetails() {
  const [phoneFleg, setTelFlag] = useState("/assets/uk.svg");
  const telFlag = (
    <div
        style={{
          display: "inline-block",
          flexDirection: "inline-row",
          alignItems: "center"
        }}
    >
      <img
          alt=""
          src={phoneFleg}
          width="30"
          height="22"
          className="d-inline-block align-top"
      />
    </div>
  );
  const options = [
    { value: "chocolate", label: "Chocolate" },
    { value: "strawberry", label: "Strawberry" },
    { value: "vanilla", label: "Vanilla" }
  ];
  const getSet = e => {
    setTelFlag(e);
  };

  return (
    <div className="AccountDetails" id="AccountDetails">
      <Card style={{ marginBottom: 15 }}>
        <Card.Body>
          <Card.Title
            style={{
              color: "#373737",
              fontSize: 20,
              fontWeight: "bold"
            }}
          >
            Account details
          </Card.Title>
          <div>
            <Row>
              <Col sm={6}>
                <Form.Group controlId="">
                  <Form.Control type="text" placeholder="Company Name" />
                </Form.Group>
                <Form.Group controlId="">
                  <Form.Control type="text" placeholder="SMS Sender Name" />
                </Form.Group>
                <InputGroup className="mb-3">
                  <DropdownButton
                    as={InputGroup.Prepend}
                    variant="outline-secondary"
                    title={telFlag}
                    id="input-group-dropdown-1"
                  >
                    {countries.map(c => {
                      return (
                        <Dropdown.Item
                          href="#"
                          key={c.alpha3Code}
                          eventKey={c.flag}
                          onSelect={getSet}
                        >
                          <img src={c.flag} alt="" height={16} width={32} />{" "}
                          {c.name}
                        </Dropdown.Item>
                      );
                    })}
                  </DropdownButton>
                  <FormControl
                    aria-describedby="basic-addon1"
                    type="tel"
                    placeholder="Phone Number"
                  />
                </InputGroup>
                <Row>
                  <Col sm={8}>
                    <Select
                      options={countries.map(o => {
                        let O = {};
                        O.label = o.name;
                        O.value = o.area;
                        return O;
                      })}
                      placeholder="Country"
                    />
                  </Col>
                  <Col sm={4}>
                    <Form.Group controlId="">
                      <Form.Control type="text" placeholder="Zip Code" />
                    </Form.Group>
                  </Col>
                </Row>
                <Form.Group controlId="">
                  <Form.Control
                    type="text"
                    placeholder="Company Primary Address"
                  />
                </Form.Group>
                <Row>
                  <Col sm={6}>
                    <Form.Group controlId="">
                      <Form.Control type="text" placeholder="City" />
                    </Form.Group>
                  </Col>
                  <Col sm={6}>
                    <Select options={options} placeholder="State" />
                  </Col>
                </Row>
                <Form.Group controlId="">
                  <Form.Control type="email" placeholder="Email Address" />
                </Form.Group>
                <Form.Group controlId="">
                  <Form.Control type="url" placeholder="Website" />
                </Form.Group>
              </Col>
              <Col sm={6}>
                <Card>
                  <div style={{ padding: 15 }}>
                    <Card.Title
                      style={{
                        color: "#808080",
                        fontSize: 20,
                        fontWeight: "bold"
                      }}
                    >
                      Global Présences
                    </Card.Title>
                    <Select
                      options={countries.map(o => {
                        let O = {};
                        O.label = o.name;
                        O.value = o.area;
                        return O;
                      })}
                      isMulti
                      placeholder="Search"
                      closeMenuOnSelect={false}
                    />
                  </div>
                </Card>
              </Col>
            </Row>
          </div>
          <Button className={"btn btn-primary btn-lg"} href="#">
            EDIT
          </Button>
        </Card.Body>
      </Card>
    </div>
  );
}

export default AccountDetails;
