import React from 'react';
import {render} from '@testing-library/react';

import AccountDetails from './AccountDetails';

test('render component', () => {

        	const { getByText } = render(<AccountDetails />);

        	const linkElement = getByText(/AccountDetails/i);

        	expect(linkElement).toBeInTheDocument();

        });
