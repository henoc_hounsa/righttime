import React from 'react';
import {render} from '@testing-library/react';

import AccountManagers from './AccountManagers';

test('render component', () => {

        	const { getByText } = render(<AccountManagers />);

        	const linkElement = getByText(/AccountManagers/i);

        	expect(linkElement).toBeInTheDocument();

        });
