import React from "react";
import "./AccountManagers.css";
import {Badge, Button, Card, Table} from "react-bootstrap";

function AccountManagers() {
  return (
    <div className="AccountManagers" id="AccountManagers">
      <Card style={{ marginBottom: 15 }}>
        <Card.Body>
          <Card.Title
            style={{
              color: "#373737",
              fontSize: 20,
              fontWeight: "bold"
            }}
          >
            Account Managers
          </Card.Title>
          <div>
            <div style={{ overflow: "auto" }}>
              <Table bordered hover size="sm"  className={"superTable"}>
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Email Address</th>
                    <th>Mobile Number</th>
                    <th>Role</th>
                    <th>Location</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      Adebukunmi Akmande{" "}
                      <Badge
                        style={{
                          color: "#38C800",
                          background: "#38C80012"
                        }}
                        variant="success"
                      >
                        OWNER
                      </Badge>
                    </td>
                    <td>jamesbright@mail.com</td>
                    <td>(+234) 8011111678</td>
                    <td>@Super Administrator</td>
                    <td>Lagos, Nigeria</td>
                  </tr>
                  <tr>
                    <td>Rebecca Rigabalokomolowo</td>
                    <td>anthony@mail.com</td>
                    <td>(+229) 8111111111</td>
                    <td>Administrator</td>
                    <td>Cotonou, Benin Republic</td>
                  </tr>
                </tbody>
              </Table>
            </div>
          </div>
          <Button className={"btn btn-primary btn-lg"}>
            Transfer ownership
          </Button>
        </Card.Body>
      </Card>
    </div>
  );
}

export default AccountManagers;
