import React from 'react';
import {render} from '@testing-library/react';

import AccountScheduleUrl from './AccountScheduleUrl';

test('render component', () => {

        	const { getByText } = render(<AccountScheduleUrl />);

        	const linkElement = getByText(/AccountScheduleUrl/i);

        	expect(linkElement).toBeInTheDocument();

        });
