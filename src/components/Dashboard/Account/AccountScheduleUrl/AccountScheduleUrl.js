import React, { useCallback, useState } from "react";
import "./AccountScheduleUrl.css";
import {
  Button,
  Card,
  FormControl,
  InputGroup,
  Row,
  Col,
  Form,
  DropdownButton,
  Dropdown,
  Table,
  Tab,
  Nav
} from "react-bootstrap";
import { useDispatch, useMappedState } from "redux-react-hook";
import { toast, ToastContainer } from "react-toastify";

const toastList = new Set();
const MAX_TOAST = 3;

function notify(message) {
  let toastIdToDismiss = null;
  if (toastList.size === MAX_TOAST) {
    const arr = Array.from(toastList);
    const toastId = arr[0];
    if (toastId) {
      toastIdToDismiss = toastId;
    }
    // toast.dismiss(toastId);
  }

  const id = toast(message, {
    // I'm using the onClose hook here to remove the id
    // from the set
    onClose: () => toastList.delete(id),
    onOpen: () => {
      if (toastIdToDismiss !== null) {
        setTimeout(() => {
          toast.dismiss(toastIdToDismiss);
        }, 1000);
      }
    }
  });
  toastList.add(id);
}

const useStateWithLocalStorageUrl = localStorageKey => {
  const [url, setUrl] = React.useState(
    localStorage.getItem(localStorageKey) || ""
  );
  React.useEffect(() => {
    localStorage.setItem(localStorageKey, url);
  }, [url]);
  return [url, setUrl];
};

function AccountScheduleUrl() {
  const dispatch = useDispatch();
  const store = useCallback(state => state, []);
  const statestore = useMappedState(store);
  const [show, setShow] = useState(false);
  const [url, setUrl] = useStateWithLocalStorageUrl("rightTime_url");
  const [scurl, setScurl] = useState("");
  const [isValid, setValidity] = useState(false);
  const [isInvalid, setInvalidity] = useState(false);

  const verifUrl = e => {
    if (e.target.value.match("^[a-zA-Z][a-zA-Z0-9]*$") != null) {
      setValidity(true);
      setInvalidity(false);
      //setUrl(e.target.value.toLowerCase());
      setScurl(e.target.value.toLowerCase());
    } else {
      setValidity(false);
      setInvalidity(true);
    }
  };
  return (
    <div className="AccountScheduleUrl" id="AccountScheduleUrl">
      {url ? (
        <Card>
          <Card.Body>
            <Card.Title
              style={{
                color: "#373737",
                fontSize: 20,
                fontWeight: "bold"
              }}
            >
              URL to my Scheduling Page
            </Card.Title>
            <Card.Text>
              Your clients can access your scheduling page on{" "}
              <a
                style={{ color: "#0089E1", fontWeight: "bold" }}
                target="_blank"
                href="/schedule"
              >
                https://{url}.righttime.com
              </a>
              <input
                id="schedulingUrl"
                value={"https://" + url + ".righttime.com"}
                style={{
                  zIndex: -99,
                  position: "absolute",
                  bottom: 0,
                  left: 0
                }}
                readOnly
              />
            </Card.Text>
            <Card.Link
              style={{ color: "#0089E1", fontWeight: "bold" }}
              href="#"
              onClick={() => {
                var copyText = document.querySelector("#schedulingUrl");
                copyText.select();
                copyText.setSelectionRange(0, 99999);
                document.execCommand("copy");
                notify("Scheduling URL copied!");
              }}
            >
              COPY URL
            </Card.Link>
          </Card.Body>
        </Card>
      ) : (
        <Card>
          <Card.Body>
            <Card.Title
              style={{
                color: "#373737",
                fontSize: 20,
                fontWeight: "bold"
              }}
            >
              URL to my Scheduling Page
            </Card.Title>
            <div>
              <div>
                <p>
                  This is where your clients schedule a time with you. It is
                  advisable to choose a URL that is succint and easy to
                  remember.
                </p>
                <div>
                  <InputGroup size="lg">
                    <InputGroup.Prepend>
                      <InputGroup.Text id="inputGroup-sizing-lg">
                        https://
                      </InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl
                      aria-label="Large"
                      aria-describedby="inputGroup-sizing-sm"
                      isValid={isValid}
                      isInvalid={isInvalid}
                      onChange={verifUrl}
                    />
                    <InputGroup.Append>
                      <InputGroup.Text>.righttime.com</InputGroup.Text>
                    </InputGroup.Append>
                  </InputGroup>
                </div>
                {url && isValid ? (
                  <small className={"text-success"}>
                    Here is what yout url looks like{" "}
                    <strong>https://{url}.righttime.com</strong>
                  </small>
                ) : (
                  ""
                )}
                {isInvalid ? (
                  <div>
                    <small className={"text-danger"}>
                      This URL is not available, please try again using another
                      unique name.
                    </small>
                  </div>
                ) : (
                  ""
                )}
                <small>
                  Note: Your custom URL must contain 3-100 letters or numbers.
                  Please do not use spaces, symbols or special characters.
                </small>
              </div>
              <Button
                onClick={() => {
                  setUrl(scurl);
                }}
              >
                SAVE
              </Button>
            </div>
          </Card.Body>
        </Card>
      )}
      <ToastContainer
        position="bottom-left"
        hideProgressBar={true}
        autoClose={4000}
      />
    </div>
  );
}

export default AccountScheduleUrl;
