import React from 'react';
import { cleanup, render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Account from './Account';

describe('<Account />', () => {
  afterEach(cleanup);

  test('it should mount', () => {
    const { getByTestId } = render(<Account />);
    const account = getByTestId('Account');

    expect(account).toBeInTheDocument();
  });
});