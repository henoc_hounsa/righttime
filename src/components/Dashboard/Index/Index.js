import React from "react";
import styles from "./Index.module.css";

const Index = () => (
  <div className={styles.Index} data-testid="Index">
    Index Component
  </div>
);

Index.propTypes = {};

Index.defaultProps = {};

export default Index;
