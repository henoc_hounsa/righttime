import React from 'react';
import { cleanup, render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Index from './Index';

describe('<Index />', () => {
  afterEach(cleanup);

  test('it should mount', () => {
    const { getByTestId } = render(<Index />);
    const index = getByTestId('Index');

    expect(index).toBeInTheDocument();
  });
});