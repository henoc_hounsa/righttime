import React, {useCallback, useEffect, useRef, useState} from "react";
import "./Dashboard.module.css";
import Footer from "../Footer/Footer";
import Topnavbar from "../Topnavbar/Topnavbar";
import Sidebar from "../dashboard/Sidebar/Sidebar";
import {Route, Switch, useRouteMatch} from "react-router-dom";
import {Container, Row} from "react-bootstrap";
import useHttpService from "../../services/HttpService";
import {useDispatch} from "redux-react-hook";
import Account from "../dashboard/Account/Account";
import ServiceList from "../dashboard/services/ServiceList/ServiceList";
import ServiceForm from "../dashboard/services/ServiceForm/ServiceForm";
import Loading from "../Loading/Loading";

function Dashboard(props) {
  localStorage.setItem("rightTime_dash", true);
  let match = useRouteMatch();
  const hasMount = useRef(false);
  const unmounted = useRef(false);
    const [stateCheckSession, setStateCheckSession] = useState("loading");
    const [loading, setLoading] = useState(false);
    const {
        send: sendRequestCheckSession,
        data: sessionResponse,
        loading: onLoadingSession,
        error: onErrorSession
    } = useHttpService("CHECK_SESSION");
  const dispatch = useDispatch();
  const updateUserInfo = useCallback(
    user => dispatch({ type: "UPDATE_USER_INFO", value: user }),
    [dispatch]
  );
  const updateUserIsConnected = useCallback(
    status => dispatch({ type: "UPDATE_USER_IS_CONNECTED", value: status }),
    [dispatch]
  );
  const updateUserFullName = useCallback(
    fullName => dispatch({ type: "UPDATE_USER_FULLNAME", value: fullName }),
    [dispatch]
  );

  useEffect(() => {
    if (!hasMount.current) {
      // componentDidMount
      checkSession();
      hasMount.current = true;
    } else {
      // componentDidUpdate
    }
    return () => {
      unmounted.current = true;
      // componentWillUnmount
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // CheckSession submit result hook
  useEffect(() => {
    if (!unmounted.current) {
      if (!onLoadingSession && onErrorSession) {
        setStateCheckSession("error");
      }
      if (!onLoadingSession && sessionResponse) {
        if (sessionResponse.status === 200) {
          // user session is ok
          // set user session
          updateUserInfo(sessionResponse.user);
          updateUserFullName(
            sessionResponse.user.firstname + " " + sessionResponse.user.lastname
          );
          updateUserIsConnected(true);
          setStateCheckSession("success");
          //props.onLoaded(true);
        } else if (sessionResponse.status === 201) {
          // user session is ok
          // set user session
          updateUserInfo(sessionResponse.user);
          updateUserFullName(
            sessionResponse.user.firstname + " " + sessionResponse.user.lastname
          );
          updateUserIsConnected(true);
          setStateCheckSession("success");
          //props.onLoaded(true);
        } else {
          setStateCheckSession("error");
        }
      }
    }
  }, [
    onLoadingSession,
    onErrorSession,
    sessionResponse,
    props,
    updateUserInfo,
    updateUserFullName,
    updateUserIsConnected
  ]);

  function checkSession() {
    sendRequestCheckSession();
  }
  return (
    <div className={"Dashboard"} data-testid="Dashboard">
      <Topnavbar fixed={"top"} brandHref="/dashboard" dashMenu={true} />
      <Switch>
        <Route path={`${match.path}`}>
          <div style={{ height: 64 }}></div>
          <Container fluid>
            <Row>
              <div className={"dashShell"} id="wrapper">
                <Sidebar />
                <div
                  className={"dashContent"}
                  style={{
                    background: "#FAFBFE 0% 0% no-repeat padding-box"
                  }}
                  id="page-content-wrapper"
                >
                  <Route path={`${match.path}/settings/account`}>
                    <Account />
                  </Route>
                  <Route exact path={`${match.path}/settings`}>
                  </Route>
                  <Route exact path={`${match.path}/resource/services/edit/:id`}>
                    edit service
                  </Route>
                  <Route exact path={`${match.path}/resource/services/add`}>
                    <ServiceForm />
                  </Route>
                  <Route exact path={`${match.path}/resource/services`}>
                    <ServiceList/>
                  </Route>
                    <Route exact path={`${match.path}/resource`}>
                    </Route>
                </div>
              </div>
            </Row>
          </Container>
        </Route>
      </Switch>
        <Footer/>
        <div hidden={!loading} id={"loadingContainer"}>
            <Loading/>
        </div>
    </div>
  );
}

Dashboard.propTypes = {};

Dashboard.defaultProps = {};

export default Dashboard;
