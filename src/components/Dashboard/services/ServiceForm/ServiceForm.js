import React, {useState} from 'react';
import './ServiceForm.css';
import {
    Button,
    Card,
    Col,
    Dropdown,
    DropdownButton,
    Form,
    FormControl,
    InputGroup,
    Nav,
    Row,
    Tab
} from "react-bootstrap";
import CreatableSelect from "react-select/creatable";
import categories from "../../../../datas/categories";
import Select from 'react-select';
import appointmentTypes from '../../../../datas/appointmentTypes';
import businessLocations from '../../../../datas/businessLocations';
import currencies from '../../../../datas/currencies';
import initiators from '../../../../datas/initiators';
import agents from '../../../../datas/agents';
import {FaArrowLeft, FaArrowRight} from "react-icons/fa";

function ServiceForm() {
    const [isLoading, setIsLoading] = useState(false);
    const [cat, enableCat] = useState(true);
    const [tab, enableTab] = useState("first");
    const [desc, enableDesc] = useState(true);
    const [durationUnit, setDurationUnit] = useState("Minutes");
    const [currency, setCurrency] = useState("$");

    let service = {};
    const [serviceName, setServiceName] = useState("");
    const [serviceCat, setServiceCat] = useState("");
    const [serviceDesc, setServiceDesc] = useState("");
    const [serviceDur, setServiceDur] = useState("");
    const [serviceLoc, setServiceLoc] = useState("");
    const [serviceAppointType, setServiceAppointType] = useState("");
    const [serviceImage, setServiceImage] = useState("");
    const [serviceCost, setServiceCost] = useState("");
    const [serviceInitiator, setServiceInitiator] = useState("");
    const [serviceDelivery, setServiceDelivery] = useState("");

    function doCreate(e) {
        //console.log(e)
        setIsLoading(true);

        service.active = true;
        service.name = serviceName;
        service.category = serviceCat;
        service.description = serviceDesc;
        service.duration = serviceDur;
        service.durationUnit = durationUnit;
        service.location = serviceLoc;
        service.appointmentType = serviceAppointType;
        service.imagePath = serviceImage;
        service.cost = serviceCost;
        service.costCurency = currency;
        service.delivery = serviceDelivery;

        document.getElementById("loadingContainer").classList.add("showLoading");
        setTimeout(()=>{
            //document.getElementById("loadingContainer").classList.remove("showLoading");
            window.location = "/dashboard/resource/services"
        },3000)
        console.log(service)
    }

    const uploadImage = (e) => {
        setServiceImage(e.target.value);
        console.log(e)
    };

    return (
        <div className="ServiceForm" id="ServiceForm">
            <div className="row" style={{marginBottom: 16}}>
                <div className={"col-sm-5"}>
                    <h2
                        style={{
                            color: "#373737",
                            fontSize: 20,
                            fontWeight: "bold"
                        }}
                    >
                        Services
                    </h2>
                </div>
                <div className={"col-sm-4"}>

                </div>
                <div className={"col-sm-3"}>
                </div>

            </div>

            <Card style={{marginBottom: 15}}>
                <Card.Body>
                    <Tab.Container id="left-tabs-example" defaultActiveKey={tab} activeKey={tab}>
                        <Row>
                            <Col sm={3} style={{marginTop: "32px", height: "50%"}}>
                                <img src="/assets/services/side-image.svg" className={"float-right"}
                                     style={{width: "auto", maxWidth: "100%"}} alt=""/>
                            </Col>
                            <Col sm={2}>
                                <div style={{marginTop: "100px"}}>
                                    <div>Setp <strong>1 of 2</strong></div>
                                    <Nav variant="" className="flex-column tabsNavService">
                                        <Nav.Item>
                                            <Nav.Link eventKey="first">Service Details</Nav.Link>
                                        </Nav.Item>
                                        <Nav.Item>
                                            <Nav.Link eventKey="second">Other Options</Nav.Link>
                                        </Nav.Item>
                                    </Nav>
                                </div>
                            </Col>
                            <Col sm={7} md={5}>
                                <Tab.Content>
                                    <Tab.Pane eventKey="first">
                                        <Card className={"shadow"}>
                                            <Card.Header style={{background: "none"}}>
                                                Create a new Service
                                            </Card.Header>
                                            <Card.Body>
                                                <div>
                                                    <Form.Group controlId="x">
                                                        <Form.Check
                                                            type="switch"
                                                            id="custom-switch"
                                                            label="Assign service to categoty"
                                                            onClick={() => {
                                                                enableCat(!cat)
                                                            }}
                                                        />
                                                    </Form.Group>
                                                </div>
												<div hidden={cat}>
													<Form.Group controlId="x">
                                                        <Form.Label>Service category</Form.Label>
                                                        <CreatableSelect
                                                            options={categories}
                                                            isMulti
                                                            size={"sm"}
                                                            value={serviceCat}
                                                            onChange={(e) => {
                                                                setServiceCat(e)
                                                            }}
                                                        />
                                                    </Form.Group>
                                                </div>
                                                <Form.Group controlId="x">
                                                    <Form.Label>Service name</Form.Label>
                                                    <Form.Control type="text" placeholder="Enter the service name"
                                                                  onChange={(e) => {
                                                                      setServiceName(e.target.value)
                                                                  }}/>
                                                </Form.Group>
                                                <button hidden={!desc} className={"btn btn-link"} onClick={() => {
                                                    enableDesc(!desc)
                                                }}>Add Description
                                                </button>
                                                <div hidden={desc}>
                                                    <Form.Group controlId="x">
                                                        <Form.Label>Service Description</Form.Label>
                                                        <Form.Control as="textarea" onChange={(e) => {
                                                            setServiceDesc(e.target.value)
                                                        }} placeholder="Enter the service description"/>
                                                    </Form.Group>
                                                </div>
                                                    <div className={""}>
                                                        <Form.Group controlId="x" className={"specialInputGroup"} style={{width:210}}>
                                                            <Form.Label>Service duration</Form.Label>
                                                            <InputGroup>
                                                                <InputGroup.Prepend>
                                                                    <InputGroup.Text>
                                                                        <img src="/assets/services/duration.svg" alt=""/>
                                                                    </InputGroup.Text>
                                                                </InputGroup.Prepend>
                                                                <FormControl
                                                                    placeholder="30"
                                                                    type="number"
                                                                    onChange={(e) => {
                                                                        setServiceDur(e.target.value)
                                                                    }}
                                                                />

                                                                <DropdownButton
                                                                    as={InputGroup.Append}
                                                                    variant="outline-secondary"
                                                                    title={durationUnit}
                                                                    id="input-group-dropdown-2"
                                                                >
                                                                    <Dropdown.Item href="#" onClick={() => {
                                                                        setDurationUnit("Minutes")
                                                                    }}>Minutes</Dropdown.Item>
                                                                    <Dropdown.Item href="#" onClick={() => {
                                                                        setDurationUnit("Hours")
                                                                    }}>Hours</Dropdown.Item>
                                                                </DropdownButton>
                                                            </InputGroup>
                                                        </Form.Group>
                                                    </div>
                                                <div>
                                                    <Form.Group controlId="x">
                                                        <Form.Label>Service location</Form.Label>
                                                        <Select options={businessLocations} onChange={(e) => {
                                                            setServiceLoc(e)
                                                        }}/>
                                                    </Form.Group>
                                                </div>
                                                <div>
                                                    <Form.Group controlId="x">
                                                        <Form.Label>Applicable appointment type</Form.Label>
                                                        <Select options={appointmentTypes} onChange={(e) => {
                                                            setServiceAppointType(e)
                                                        }}/>
                                                    </Form.Group>
                                                </div>
                                            </Card.Body>
                                            <Card.Footer className="text-muted">
                                                <Button variant="primary" onClick={() => {
                                                    enableTab("second")
                                                }} className={"float-right"}>Next <FaArrowRight/></Button>
                                            </Card.Footer>
                                        </Card>
                                    </Tab.Pane>
                                    <Tab.Pane eventKey="second">
                                        <Card className={"shadow"}>
                                            <Card.Header style={{background: "none"}}>
                                                Create a new Service
                                            </Card.Header>
                                            <Card.Body>
                                                <div className={"serviceFormFile"}>
                                                    <Form.Group controlId="x">
                                                        <Form.Label>Service Image (optional)</Form.Label>
                                                        <Form.Control type="file" variant="primary"
                                                                      placeholder="Enter the service name"
                                                                      onChange={uploadImage}/>
                                                    </Form.Group>
                                                </div>
                                                <div className={"serviceFormCost"}>
                                                    <Form.Group controlId="x">
                                                        <Form.Label>Service cost (optional)</Form.Label>
                                                        <InputGroup>
                                                            <DropdownButton
                                                                as={InputGroup.Prepend}
                                                                variant="outline"
                                                                title={currency}
                                                                id="input-group-dropdown-1"
                                                            >
                                                                {currencies.map((c) => {
                                                                    return <Dropdown.Item key={c.value.symbol} href="#"
                                                                                          onClick={() => {
                                                                                              setCurrency(c.value.symbol)
                                                                                          }}>{c.value.name}</Dropdown.Item>
                                                                })}

                                                            </DropdownButton>
                                                            <FormControl
                                                                placeholder="20,000.00"
                                                                onChange={(e) => {
                                                                    setServiceCost(e.target.value)
                                                                }}
                                                                type={"number"}
                                                            />
                                                        </InputGroup>
                                                    </Form.Group>
                                                </div>
                                                <Form.Label>Schedule Initiator
                                                    <br/><small>Who can schedule an appointment with this
                                                        service?</small>
                                                </Form.Label>
                                                <div style={{display: "flex"}}>
                                                    {initiators.map(i => (
                                                        <div key={`custom-${i}`} style={{marginRight: 15}}>
                                                            <Form.Group controlId="x">

                                                                <Form.Check
                                                                    custom
                                                                    type="checkbox"
                                                                    id={`init-${i}`}
                                                                    label={i}
                                                                    onChange={(e) => {
                                                                        console.log(e.target)
                                                                    }}
                                                                />
                                                            </Form.Group>
                                                        </div>
                                                    ))}
                                                </div>
                                                <div>
                                                    <Form.Group controlId="x">
                                                        <Form.Label>Service Delivery</Form.Label>
                                                        <Select
                                                            options={agents}
                                                            getOptionLabel={(o) => o.name}
                                                            getOptionValue={(o) => o}
                                                            isMulti
                                                            onChange={(e) => {
                                                                setServiceDelivery(e)
                                                            }}
                                                        />
                                                    </Form.Group>
                                                </div>
                                            </Card.Body>
                                            <Card.Footer className="text-muted">
                                                <Button
                                                    variant="link-dark"
                                                    onClick={() => {enableTab("first")}}
                                                    className={"float-left"}>
                                                    <FaArrowLeft/> Back
                                                </Button>
                                                <Button
                                                    variant="primary"
                                                    className={"float-right"}
                                                    onClick={doCreate}
                                                >{isLoading ? 'Creating …' : 'Create'} </Button>
                                            </Card.Footer>
                                        </Card>
                                    </Tab.Pane>
                                </Tab.Content>
                            </Col>
                            <Col sm={1}>&nbsp;</Col>
                        </Row>
                    </Tab.Container>
                </Card.Body>
            </Card>
        </div>
    );
};

export default ServiceForm;

        