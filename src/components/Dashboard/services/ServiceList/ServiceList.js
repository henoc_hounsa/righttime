import React, {useCallback, useEffect, useMemo, useState} from 'react';
import './ServiceList.css';
import {Button, Card, FormControl, InputGroup, OverlayTrigger, Popover} from "react-bootstrap";
import services from "../../../../datas/services"
import {NavLink} from "react-router-dom";
import {FaBan, FaRegTrashAlt, FaSearch} from "react-icons/fa";
import DataTable from "react-data-table-component";
import {MdMoreVert} from "react-icons/all";


function ServiceList() {
	const [fs,filterServices] = useState("");
	const [selectAll,setSelectAll] = useState(false);
    const [selectedRows, setSelectedRows] = useState([]);

    useEffect(() => {

        console.log('state', selectedRows);
    }, [selectedRows]);

    const handleButtonClick = (e) => {

        console.log('clicked');
        console.log(e);
    };

    const handleChange = useCallback(state => {
        setSelectedRows(state.selectedRows);
    }, []);

    const columns = useMemo(() => [
        {
            name: 's/N',
            cell: row => {return row.index},
            sortable: true,
        },
        {
            name: 'Service',
            selector: 'label',
            sortable: true,
        },
        {
            name: 'Category',
            cell: row => row.category.map((e)=>{return e.label}),
            sortable: true,
        },
        {
            name: 'Duration',
            cell: row => {return row.duration.time+' '+row.duration.unit},
            sortable: true,
        },
        {
            name: 'Price',
            cell: row => {return row.price.amount+' '+row.price.curency},
            sortable: true,
        },
        {
            name: 'Status',
            selector: 'status',
            sortable: true,
        },{
            name:"Action",
            cell: () => <div>
                <OverlayTrigger
                    trigger="click"
                    key="left"
                    placement="left"
                    overlay={
                        <Popover id={`popover-positioned-left`}>
                            <Popover.Content>
                                <strong>Holy guacamole!</strong> Check this info.
                            </Popover.Content>
                        </Popover>
                    }
                >
                    <Button variant="link-dark"><MdMoreVert/></Button>
                </OverlayTrigger>
            </div>,
            ignoreRowClick: true,
            allowOverflow: true,
            button: true,
        }
    ], []);
    const contextActions = () => (
        <div
            color="secondary"
        >
            <Button variant={"lg-dark lg"}><FaBan/></Button>
            <Button variant={"lg-dark lg"}><FaRegTrashAlt/></Button>
        </div>
    );
    const [filterText, setFilterText] = useState('');
    const [resetPaginationToggle, setResetPaginationToggle] = useState(false);
    //const filteredServices = services.filter(item => item && item.toString().includes(filterText));
    const filteredServices = services.filter(item => item && JSON.stringify(item).includes(filterText));

    return (
        <div className="ServiceList" id="ServiceList">
            <div className="row" style={{marginBottom: 16}}>
                <div className={"col-sm-5"}>
                    <h2
                        style={{
                            color: "#373737",
                            fontSize: 20,
                            fontWeight: "bold"
                        }}
                    >
                        Services
                    </h2>
                </div>
                <div className={"col-sm-4"}>
                    <InputGroup className="mb-3">
                        <InputGroup.Prepend className={"border-0"} >
                            <Button variant="outline-secondary" className="border-right-0 bg-white text-dark" style={{borderColor:"#ccc"}}><FaSearch/></Button>
                        </InputGroup.Prepend>
                        <FormControl aria-describedby="basic-addon1" placeholder={"Search"} onChange={e => setFilterText(e.target.value )}/>
                    </InputGroup>
                </div>
                <div className={"col-sm-3"}>
                    <NavLink to={"services/add"} className="btn btn-lg btn-primary float-right">Add Service</NavLink>
                </div>

            </div>

            <Card style={{marginBottom: 15}}>
                <Card.Body>

                    <div>
                        <div style={{overflow: "auto"}}>
                            <DataTable
                                title={filteredServices.length + ' of '+services.length + " Services"}
                                data={filteredServices}
                                columns={columns}
                                onSelectedRowsChange={handleChange}
                                selectableRows
                                contextActions={contextActions()}
                                subHeaderComponent={
                                    (
                                        <div style={{ display: 'flex', alignItems: 'center' }}>
                                            <input id="outlined-basic" label="Search" variant="outlined" size="small" style={{ margin: '5px' }} />
                                        </div>
                                    )
                                }
                            />
                            <hr/>
                            {/*
                            <Table bordered hover size="sm" className={"superTable"}>
                                <thead>
                                <tr>
                                    <th>
                                        <Form.Check
                                            custom
                                            type={"checkbox"}
                                            id={"selectAll"}
                                            label={``}
                                            onChange={()=>{setSelectAll(!selectAll)}}
                                        />
                                    </th>
                                    <th>S/N</th>
                                    <th>Service</th>
                                    <th>Category</th>
                                    <th>Duration</th>
                                    <th>Price</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
								{services
									.filter((e) => e.label.includes(fs))
									.map((s, index)=>{
									return (<tr key={index}>
                                        <td style={{width:5}}>
                                            <Form.Check
                                                custom
                                                type={"checkbox"}
                                                id={`select-${s.id}`}
                                                label={``}
                                            />
                                        </td>
										<td style={{width:5}}>
											{index+1}
										</td>
										<td>{s.label}</td>
										<td>{s.category.map(c=>{return c.label})}</td>
										<td>{s.duration.time} {s.duration.unit}</td>
										<td>{s.price.amount} {s.price.curency}</td>
										<td>{s.status}</td>
                                        <td><NavLink to={"services/edit/"+s.id}>EDIT</NavLink></td>
									</tr>)
								})}
                                </tbody>
                            </Table>
                            */}
                        </div>
                    </div>
                </Card.Body>
            </Card>
        </div>
    );
};

export default ServiceList;

        