import React from 'react';
import {render} from '@testing-library/react';

import ServiceList from './ServiceList';

test('render component', () => {

        	const { getByText } = render(<ServiceList />);

        	const linkElement = getByText(/ServiceList/i);

        	expect(linkElement).toBeInTheDocument();

        });
