import React, {useState} from "react";
import "./Sidebar.module.css";
import {NavLink} from "react-router-dom";
import Collapse from "react-bootstrap/Collapse";
import {withNamespaces} from "react-i18next";
import menu from "../../../datas/dashboardMenu"

function Sidebar(props) {
  const [open, setOpen] = useState(false);
  const [subs,openSubs] = useState({})
  const ctoggleSidenav = () => {
    var element = document.getElementsByClassName("dashShell")[0];
    if (window.innerWidth < 768) {
      if (element.classList) {
        element.classList.toggle("toggled");
      } else {
        // For IE9
        var classes = element.className.split(" ");
        var i = classes.indexOf("toggled");

        if (i >= 0) classes.splice(i, 1);
        else classes.push("toggled");
        element.className = classes.join(" ");
      }
    }
  };
  return (
    <div
      className={"dashSidebar"}
      id="sidebar-wrapper"
      style={{ paddingTop: 20 }}
    >
      <ul className="sidebar-nav">
        {menu.map(m => {
          return (
            <li id={"li" + m.id} key={"li" + m.id}>
              <NavLink to={m.link} id={"a" + m.id} onClick={()=>{openSubs({["c"+m.id]:true})}}>
                <div>
                  <img src={m.icon} alt="" style={{ width: 18, height: 20 }} />
                  {props.t(m.label)}
                </div>
              </NavLink>
              {m.sub.length ? (
                <Collapse id={"c" + m.id} in={subs["c"+m.id]}>
                  <div>
                    {m.sub.map(s => {
                      return (
                        <div key={"a"+s.id}>
                          <NavLink onClick={ctoggleSidenav} to={s.link}>
                            <div>{s.label}</div>
                          </NavLink>
                        </div>
                      );
                    })}
                  </div>
                </Collapse>
              ) : (
                ""
              )}
            </li>
          );
        })}
      </ul>
    </div>
  );
}

Sidebar.propTypes = {};

Sidebar.defaultProps = {};

export default withNamespaces()(Sidebar);
