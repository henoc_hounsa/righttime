import React from "react";
import "./Hello.module.css";
import { Button, Col, Row } from "react-bootstrap";
import GetStore from "../../../getStore";

function Hello() {
  const storeData = GetStore();

  return (
    <div className={"Hello"} data-testid="Hello">
      <Row>
        <Col md={5}>
          <h1 style={{ fontSize: 36, fontWeight: "bold" }}>
            Hello{" "}
            {storeData.userIsConnected ? (
              storeData.userInfo.firstname
            ) : (
              <span>Salewa</span>
            )}
            !
          </h1>
          <p style={{ fontSize: 16, color: "#818181" }}>
            Great to have you on board!
          </p>
          <img
            src="/assets/onboarding/General/customer-support.jpg"
            alt=""
            style={{ width: 96, height: 96, borderRadius: "50%" }}
          />
          <div>
            <div className={"triangle"}></div>
            <div
              style={{
                fontSize: 10,
                maxWidth: 390,
                padding: 16,
                paddingTop: 30,
                paddingRight: 60,
                background:
                  "rgba(0, 137, 233, .05) 0% 0% no-repeat padding-box",
                marginBottom: 40
              }}
            >
              <p>I’m Sephora, Customer Success Associate.</p>
              <p>
                I’ll be guiding you through setting up your account and getting
                a hold of RightTime in minutes.
              </p>
            </div>
          </div>
          <Button
            href={"/onboarding/seturl"}
            size={"lg"}
            variant="primary active"
          >
            CONTINUE
          </Button>
        </Col>
        <Col md={1}></Col>
        <Col
          md={6}
          className={"d-none d-sm-none d-md-block d-lg-block d-xl-block"}
        >
          <img
            src="/assets/onboarding/Desktop/Group 839.svg"
            alt=""
            style={{ width: "100%", height: 322 }}
          />
        </Col>
      </Row>
    </div>
  );
}

Hello.propTypes = {};

Hello.defaultProps = {};

export default Hello;
