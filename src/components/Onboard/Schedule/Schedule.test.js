import React from 'react';
import { cleanup, render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Schedule from './Schedule';

describe('<Schedule />', () => {
  afterEach(cleanup);

  test('it should mount', () => {
    const { getByTestId } = render(<Schedule />);
    const schedule = getByTestId('Schedule');

    expect(schedule).toBeInTheDocument();
  });
});