import React, { useEffect, useState } from "react";
import { Button, Col, Form, Row, Table } from "react-bootstrap";
import Select from "react-select";
import CreatableSelect from "react-select/creatable";
import GetStore from "../../../getStore";

const useStateWithLocalStorageSname = localStorageKey => {
  const [sname, setSname] = React.useState(
    localStorage.getItem(localStorageKey) || ""
  );
  React.useEffect(() => {
    localStorage.setItem(localStorageKey, sname);
  }, [sname]);
  return [sname, setSname];
};
const useStateWithLocalStorageDays = localStorageKey => {
  const [sdays, setDays] = React.useState(
    JSON.parse(localStorage.getItem(localStorageKey)) || ""
  );
  React.useEffect(() => {
    localStorage.setItem(localStorageKey, JSON.stringify(sdays));
  }, [sdays]);
  return [sdays, setDays];
};
const useStateWithLocalStorageOnb = localStorageKey => {
  const [onb, setOnb] = React.useState(
    localStorage.getItem(localStorageKey) || "false"
  );
  React.useEffect(() => {
    localStorage.setItem(localStorageKey, onb);
  }, [onb]);
  return [onb, setOnb];
};
function Schedule(props) {
  const storeData = GetStore();
  const [sname, setSname] = useStateWithLocalStorageSname("rightTime_sname");
  const [sdays, setDays] = useStateWithLocalStorageDays("rightTime_days");
  const [onb, setOnb] = useStateWithLocalStorageOnb("rightTime_onb");
  const saveName = e => {
    setSname(e.target.value);
  };
  const hours = [
    { value: "12:00 AM", label: "12:00 AM" },
    { value: "01:00 AM", label: "01:00 AM" },
    { value: "02:00 AM", label: "02:00 AM" },
    { value: "03:00 AM", label: "03:00 AM" },
    { value: "04:00 AM", label: "04:00 AM" },
    { value: "05:00 AM", label: "05:00 AM" },
    { value: "06:00 AM", label: "06:00 AM" },
    { value: "07:00 AM", label: "07:00 AM" },
    { value: "08:00 AM", label: "08:00 AM" },
    { value: "09:00 AM", label: "09:00 AM" },
    { value: "10:00 AM", label: "10:00 AM" },
    { value: "11:00 AM", label: "11:00 AM" },
    { value: "12:00 PM", label: "12:00 PM" },
    { value: "01:00 PM", label: "01:00 PM" },
    { value: "02:00 PM", label: "02:00 PM" },
    { value: "03:00 PM", label: "03:00 PM" },
    { value: "04:00 PM", label: "04:00 PM" },
    { value: "05:00 PM", label: "05:00 PM" },
    { value: "06:00 PM", label: "06:00 PM" },
    { value: "07:00 PM", label: "07:00 PM" },
    { value: "08:00 PM", label: "08:00 PM" },
    { value: "09:00 PM", label: "09:00 PM" },
    { value: "10:00 PM", label: "10:00 PM" },
    { value: "11:00 PM", label: "11:00 PM" }
  ];
  const [scheduelName, setScheduleName] = useState("");

  const [monSelected, selectMon] = useState(false);
  const [tueSelected, selectTue] = useState(false);
  const [wedSelected, selectWed] = useState(false);
  const [thuSelected, selectThu] = useState(false);
  const [friSelected, selectFri] = useState(false);
  const [satSelected, selectSat] = useState(false);
  const [sunSelected, selectSun] = useState(false);

  const [monOpenH, setMonOpenH] = useState("");
  const [monCloseH, setMonCloseH] = useState("");
  const [tueOpenH, setTueOpenH] = useState("");
  const [tueCloseH, setTueCloseH] = useState("");
  const [wedOpenH, setWedOpenH] = useState("");
  const [wedCloseH, setWedCloseH] = useState("");
  const [thuOpenH, setThuOpenH] = useState("");
  const [thuCloseH, setThuCloseH] = useState("");
  const [friOpenH, setFriOpenH] = useState("");
  const [friCloseH, setFriCloseH] = useState("");
  const [satOpenH, setSatOpenH] = useState("");
  const [satCloseH, setSatCloseH] = useState("");
  const [sunOpenH, setSunOpenH] = useState("");
  const [sunCloseH, setSunCloseH] = useState("");

  const days = [
    {
      key: 0,
      label: "Monday",
      active: monSelected,
      openH: monOpenH,
      closeH: monCloseH
    },
    {
      key: 1,
      label: "Tuesday",
      active: tueSelected,
      openH: tueOpenH,
      closeH: tueCloseH
    },
    {
      key: 2,
      label: "Wednesday",
      active: wedSelected,
      openH: wedOpenH,
      closeH: wedCloseH
    },
    {
      key: 3,
      label: "Thursday",
      active: thuSelected,
      openH: thuOpenH,
      closeH: thuCloseH
    },
    {
      key: 4,
      label: "Friday",
      active: friSelected,
      openH: friOpenH,
      closeH: friCloseH
    },
    {
      key: 5,
      label: "Saturday",
      active: satSelected,
      openH: satOpenH,
      closeH: satCloseH
    },
    {
      key: 6,
      label: "Sunday",
      active: sunSelected,
      openH: sunOpenH,
      closeH: sunCloseH
    }
  ];

  const setOpenH = (v, k) => {
    switch (k) {
      case 0:
        setMonOpenH(v.value);
        break;
      case 1:
        setTueOpenH(v.value);
        break;
      case 2:
        setWedOpenH(v.value);
        break;
      case 3:
        setThuOpenH(v.value);
        break;
      case 4:
        setFriOpenH(v.value);
        break;
      case 5:
        setSatOpenH(v.value);
        break;
      case 6:
        setSunOpenH(v.value);
        break;
      default:
        break;
    }
    console.log(days[k]);
  };
  const setCloseH = (v, k) => {
    switch (k) {
      case 0:
        setMonCloseH(v.value);
        break;
      case 1:
        setTueCloseH(v.value);
        break;
      case 2:
        setWedCloseH(v.value);
        break;
      case 3:
        setThuCloseH(v.value);
        break;
      case 4:
        setFriCloseH(v.value);
        break;
      case 5:
        setSatCloseH(v.value);
        break;
      case 6:
        setSunCloseH(v.value);
        break;
      default:
        break;
    }
    console.log(days[k]);
  };
  const customStyles = {
    option: (provided, state) => ({
      ...provided
      //color: state.isSelected ? 'red' : '',
    }),
    control: (provided, state) => ({
      // none of react-select's styles are passed to <Control />
      display: "flex"
    })
  };
  const [isLoading, setLoading] = useState(false);
  const handleClick = () => {
    setLoading(true);
    setDays(days);
    setOnb(true);
  };
  useEffect(() => {
    if (isLoading) {
      setTimeout(() => {
        setLoading(false);
        window.location = "/onboarding/calendar";
      }, 2000);
    }
  }, [isLoading]);
  const [isNameValid, setNameValidity] = useState(false);
  const [isNameInalid, setNameInalidity] = useState(false);
  const verifName = e => {
    if (e.target.value.match("^[a-zA-Z][a-zA-Z0-9]*$") != null) {
      setNameValidity(true);
      setNameInalidity(false);
      saveName(e);
    } else {
      setNameValidity(false);
      setNameInalidity(true);
    }
  };
  return (
    <div className={"Schedule"} data-testid="Schedule">
      <Row>
        <Col md={6}>
          <h1 style={{ fontSize: 36, fontWeight: "bold" }}>Great!</h1>
          <p style={{ fontSize: 16, color: "#818181" }}>
            Let’s create the first schedule for{" "}
            <strong>
              {storeData.userIsConnected ? (
                storeData.userInfo.company
              ) : (
                <span>A Company</span>
              )}
            </strong>
          </p>
          <div>
            <p></p>
            <div>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Give the schedule a name</Form.Label>
                <Form.Control
                  onChange={verifName}
                  placeholder="What would you like to call this?"
                  defaultValue={sname}
                  isInvalid={isNameInalid}
                  isValid={isNameValid}
                />
              </Form.Group>
            </div>
          </div>
          <div>
            <p>
              <strong>
                Let RightTime know when you are open for appointments
              </strong>
            </p>
            <p>Select working days </p>
            <p className={"days"}>
              <Button
                variant={monSelected ? "primary" : "outline-secondary"}
                onClick={() => {
                  selectMon(!monSelected);
                }}
                size={"lg"}
              >
                MON
              </Button>
              {"  "}
              <Button
                variant={tueSelected ? "primary" : "outline-secondary"}
                onClick={() => {
                  selectTue(!tueSelected);
                }}
                size={"lg"}
              >
                TUE
              </Button>
              {"  "}
              <Button
                variant={wedSelected ? "primary" : "outline-secondary"}
                onClick={() => {
                  selectWed(!wedSelected);
                }}
                size={"lg"}
              >
                WED
              </Button>
              {"  "}
              <Button
                variant={thuSelected ? "primary" : "outline-secondary"}
                onClick={() => {
                  selectThu(!thuSelected);
                }}
                size={"lg"}
              >
                THU
              </Button>
              {"  "}
              <Button
                variant={friSelected ? "primary" : "outline-secondary"}
                onClick={() => {
                  selectFri(!friSelected);
                }}
                size={"lg"}
              >
                FRI
              </Button>
              {"  "}
              <Button
                variant={satSelected ? "primary" : "outline-secondary"}
                onClick={() => {
                  selectSat(!satSelected);
                }}
                size={"lg"}
              >
                SAT
              </Button>
              {"  "}
              <Button
                variant={sunSelected ? "primary" : "outline-secondary"}
                onClick={() => {
                  selectSun(!sunSelected);
                }}
                size={"lg"}
              >
                SUN
              </Button>
              {"  "}
            </p>
          </div>
          <div style={{ marginBottom: 26 }}>
            <p>Select working hours</p>
            <Table hover size="sm" style={{ marginBottom: 5 }}>
              <thead>
                <tr>
                  <th>Working days</th>
                  <th>Opening</th>
                  <th></th>
                  <th>Closing</th>
                </tr>
              </thead>
              <tbody>
                {days.map(d =>
                  d.active ? (
                    <tr key={d.key}>
                      <td>{d.label}</td>
                      <td>
                        <CreatableSelect
                          defaultValue={{
                            value: d.openH ? d.openH : "custom",
                            label: d.openH ? d.openH : "Select"
                          }}
                          options={hours}
                          size={"sm"}
                          onChange={e => {
                            setOpenH(e, d.key);
                          }}
                          styles={customStyles}
                        />
                      </td>
                      <td>to</td>
                      <td>
                        <CreatableSelect
                          defaultValue={{
                            value: d.closeH ? d.closeH : "custom",
                            label: d.closeH ? d.closeH : "Select"
                          }}
                          options={hours}
                          size={"sm"}
                          onChange={e => {
                            setCloseH(e, d.key);
                          }}
                          styles={customStyles}
                        />
                      </td>
                    </tr>
                  ) : null
                )}
              </tbody>
            </Table>
            <small style={{ color: "#A8A8A8" }}>
              Don’t worry you’ll be able to further change this later
            </small>
          </div>
          <p>
            <Button
              size={"lg"}
              variant="primary active"
              onClick={!isLoading ? handleClick : null}
            >
              {isLoading ? "Loading…" : "CONTINUE"}
            </Button>
            <Button
              size={"lg"}
              href="/onboarding/calendar"
              variant="link active"
            >
              I'll do this later
            </Button>
          </p>
        </Col>
        <Col
          md={6}
          className={"d-none d-sm-none d-md-block d-lg-block d-xl-block"}
        >
          <img
            src="/assets/onboarding/Desktop/Group 839.svg"
            alt=""
            style={{ width: "100%", height: 322 }}
          />
        </Col>
      </Row>
    </div>
  );
}

Schedule.propTypes = {};

Schedule.defaultProps = {};

export default Schedule;
