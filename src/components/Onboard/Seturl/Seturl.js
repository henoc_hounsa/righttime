import React, { useEffect, useState } from "react";

import { Button, Col, FormControl, InputGroup, Row } from "react-bootstrap";
import Select from "react-select";

function simulateNetworkRequest() {
  return new Promise(resolve => setTimeout(resolve, 2000));
}
const useStateWithLocalStorageUrl = localStorageKey => {
  const [url, setUrl] = React.useState(
    localStorage.getItem(localStorageKey) || ""
  );
  React.useEffect(() => {
    localStorage.setItem(localStorageKey, url);
  }, [url, localStorageKey]);
  return [url, setUrl];
};
const useStateWithLocalStorageTz = localStorageKey => {
  const [tz, setTz] = React.useState(
    JSON.parse(localStorage.getItem(localStorageKey)) || null
  );
  React.useEffect(() => {
    localStorage.setItem(localStorageKey, JSON.stringify(tz));
  }, [tz, localStorageKey]);
  return [tz, setTz];
};
//////////////////////////////////////////////////////////////////
function Seturl(props) {
  const [url, setUrl] = useStateWithLocalStorageUrl("rightTime_url");
  const [tz, setTz] = useStateWithLocalStorageTz("rightTime_tz");
  const xtimeszones = require("../../../timezone.json");
  let timeszones = [];
  xtimeszones.map(function(o) {
    o.utc.map(function(i) {
      timeszones.push({ label: o.text.substring(0, 12) + " " + i, value: i });
    });
  });
  const [isLoading, setLoading] = useState(false);
  const [isValid, setValidity] = useState(false);
  const [tzIsValid, setTvValidity] = useState(null);
  const [isInvalid, setInvalidity] = useState(false);

  useEffect(() => {
    if (isLoading) {
      simulateNetworkRequest().then(() => {
        setLoading(false);
      });
    }
  }, [isLoading]);

  const handleClick = () => {
    if (url) {
      setValidity(true);
      if (tz?.value) {
        setLoading(true);
        window.location = "/onboarding/schedule";
      } else {
      }
    }
  };
  const verifUrl = e => {
    if (e.target.value.match("^[a-zA-Z][a-zA-Z0-9]*$") != null) {
      setValidity(true);
      setInvalidity(false);
      setUrl(e.target.value.toLowerCase());
    } else {
      setValidity(false);
      setInvalidity(true);
    }
  };
  const getTimez = e => {
    setTz(e);
    setTvValidity(true);
  };

  return (
    <div className={"Seturl"} data-testid="Seturl">
      <Row>
        <Col md={5}>
          <h1 style={{ fontSize: 36, fontWeight: "bold" }}>Let’s begin!</h1>
          <p style={{ fontSize: 16, color: "#818181" }}>
            Customize Your Scheduling Page URL
          </p>
          <div>
            <p>
              This is where your clients schedule a time with you. It is
              advisable to choose a URL that is succint and easy to remember.
            </p>
            <div>
              <InputGroup size="lg">
                <InputGroup.Prepend>
                  <InputGroup.Text id="inputGroup-sizing-lg">
                    https://
                  </InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                  aria-label="Large"
                  aria-describedby="inputGroup-sizing-sm"
                  isValid={isValid}
                  isInvalid={isInvalid}
                  onChange={verifUrl}
                  defaultValue={url}
                />
                <InputGroup.Append>
                  <InputGroup.Text>.righttime.com</InputGroup.Text>
                </InputGroup.Append>
              </InputGroup>
            </div>
            {url && isValid ? (
              <div>
                <small className={"text-success"}>
                  Here is what yout url looks like{" "}
                  <strong>https://{url}.righttime.com</strong>
                </small>
              </div>
            ) : (
              ""
            )}
            {isInvalid == true ? (
              <div>
                <small className={"text-danger"}>
                  This URL is not available, please try again using another
                  unique name.
                </small>
              </div>
            ) : (
              ""
            )}
            <small>
              Note: Your custom URL must contain 3-100 letters or numbers.
              Please do not use spaces, symbols or special characters.
            </small>
          </div>
          <div style={{ marginBottom: 26, marginTop: 26 }}>
            <h5>Time Zone</h5>
            <Select
              defaultValue={tz}
              options={timeszones}
              onChange={getTimez}
            />
            {tzIsValid == false ? (
              <small className={"text-danger"}>
                Please select your timezone
              </small>
            ) : (
              ""
            )}
          </div>
          <Button
            disabled={isInvalid}
            size={"lg"}
            variant="primary active"
            onClick={!isLoading ? handleClick : null}
          >
            {isLoading ? "Loading…" : "CONTINUE"}
          </Button>
          <Button size={"lg"} href="/onboarding/schedule" variant="link active">
            I'll do this later
          </Button>
        </Col>
        <Col md={1}></Col>
        <Col
          md={6}
          className={"d-none d-sm-none d-md-block d-lg-block d-xl-block"}
        >
          <img
            src="/assets/onboarding/Desktop/Group 839.svg"
            alt=""
            style={{ width: "100%", height: 322 }}
          />
        </Col>
      </Row>
    </div>
  );
}

Seturl.propTypes = {};

Seturl.defaultProps = {};

export default Seturl;
