import React from 'react';
import { cleanup, render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Seturl from './Seturl';

describe('<Seturl />', () => {
  afterEach(cleanup);

  test('it should mount', () => {
    const { getByTestId } = render(<Seturl />);
    const seturl = getByTestId('Seturl');

    expect(seturl).toBeInTheDocument();
  });
});