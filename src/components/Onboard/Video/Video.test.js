import React from 'react';
import { cleanup, render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Video from './Video';

describe('<Video />', () => {
  afterEach(cleanup);

  test('it should mount', () => {
    const { getByTestId } = render(<Video />);
    const video = getByTestId('Video');

    expect(video).toBeInTheDocument();
  });
});