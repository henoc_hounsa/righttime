import React from "react";
import {Button, Col, Row} from "react-bootstrap";
import GetStore from "../../../getStore";

const useStateWithLocalStorageOnb = localStorageKey => {
    const [onb, setOnb] = React.useState(
        localStorage.getItem(localStorageKey) || "false"
    );
    React.useEffect(() => {
        localStorage.setItem(localStorageKey, onb);
    }, [onb]);
    return [onb, setOnb];
};

function Video() {
    const [videoShow, setVideoShow] = React.useState(true);
    const [onb, setOnb] = useStateWithLocalStorageOnb("rightTime_onb");
    const storeData = GetStore();
    return (
        <div className={"Video"} data-testid="Video">
            <Row>
                <Col md={12}>
                    <h1 style={{fontSize: 36, fontWeight: "bold"}}>
                        Hello{" "}
                        {storeData.userIsConnected ? (
                            storeData.userInfo.firstname
                        ) : (
                            <span>Salewa</span>
                        )}
                        !
                    </h1>
                    <p style={{fontSize: 16, color: "#818181"}}>
                        I’ll be showing you how easy and convenient using RightTime can be
                    </p>
                    <div
                        style={{
                            margin: "1% auto",
                            maxWidth: 540,
                            height: "auto",
                            textAlign: "center"
                        }}
                    >
                        <iframe
                            className={"embed-responsive-item"}
                            title="vimeo-player"
                            src="https://player.vimeo.com/video/181002384"
                            width="100%"
                            height="500"
                            frameBorder="0"
                            allow="autoplay ; fullscreen"
                            allowFullScreen
                        ></iframe>
                        <Button
                            onClick={() => {
                                window.location = '/dashboard/settigs/account';
                                setOnb(true)
                            }}
                            size={"lg"}
                            variant="primary active"
                        >
                            Proceed to MY Account
                        </Button>
                    </div>
                </Col>
            </Row>
        </div>
    );
}

Video.propTypes = {};

Video.defaultProps = {};

export default Video;
