import React from "react";
import styles from "./Calendar.module.css";
import { Button, Col, Row } from "react-bootstrap";

function Calendar(props) {
  return (
    <div className={"Calendar"} data-testid="Calendar">
      <Row>
        <Col md={5}>
          <h1 style={{ fontSize: 36, fontWeight: "bold" }}>
            We’re almost done!
          </h1>
          <p style={{ fontSize: 16, color: "#818181" }}>
            To ensure that you are never double-booked, RightTime needs to
            connect to your active calendars
          </p>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              borderBottom: "solid 1px rgba(112, 112, 112,0.16)",
              padding: "24px 0"
            }}
          >
            <div>
              <img
                src="/assets/onboarding/General/Image 3.png"
                alt=""
                style={{ width: 65, height: 65 }}
              />
            </div>
            <div style={{ flexGrow: 1, paddingLeft: 15 }}>
              <span style={{ fontSize: 16 }}>Google Calendar</span>
              <br />
              <small style={{ fontSize: 12, color: "#818181" }}>
                Gmail, G Suite
              </small>
            </div>
            <div>
              <Button variant="outline-primary">Connect</Button>
            </div>
          </div>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              borderBottom: "solid 1px rgba(112, 112, 112,0.16)",
              padding: "24px 0"
            }}
          >
            <div>
              <img
                src="/assets/onboarding/General/Image 2.png"
                alt=""
                style={{ width: 65, height: 65 }}
              />
            </div>
            <div style={{ flexGrow: 1, paddingLeft: 15 }}>
              <span style={{ fontSize: 16 }}>Office 365 Calendar</span>
              <br />
              <small style={{ fontSize: 12, color: "#818181" }}>
                Office 365, Outlook.com, live.com, or hotmail
              </small>
            </div>
            <div>
              <Button variant="outline-primary">Connect</Button>
            </div>
          </div>
          <div
            style={{ display: "flex", alignItems: "center", padding: "24px 0" }}
          >
            <div>
              <img
                src="/assets/onboarding/General/Image 4.png"
                alt=""
                style={{ width: 65, height: 65 }}
              />
            </div>
            <div style={{ flexGrow: 1, paddingLeft: 15 }}>
              <span style={{ fontSize: 16 }}>iCloud calendar</span>
              <br />
              <small style={{ fontSize: 12, color: "#818181" }}>
                Default calendar for all Apple products.
              </small>
            </div>
            <div>
              <Button variant="outline-primary">Connect</Button>
            </div>
          </div>
          <Button
            href={"/onboarding/video"}
            size={"lg"}
            variant="primary active"
          >
            Continue without calendar
          </Button>
        </Col>
        <Col md={1}></Col>
        <Col
          md={6}
          className={"d-none d-sm-none d-md-block d-lg-block d-xl-block"}
        >
          <img
            src="/assets/onboarding/Desktop/Group 839.svg"
            alt=""
            style={{ width: "100%", height: 322 }}
          />
        </Col>
      </Row>
    </div>
  );
}

Calendar.propTypes = {};

Calendar.defaultProps = {};

export default Calendar;
