import React from 'react';
import { cleanup, render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Calendar from './Calendar';

describe('<Calendar />', () => {
  afterEach(cleanup);

  test('it should mount', () => {
    const { getByTestId } = render(<Calendar />);
    const calendar = getByTestId('Calendar');

    expect(calendar).toBeInTheDocument();
  });
});