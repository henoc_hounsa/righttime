import React, { useCallback, useEffect, useRef, useState } from "react";
import { useDispatch } from "redux-react-hook";
import "./Onboard.module.css";
import Topnavbar from "../Topnavbar/Topnavbar";
import Footer from "../Footer/Footer";
import Hello from "../onboard/Hello/Hello";
import Seturl from "../onboard/Seturl/Seturl";
import useHttpService from "../../services/HttpService";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams
} from "react-router-dom";
import Schedule from "../onboard/Schedule/Schedule";
import Calendar from "../onboard/Calendar/Calendar";
import Video from "../onboard/Video/Video";

function Onboard(props) {
  //localStorage.removeItem();
  let match = useRouteMatch();
  const hasMount = useRef(false);
  const unmounted = useRef(false);
  const [stateCheckSession, setStateCheckSession] = useState("loading");
  const {
    send: sendRequestCheckSession,
    data: sessionResponse,
    loading: onLoadingSession,
    error: onErrorSession
  } = useHttpService("CHECK_SESSION");
  const dispatch = useDispatch();
  const updateUserInfo = useCallback(
    user => dispatch({ type: "UPDATE_USER_INFO", value: user }),
    [dispatch]
  );
  const updateUserIsConnected = useCallback(
    status => dispatch({ type: "UPDATE_USER_IS_CONNECTED", value: status }),
    [dispatch]
  );
  const updateUserFullName = useCallback(
    fullName => dispatch({ type: "UPDATE_USER_FULLNAME", value: fullName }),
    [dispatch]
  );

  useEffect(() => {
    if (!hasMount.current) {
      // componentDidMount
      checkSession();
      hasMount.current = true;
    } else {
      // componentDidUpdate
    }
    return () => {
      unmounted.current = true;
      // componentWillUnmount
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // CheckSession submit result hook
  useEffect(() => {
    if (!unmounted.current) {
      if (!onLoadingSession && onErrorSession) {
        setStateCheckSession("error");
      }
      if (!onLoadingSession && sessionResponse) {
        if (sessionResponse.status === 200) {
          // user session is ok
          // set user session
          updateUserInfo(sessionResponse.user);
          updateUserFullName(
            sessionResponse.user.firstname + " " + sessionResponse.user.lastname
          );
          updateUserIsConnected(true);
          setStateCheckSession("success");
          //props.onLoaded(true);
        } else if (sessionResponse.status === 201) {
          // user session is ok
          // set user session
          updateUserInfo(sessionResponse.user);
          updateUserFullName(
            sessionResponse.user.firstname + " " + sessionResponse.user.lastname
          );
          updateUserIsConnected(true);
          setStateCheckSession("success");
          //props.onLoaded(true);
        } else {
          setStateCheckSession("error");
        }
      }
    }
  }, [
    onLoadingSession,
    onErrorSession,
    sessionResponse,
    props,
    updateUserInfo,
    updateUserFullName,
    updateUserIsConnected
  ]);

  function checkSession() {
    sendRequestCheckSession();
  }

  function OnClickedRetry() {
    window.location.href = "/";
  }
  return (
    <div className={"dashboard"} data-testid="Onboard">
      <Topnavbar fixed={"top"} brandHref="/onboarding" />
      <div
        style={{ xminHeight: "calc( 100vh - 84px )", marginTop: 64 }}
        className={"container"}
      >
        <Switch>
          <Route path={`${match.path}/video`}>
            <Video />
          </Route>
          <Route path={`${match.path}/calendar`}>
            <Calendar />
          </Route>
          <Route path={`${match.path}/schedule`}>
            <Schedule />
          </Route>
          <Route path={`${match.path}/seturl`}>
            <Seturl />
          </Route>
          <Route path={match.path}>
            <Hello />
          </Route>
        </Switch>
      </div>
      <Footer static={"bottom"} />
    </div>
  );
}

Onboard.propTypes = {};

Onboard.defaultProps = {};

export default Onboard;
