import React from 'react';
import { cleanup, render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Onboard from './Onboard';

describe('<Onboard />', () => {
  afterEach(cleanup);

  test('it should mount', () => {
    const { getByTestId } = render(<Onboard />);
    const dashboard = getByTestId('Onboard');

    expect(dashboard).toBeInTheDocument();
  });
});
