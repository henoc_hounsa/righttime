import React from 'react';
import { cleanup, render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Blacknav from './Blacknav';

describe('<Blacknav />', () => {
  afterEach(cleanup);

  test('it should mount', () => {
    const { getByTestId } = render(<Blacknav />);
    const blacknav = getByTestId('Blacknav');

    expect(blacknav).toBeInTheDocument();
  });
});