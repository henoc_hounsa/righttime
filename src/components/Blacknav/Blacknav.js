import React from "react";
import "./Blacknav.module.css";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Button from "react-bootstrap/Button";
import { withNamespaces } from "react-i18next";
import GetStore from "../../getStore.js";

function Blacknav({ t }) {
  const storeData = GetStore();
  return (
    <Navbar
      bg="dark"
      expand="lg"
      variant="dark"
      style={{ minHeight: 64 }}
      sticky={"top"}
    >
      <Navbar.Brand href="/" className={"d-none d-sm-block"}>
        <img
          alt=""
          src="/assets/landing/General/SVG/RT-logo-for-dark-background.svg"
          width="124"
          className="d-inline-block align-top"
        />
      </Navbar.Brand>
      <Nav
        className="ml-auto"
        justify
        style={{ display: "flex", flexDirection: "row", alignItems: "center" }}
      >
        <Nav className="Item">
          <Nav.Link href="#">{t("features")}</Nav.Link>
        </Nav>
        <Nav className="Item">
          {!storeData.userIsConnected ? (
            <Nav.Link href="/onboarding">{t("sign_in")}</Nav.Link>
          ) : (
            " "
          )}
        </Nav>
        <Nav className="Item" style={{ paddingLeft: 15 }}>
          <Button size={"sm"} href="/onboarding" variant="outline-light">
            {t("get_started")}
          </Button>
        </Nav>
      </Nav>
    </Navbar>
  );
}

Blacknav.propTypes = {};

Blacknav.defaultProps = {};

export default withNamespaces()(Blacknav);
