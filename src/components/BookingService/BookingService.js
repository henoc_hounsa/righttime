import React, { Component } from "react";
import { Col, Row, Container } from "react-bootstrap";
import Card from "react-bootstrap/Card";
import "./BookingService.css";
import "../BookingCommon/Main.css";
import CompanyHeader from "../BookingCommon/CompanyHeader";
import CompanyFooter from "../BookingCommon/CompanyFooter";
import Brand from "../BookingCommon/Brand";
import BookingAgent from "../BookingAgent/BookingAgent";
import BookingDate from "../BookingDate/BookingDate";
import BookingServiceSelector from "../BookingService/BookingServiceSelector";
import NavigateNextButton from "../BookingWidget/NavigateNextButton";
import bookingConstants from "../../constants/bookingConstants";
import { connect } from "react-redux";
import { compose } from "redux";
import { withTranslation } from "react-i18next";

class BookingService extends Component {
  constructor(props) {
    super(props);
    this.navigation = React.createRef();
    this.state = {
      navigate: "",
      service: this.props.appointmentDetails.service,
    };

    this.handleServiceUpdate = this.handleServiceUpdate.bind(this);
  }

  componentDidMount() {
    // Re-Initialisation du service actif.
    this.props.appointmentDetails.service = "";
    //
    this.props.enableLanguageButton();
    // Deactive language change.
    this.props.handleLanguage(false);
  }

  navigate(val) {
    this.props.disableLanguageButton();
    this.setState({ navigate: val });
  }

  handleServiceUpdate(svc) {
    this.props.appointmentDetails.service = svc;
    this.setState({
      service: this.props.appointmentDetails.service,
    });
  }

  render() {
    const { t } = this.props;
    const step = "1";
    var appointmentDetails = this.props.appointmentDetails;
    var serviceHeadingText = t("service.service_heading");

    if (
      appointmentDetails.schedule &&
      appointmentDetails.schedule.customServiceOffering &&
      appointmentDetails.schedule.customServiceOffering.serviceHeadingText
    ) {
      serviceHeadingText =
        localStorage.getItem("rightTime_lang") === "fr"
          ? appointmentDetails.schedule.customServiceOffering.serviceHeadingText
              .fr
          : appointmentDetails.schedule.customServiceOffering.serviceHeadingText
              .en;
    }

    var schedule = appointmentDetails.schedule;
    var customFlow =
      schedule.customDisplaySettings &&
      schedule.customDisplaySettings.customFlow;
    var calendarPageIsFirst =
      customFlow &&
      schedule.customDisplaySettings.customizeFlow[1] ===
        bookingConstants.CALENDAR;

    if (this.state.navigate === "NEXT") {
      if (customFlow && calendarPageIsFirst) {
        return (
          <BookingDate
            appointmentDetails={appointmentDetails}
            handleLanguage={this.props.handleLanguage}
          />
        );
      }
      return (
        <BookingAgent
          appointmentDetails={appointmentDetails}
          handleLanguage={this.props.handleLanguage}
        />
      );
    }

    return (
      <div>
        <Container>
          <Row className="main-content mb-5">
            <Col md={4} lg={6}>
              <Row className="left-container">
                <div className="company-header-container">
                  <CompanyHeader
                    alignment="left"
                    schedule={appointmentDetails.schedule}
                  />
                </div>
                <div className="brand-step-container">
                  <Brand step={step} schedule={appointmentDetails.schedule} />
                </div>
              </Row>
            </Col>
            <Col md={8} lg={6}>
              <Row className="right-container">
                <Card className="card-main">
                  {serviceHeadingText === "" ? (
                    ""
                  ) : (
                    <Card.Header>
                      <h6 className="header-title">
                        <strong>{serviceHeadingText}</strong>
                      </h6>
                    </Card.Header>
                  )}
                  <Card.Body className="card-body-booking">
                    <BookingServiceSelector
                      handleServiceUpdate={this.handleServiceUpdate}
                      appointmentDetails={appointmentDetails}
                    />
                  </Card.Body>
                  <Card.Footer>
                    <NavigateNextButton
                      schedule={appointmentDetails.schedule}
                      ref={this.navigation}
                      title={t("service.step_one_button_next")}
                      disable={!this.state.service}
                      navigate={this.navigate.bind(this, "NEXT")}
                    />
                  </Card.Footer>
                </Card>
              </Row>
            </Col>
          </Row>
          <Row className="footer-holder">
            <CompanyFooter schedule={appointmentDetails.schedule} />
          </Row>
        </Container>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    disableLanguageButton: () => {
      dispatch({ type: "UPDATE_LANGUAGE_BUTTON", value: true });
    },

    enableLanguageButton: () => {
      dispatch({ type: "UPDATE_LANGUAGE_BUTTON", value: false });
    },
  };
};

export default compose(
  withTranslation(),
  connect(null, mapDispatchToProps)
)(BookingService);
