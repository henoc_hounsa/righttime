import React, { Component } from "react";
import axios from "axios";
import apiUrl from "../../datas/apiUrl";
import {Form} from "react-bootstrap";
import {toast, ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import Loader from 'react-loader-spinner'
import ToastMessage from "../BookingCommon/ToastMessage";
import RTToastContainer from "../BookingCommon/RTToastContainer";
import { withTranslation } from 'react-i18next';

class BookingServiceSelector extends Component {

    constructor(props) {
        super(props);
        this.state = {
            services: [],
            service: this.props.appointmentDetails.service,
            loading: true,
            hasError: false,
        }

        this.serviceSelection = this.serviceSelection.bind(this);
        this.populateAppointmentProps = this.populateAppointmentProps.bind(this);
    }

    componentDidMount() {
        console.log("appointment details", this.props.appointmentDetails);
        this.retrieveServices();
    }

    retrieveServices() {
        axios.get(apiUrl.serverUrl + apiUrl.serviceGetAll, apiUrl.config).then(services => {
             this.setState({
                loading: false,
                services: services.data
             });
        })
        .catch(error => {
            console.log(error);
            toast.error(<ToastMessage 
                msg="Error occured while getting service. Please try again or refresh the page." 
                icon="fa fa-times-circle" 
            />);
            this.setState({loading: false, hasError: true});
        });
    }

    serviceSelection = svc => event  => {
        this.props.handleServiceUpdate(svc);
        this.setState({
          service: svc
        });
    }

    populateAppointmentProps() {
        this.props.appointmentDetails.service = this.state.service;
    }

    render() {
        const { t } = this.props;
        this.populateAppointmentProps();
        let {services, service, loading, hasError} = this.state;
        let schedule = this.props.appointmentDetails.schedule;
        let displayImage = schedule && schedule.customServiceOffering && schedule.customServiceOffering.attributes.IMAGE;
        let displayPrice = schedule && schedule.customServiceOffering && schedule.customServiceOffering.attributes.PRICE;
        let displayCategory = schedule && schedule.customServiceOffering && schedule.customServiceOffering.attributes.CATEGORY;
        let marginTopValForName = displayCategory ? "-8px" : "-2px";
        let xpCdn = apiUrl.xpCdn;
        let locale = localStorage.getItem("rightTime_lang");

        console.log(schedule);

        return (
            <div className="scrollable-main" id="scrollId">
                <div className="scrollable-content">
                    <label className="card-body-title">{t('service.infos_message')}</label>
                <div className="booking-card-content">
                    {loading ?
                        <div className="loading-container">
                            <Loader
                                     type="ThreeDots"
                                     color="#00BFFF"
                                     height={100}
                                     width={100}
                                     />
                        </div>
                        :
                        hasError ?
                           <RTToastContainer
                               />
                            :
                            <div className="service-radio-holder">
                            {services.length === 0 ? <div className="no-service-container">{t('service.no_service_msg')}</div> : ""}
                            {services.map(ser => (
                                   <div key={ser.id+service.id} className={ser.id == service.id ? "service-radio service-radio-selected" : "service-radio"}
                                        onClick={this.serviceSelection(ser)} value={ser} name="service" >
                                       <div className="service-info form-group">
                                           <div className="service-details">
                                               {displayImage && ser.imagePath !== "" ?
                                                <div>
                                                    <img className="service-image" src={xpCdn.replace('{{imagepath}}', ser.imagePath)} />
                                                </div>
                                                :
                                                ""
                                               }
                                               <div className="service-details-content" style={{marginTop: marginTopValForName}}>
                                                    <label className="service-name" htmlFor={ser.id}>
                                                        {
                                                            locale === "en" ? ser.name.en ? ser.name.en : ser.name.fr : ser.name.fr ? ser.name.fr : ser.name.en
                                                        }
                                                    </label><br />
                                                    {displayCategory ? <label className="service-category" htmlFor={ser.id}>{ser.category}</label> : "" }
                                               </div>
                                           </div>
                                       </div>
                                       <div className="service-cost-container">
                                            <div className="service-cost-sub-container">
                                                { displayPrice && (ser.serviceCost != null && ser.serviceCost != "" && ser.serviceCost != 0) ? <div className="service-cost-separator"></div> : "" }
                                                { displayPrice ? <span className="service-cost"> {(ser.serviceCost == null || ser.serviceCost == "" || ser.serviceCost == 0) ? "" : ser.serviceCostCurrency + ser.serviceCost }</span> : ""}
                                            </div>
                                       </div>
                                   </div>
                               ))}
                             </div>
                     }
                 </div>
                </div>
            </div>
        )
    }
}

export default withTranslation()(BookingServiceSelector);