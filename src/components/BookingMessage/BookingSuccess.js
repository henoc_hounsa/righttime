import React, { Component } from "react";
import { Col, Row, Container } from "react-bootstrap";
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import "./BookingMessage.css";
import CompanyHeader from "../BookingCommon/CompanyHeader";
import BookingEntryPoint from "../BookingCommon/BookingEntryPoint";
import { withTranslation } from 'react-i18next';

class BookingSuccess extends Component {

    constructor(props) {
        super(props);
        this.bookNew = this.bookNew.bind(this);
        this.state = {
            navigate: ""
        }
    }

    bookNew() {
        this.setState({navigate: "book"})
    }

    render() {
        const { t } = this.props;
        var {navigate} = this.state;
        var appointmentDetails = this.props.appointmentDetails;
        if(navigate === "book") {
            return <BookingEntryPoint />
        }
        var schedule = appointmentDetails.schedule;
        var color = schedule.customDisplaySettings && schedule.customDisplaySettings.color ? schedule.customDisplaySettings.color : "0089e1";

        return(
            <div>
            <Row className="company-header-row-success">
                <CompanyHeader schedule={appointmentDetails.schedule} id="companyHeaderDateId" />
            </Row>
            <Row className="row-center message-container">
                <div className="message-container-col">
                    <div className="message-col">
                        <Card className="card-message">
                             <Card.Body className="card-message-body">
                               <img className="success-img" src="/assets/booking/icon-success.svg" /><br />
                                <label className="success-great-label">{t('message.text_one')}</label><br />
                               <label className="success-msg-label-1">{t('message.text_two')}</label><br />
                               <label className="success-msg-label-2">{t('message.text_three')}</label><br />
                             </Card.Body>
                             <Card.Footer className="card-message-footer">
                                   <Button className="book-button" style={{backgroundColor: color, borderColor: color}} onClick={this.bookNew}>{t('message.button_new')}</Button>
                             </Card.Footer>
                        </Card>
                    </div>
                </div>
            </Row>
            </div>
        );
    }

}

export default withTranslation()(BookingSuccess)
