import React from 'react';
import {render} from '@testing-library/react';

import Loading from './Loading';

test('render component', () => {

    const {getByText} = render(<Loading/>);

    const linkElement = getByText(/Loading/i);

    expect(linkElement).toBeInTheDocument();

});
