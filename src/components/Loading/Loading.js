import React from 'react';
import './Loading.css';

function Loading() {
    return (
        <div className="Loading" id="Loading">
            <div style={{position: "relative", display: "flex", height: "100%", alignItems: "center"}}>
                <div style={{width: "10vw", minWidth: 128, margin: "auto"}}>
					<img src="/favicon.png" alt="" style={{width: "100%"}}/></div>
            </div>
        </div>
    );
};

export default Loading;

        