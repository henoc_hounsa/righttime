import React, { Component } from "react";
import Button from 'react-bootstrap/Button'
import "../BookingCommon/Main.css";

export default class NavigateNextButton extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        var color = this.props.schedule.customDisplaySettings && this.props.schedule.customDisplaySettings.color ? this.props.schedule.customDisplaySettings.color : "0089e1";
        return (
            <div>
                <Button onClick={this.props.navigate} style={{backgroundColor: color, borderColor: color}} disabled={this.props.disable} className={this.props.disable ? "next-navigate-button button-disable" : "next-navigate-button"} >
                    <span style={{marginRight: "10px"}}>{this.props.title}</span>  <img className="next-arrow" src="/assets/booking/next-icon.svg" />
                </Button>
            </div>

        )
    }
}