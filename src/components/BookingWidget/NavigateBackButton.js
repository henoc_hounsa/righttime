import React, { Component } from "react";
import Button from 'react-bootstrap/Button'
import "../BookingCommon/Main.css";

export default class NavigateBackButton extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <Button variant="link" onClick={this.props.navigate} disabled={this.props.disable} className="back-navigate-button" >
                    <span className="back-navigate-button"><img className="arrow-back" src="/assets/booking/back-icon.svg" />
                        <span style={{marginLeft: "10px"}}><strong>{this.props.title}</strong></span></span>
                </Button>
            </div>

        )
    }
}