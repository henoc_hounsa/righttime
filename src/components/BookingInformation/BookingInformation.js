import React from "react";
import { Col, Row, Container } from "react-bootstrap";
import axios from "axios";
import Card from 'react-bootstrap/Card'
import 'font-awesome/css/font-awesome.min.css';
import { toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import Loader from 'react-loader-spinner';
import ToastMessage from "../BookingCommon/ToastMessage";
import "./BookingInformation.css";
import "../BookingCommon/Main.css";
import apiUrl from "../../datas/apiUrl";
import CompanyHeader from "../BookingCommon/CompanyHeader";
import CompanyFooter from "../BookingCommon/CompanyFooter";
import Brand from "../BookingCommon/Brand";
import BookingDate from "../BookingDate/BookingDate";
import BookingAgent from "../BookingAgent/BookingAgent";
import BookingSuccess from "../BookingMessage/BookingSuccess";
import NavigateNextButton from "../BookingWidget/NavigateNextButton";
import NavigateBackButton from "../BookingWidget/NavigateBackButton";
import bookingConstants from "../../constants/bookingConstants";
import { withTranslation } from 'react-i18next';
import 'react-phone-number-input/style.css'
import PhoneInput, { isValidPhoneNumber } from 'react-phone-number-input'


class BookingInformationFields extends React.Component {

    constructor(props) {
        super(props)
        this.handleTextChange = this.handleTextChange.bind(this)
        this.isNumberKey = this.isNumberKey.bind(this)
        this.phoneIndex = null
    }

    handleTextChange(e) {
        this.props.onDataChange(e.target.name, e.target.value)
    }

    handlePhoneTextChange(val) {
        val = val ? val : ''
        this.props.onDataChange(this.phoneIndex, val)
    }

    isNumberKey(e) {
        let charCode = (e.which) ? e.which : e.keyCode;
        if (charCode !== 43 && (charCode < 48 || charCode > 57)) {
            e.preventDefault();
        }
    }

    render() {
        const { language, data, listFields, disable, phoneFormat } = this.props;
        return listFields.map((field, index) => {
            let name = (language == 'fr') ? field.fr : field.en
            const isPhoneNumberField = phoneFormat.name.test(name.toLowerCase())
            if (isPhoneNumberField) {
                this.phoneIndex = index
            }
            return <div className="information-field">
                {isPhoneNumberField ?
                    <PhoneInput className="form-control form-control-lg information-text"
                        key={name}
                        name={index}
                        value={data[index].text}
                        onKeyPress={this.isNumberKey}
                        onChange={this.handlePhoneTextChange.bind(this)}
                        placeholder={name}
                        disabled={disable}
                    />
                    :
                    <input key={name} type="text"
                        className="form-control form-control-lg information-text"
                        name={index}
                        value={data[index].text}
                        onChange={this.handleTextChange}
                        placeholder={name}
                        disabled={disable}
                    />
                }
                <span className={"information-text " + data[index].class}>{data[index].error}</span>
            </div>
        });
    }
}


class BookingInformation extends React.Component {

    constructor(props) {
        super(props);
        this.appointmentDetails = this.props.appointmentDetails
        //
        // Data format.
        this.validator = {
            email: {
                name: /e?mail/,
                field: /^[a-z0-9\.]+@[a-z0-9-]+\.[a-z][a-z]+$/
            },
            person: {
                firstname: /(first.?name)|(pr.nom)/,
                lastname: /(last.?name)|(nom)/,
                field: /^.+$/
            },
            phone: {
                name: /^.*(phone)|(mobile).*$/,
                field: /^\+?[0-9]+$/
            }
        }
        // Init fields
        this.initAvailableFields()
        console.log('Array Of Fields', this.appointmentDetails.schedule.customDisplayFields)
        //
        this.state = {
            navigate: "",
            submitSuccessful: false,
            hasError: false,
            submitting: false,
            data: this.appointmentDetails.schedule.customDisplayFields.map((field) => {
                return this.initFormConstant(field)
            })
        };
        this.handleFormChange = this.handleFormChange.bind(this);
    }

    initAvailableFields() {
        if (!this.appointmentDetails.schedule.customDisplayFields || this.appointmentDetails.schedule.customDisplayFields == []) {
            this.appointmentDetails.schedule.customDisplayFields = [
                { 'en': 'Firstname', 'fr': 'Prénoms' },
                { 'en': 'Lastname', 'fr': 'Nom' },
                { 'en': 'Phone number', 'fr': 'Téléphone' },
                { 'en': 'Email', 'fr': 'Adresse mail' }
            ]
        }
    }

    phoneNumberHandler(val) {
        this.setState({ phoneNumber: val });
    }

    navigate(val) {
        this.setState({ navigate: val });
    }

    changeSubmitting(val) {
        this.setState({ submitting: val });
    }

    validateAllFields() {
        let hasError = false
        const data = this.state.data;
        data.forEach((elt) => {
            if (elt.error != "" || (elt.text == "" && elt.required)) {
                elt.class = "display";
                hasError = true;
                return;
            }
        });
        return hasError
    }

    getAppointmentFormValues() {
        const axiosData = [];
        const data = this.state.data;
        data.forEach((target, index) => {
            axiosData.push({
                "en": this.appointmentDetails.schedule.customDisplayFields[index].en,
                "fr": this.appointmentDetails.schedule.customDisplayFields[index].fr,
                "text": target.text
            })
        });
        return axiosData
    }

    initFormConstant(field) {
        const { t } = this.props;
        const defaultValue = {
            "text": "",
            "class": "hide",
            "required": false,
            "error": ""
        };
        //
        const name = field[t('lang.code')].toLowerCase()
        //
        if (this.validator.email.name.test(name)) {
            defaultValue.required = true
            defaultValue.error = t("formdata.email");
        }
        else if (this.validator.person.firstname.test(name)) {
            defaultValue.required = true
            defaultValue.error = t("formdata.firstname");
        }
        else if (this.validator.person.lastname.test(name)) {
            defaultValue.required = true
            defaultValue.error = t("formdata.lastname");
        }

        return defaultValue
    }

    handleFormChange(key, value) {
        const { t } = this.props;
        const data = this.state.data;
        const field = this.appointmentDetails.schedule.customDisplayFields[key][t('lang.code')].toLowerCase()

        if (this.validator.email.name.test(field)) {
            value = value.toLowerCase()
            //
            if (this.validator.email.field.test(value) && value.length >= 6) {
                data[key].class = "hide";
                data[key].error = "";                      
            }
            else {
                data[key].error = t("formdata.email");
                data[key].class = "display";
            }
            //
            this.appointmentDetails.emailAddress = value
        }
        else if (this.validator.person.firstname.test(field)) {
            if (this.validator.person.field.test(value) && value.length >= 3) {
                data[key].class = "hide";
                data[key].error = "";
            }
            else {
                data[key].error = t("formdata.firstname");
                data[key].class = "display";
            }
            this.appointmentDetails.firstName = value
        }
        else if (this.validator.person.lastname.test(field)) {
            if (this.validator.person.field.test(value) && value.length >= 3) {
                data[key].class = "hide";
                data[key].error = "";
            }
            else {
                data[key].error = t("formdata.lastname");
                data[key].class = "display";
            }
            //
            this.appointmentDetails.lastName = value
        }
        else if (this.validator.phone.name.test(field)) {
            value = value.trim()
            if (value != "" && !isValidPhoneNumber(value)) {
                data[key].error = t("formdata.phone");
                data[key].class = "display";
            }
            else {
                data[key].class = "hide";
                data[key].error = "";
            }
            //
            this.appointmentDetails.phoneNumber = value
        }
        else {
            data[key].class = "hide";
            data[key].error = "";
        }
        //
        data[key].text = value
        this.setState({ ...this.state, data: data });
    }

    render() {
        const { t } = this.props;
        const langCode = t('lang.code');
        let proceedButtonLabel = "Proceed";
        if (this.state.submitSuccessful) {
            // Will be always redirect if already apply.
            return <BookingSuccess appointmentDetails={this.appointmentDetails} />
        }

        const customFlow = this.appointmentDetails.schedule.customDisplaySettings && this.appointmentDetails.schedule.customDisplaySettings.customFlow;
        const calendarPageIsFirst = customFlow && this.appointmentDetails.schedule.customDisplaySettings.customizeFlow[1] === bookingConstants.CALENDAR;

        if (this.state.navigate == "BACK") {
            if (customFlow && calendarPageIsFirst) {
                return <BookingAgent appointmentDetails={this.appointmentDetails} handleLanguage={this.props.handleLanguage} />
            }
            return <BookingDate appointmentDetails={this.appointmentDetails} handleLanguage={this.props.handleLanguage} />
        }
        else if (this.state.navigate == "NEXT") {
            this.state.navigate = "";
            this.state.submitting = true
            let hasError = this.validateAllFields()
            console.log('hasError', hasError)
            if (hasError) {
                this.state.submitting = false
                proceedButtonLabel = "Proceed"
            }
            else {
                proceedButtonLabel = t('information.submitting');
                this.appointmentDetails.slots = [];
                this.appointmentDetails.appointmentForm = this.getAppointmentFormValues();
                this.appointmentDetails.locale = localStorage.getItem("rightTime_lang");
                
                console.log("Appointment", this.appointmentDetails)

                axios.post(apiUrl.serverUrl + apiUrl.book,
                    this.appointmentDetails, apiUrl.config
                ).then(response => {
                    if (response.status === 100) {
                        toast.error(<ToastMessage msg={response.data} icon="fa fa-times-circle" />);
                    }

                    if (response.status === 200) {
                        this.setState({ submitSuccessful: true });
                    }
                }).catch((error) => {
                    console.log(error);
                    let errorMessage = error.name + ": " + error.message;
                    if (error.response != null) {
                        errorMessage = "Error: " + error.response.data.message;
                    }
                    toast.error(<ToastMessage msg={errorMessage} icon="fa fa-times-circle" />);
                    proceedButtonLabel = "Proceed";
                    this.setState({ submitting: false });
                })
            }
        }

        return (
            <div>
                <Container>
                    <Row className="main-content">
                        <Col md={4} lg={6}>
                            <Row className="left-container">
                                <div className="company-header-container">
                                    <CompanyHeader alignment="left" schedule={this.appointmentDetails.schedule} id="companyHeaderDateId" />
                                </div>
                                <div className="brand-step-container">
                                    <Brand step="4" schedule={this.appointmentDetails.schedule} />
                                </div>
                            </Row>
                        </Col>
                        <Col md={8} lg={6}>
                            <Row className="right-container">
                                <Card className="card-main">
                                    <Card.Header>
                                        <h6 className="header-title"><strong>{t('information.infos_message_one')}</strong></h6>
                                    </Card.Header>
                                    <Card.Body className="card-body-booking">
                                        {this.state.submitting ?
                                            <div className="loading-container main-message">
                                                <Loader
                                                    type="ThreeDots"
                                                    color="#00BFFF"
                                                    height={100}
                                                    width={100}
                                                />
                                            </div>
                                            :
                                            <div className="scrollable-main" id="scrollId">
                                                <div className="scrollable-content">
                                                    <label className="card-body-title">{t('information.infos_message_two')}</label>
                                                    <div className="booking-card-content information-body">
                                                        <BookingInformationFields
                                                            language={langCode.toLowerCase()}
                                                            data={this.state.data}
                                                            disable={this.state.submitting}
                                                            onDataChange={this.handleFormChange}
                                                            listFields={this.appointmentDetails.schedule.customDisplayFields}
                                                            phoneFormat={this.validator.phone}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        }
                                    </Card.Body>

                                    <Card.Footer>
                                        <NavigateBackButton
                                            ref={this.navigation}
                                            title={t('information.step_four_button_back')}
                                            disable={this.state.submitting}
                                            navigate={this.navigate.bind(this, "BACK")}
                                        />
                                        <NavigateNextButton
                                            schedule={this.appointmentDetails.schedule}
                                            ref={this.navigation}
                                            title={proceedButtonLabel === "Proceed" ? t('information.step_four_button_proceed') : proceedButtonLabel}
                                            navigate={this.navigate.bind(this, "NEXT")}
                                            disable={this.state.submitting}
                                        />
                                    </Card.Footer>
                                </Card>
                                <div className="space-below"></div>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="footer-holder">
                        <CompanyFooter schedule={this.appointmentDetails.schedule} />
                    </Row>
                </Container>
            </div>
        )
    }
}

export default withTranslation()(BookingInformation)
