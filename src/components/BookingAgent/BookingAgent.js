import React, { Component } from "react";
import { Col, Row, Container } from "react-bootstrap";
import Card from 'react-bootstrap/Card'
import "./BookingAgent.css";
import "../BookingCommon/Main.css";
import CompanyHeader from "../BookingCommon/CompanyHeader";
import CompanyFooter from "../BookingCommon/CompanyFooter";
import Brand from "../BookingCommon/Brand";
import BookingDate from "../BookingDate/BookingDate";
import BookingService from "../BookingService/BookingService";
import BookingInformation from "../BookingInformation/BookingInformation";
import BookingAgentSelector from "../BookingAgent/BookingAgentSelector";
import NavigateNextButton from "../BookingWidget/NavigateNextButton";
import NavigateBackButton from "../BookingWidget/NavigateBackButton";
import bookingConstants from "../../constants/bookingConstants";
import { withTranslation } from 'react-i18next';

class BookingAgent extends Component {

    constructor(props) {
        super(props);
        this.state = {
          navigate: "",
          agent: this.props.appointmentDetails.agent,
        };
        this.navigate = this.navigate.bind(this);
        this.handleAgentUpdate = this.handleAgentUpdate.bind(this);
    }

    componentDidMount() {
        // Deactive language change.
        this.props.handleLanguage(true);
    }

    handleAgentUpdate(agentId) {
        this.setState({
          agent: agentId
        });
    }

    navigate(val) {
        this.setState({navigate: val});
    }

    render() {
        const { t } = this.props;
        let appointmentDetails = this.props.appointmentDetails;
        console.log("appointmentDetails", appointmentDetails);

        let schedule = appointmentDetails.schedule;
        let customFlow = schedule.customDisplaySettings && schedule.customDisplaySettings.customFlow;
        let calendarPageIsFirst = customFlow && schedule.customDisplaySettings.customizeFlow[1] === bookingConstants.CALENDAR;

        if(this.state.navigate == "NEXT") {
            if(customFlow && calendarPageIsFirst) {
                return <BookingInformation appointmentDetails={appointmentDetails} handleLanguage={this.props.handleLanguage} />
            }
            return <BookingDate appointmentDetails={appointmentDetails} handleLanguage={this.props.handleLanguage} />
        } else if (this.state.navigate == "BACK") {
            //
            if(customFlow && calendarPageIsFirst) {
                return <BookingDate appointmentDetails={appointmentDetails} handleLanguage={this.props.handleLanguage} />
            }
            return <BookingService appointmentDetails={appointmentDetails} handleLanguage={this.props.handleLanguage} />
        }

        let step = "2";
        if(calendarPageIsFirst) {
            step = "3";
        }
        return (
           <div>
               <Container>
                     <Row className="main-content">
                       <Col md={4} lg={6}>
                           <Row className="left-container">
                              <div className="company-header-container">
                                 <CompanyHeader alignment="left" schedule={appointmentDetails.schedule} id="companyHeaderDateId" />
                              </div>
                              <div className="brand-step-container">
                                <Brand step={step} schedule={appointmentDetails.schedule} />
                              </div>
                          </Row>
                       </Col>
                       <Col md={8} lg={6}>
                           <Row className="right-container">
                            <Card className="card-main">
                               <Card.Header>
                                    <h6 className="header-title"><strong>{t('provider.infos_message')}</strong></h6>
                               </Card.Header>

                               <Card.Body className="card-body-booking">
                                   <BookingAgentSelector
                                        handleAgentUpdate={this.handleAgentUpdate}
                                        appointmentDetails={appointmentDetails}/>
                               </Card.Body>

                               <Card.Footer>
                                  <NavigateBackButton
                                        ref={this.navigation}
                                        title={t('provider.step_three_button_back')}
                                        disable={false}
                                        appointmentDetails={this.props.appointmentDetails}
                                        navigate={this.navigate.bind(this, "BACK")} />
                                  <NavigateNextButton
                                          schedule={appointmentDetails.schedule}
                                          ref={this.navigation}
                                          title={t('provider.step_three_button_next')}
                                          disable={!this.state.agent}
                                          appointmentDetails={this.props.appointmentDetails}
                                          navigate={this.navigate.bind(this, "NEXT")} />
                                  </Card.Footer>
                             </Card>
                             <div className="space-below"></div>
                            </Row>
                       </Col>
                     </Row>
                     <Row className="footer-holder">
                         <CompanyFooter schedule={appointmentDetails.schedule} />
                     </Row>
                </Container>
           </div>
        );
    }
}

export default withTranslation()(BookingAgent)