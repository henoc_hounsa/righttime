import React, { Component } from "react";
import axios from "axios";
import apiUrl from "../../datas/apiUrl";
import {toast} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import ToastMessage from "../BookingCommon/ToastMessage";
import "../BookingCommon/Main.css";
import "./BookingAgent.css";
import Button from 'react-bootstrap/Button'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import Loader from 'react-loader-spinner'
import RTToastContainer from "../BookingCommon/RTToastContainer";
import { withTranslation } from 'react-i18next';

class BookingAgentSelector extends Component {

    constructor(props) {
        super(props);
        this.state = {
          users: [],
          agent: this.props.appointmentDetails.agent,
          loading: true,
          hasError: false,
        };

        this.agentSelection = this.agentSelection.bind(this);
        this.populateAppointmentProps = this.populateAppointmentProps.bind(this);
    }

    componentDidMount() {
        this.retrieveUserList();
    }

    retrieveUserList() {
        // Get XP user list based the alias
        let schedule = this.props.appointmentDetails.schedule;
        let customFlow = schedule.customDisplaySettings && schedule.customDisplaySettings.customFlow;
        let appDate = this.props.appointmentDetails.appointmentDateRequest;
        let appMonth = appDate.getMonth() + 1;
        let appDay = appDate.getDate();
        let appointmentDate = ("0" + appDay).slice(-2) + "/" + ("0" + appMonth).slice(-2) + "/" + appDate.getFullYear();
        axios.get(apiUrl.serverUrl + apiUrl.serviceGetUsers
            + "?userIds=" + this.props.appointmentDetails.service.delivery
            + "&serviceId=" + this.props.appointmentDetails.service.id
            + "&appointmentDate=" + appointmentDate
            + "&timeSlot=" + this.props.appointmentDetails.timeSlot
            + "&customFlow=" + (customFlow != null)
            + "&XP-USER-ALIAS=" +this.props.appointmentDetails.xpUserAlias,
            apiUrl.config).then(resp => {
             this.setState({
                users: resp.data,
                loading: false,
             });
        })
        .catch(error => {
            // Clean last agent Id.
            this.props.handleAgentUpdate("");
            //
            console.log(error);
            let errorMessage = "Some error occured while getting agent list. Try again"
            if(error.response != null) {
                errorMessage = "Error: " + error.response.data.message;
            }
            toast.error(<ToastMessage msg={errorMessage} icon="fa fa-times-circle" />);
            this.setState({loading: false, hasError: true});
        });
    }

    agentSelection = user => event => {
        if(this.state.agent === user.userId) {
            this.props.appointmentDetails.agent = "";
            this.props.appointmentDetails.agentFirstName = "";
            this.props.appointmentDetails.agentLastName = "";
            this.props.appointmentDetails.agentEmailAddress = "";
        } else {
            this.props.appointmentDetails.agent = user.userId;
            this.props.appointmentDetails.agentFirstName = user.firstname;
            this.props.appointmentDetails.agentLastName = user.lastname;
            this.props.appointmentDetails.agentEmailAddress = user.email;
            this.props.appointmentDetails.slots = [];
        }
        this.setState({
          agent: this.props.appointmentDetails.agent
        });

        this.props.handleAgentUpdate(this.props.appointmentDetails.agent);
    }

    populateAppointmentProps() {
        this.props.appointmentDetails.agent = this.state.agent;
    }

    render() {
        const { t } = this.props;
        this.populateAppointmentProps();
        let { users, agent, loading, hasError } = this.state

        return (
            <div className="scrollable-main" id="scrollId">
                <div className="scrollable-content">
                    <label className="card-body-title">{t('provider.infos_message_two')}</label>
                    <div className="booking-card-content">
                        {loading ?
                            <div className="loading-container">
                                <Loader
                                    type="ThreeDots"
                                    color="#00BFFF"
                                    height={100}
                                    width={100}
                                />
                            </div>
                            :
                            hasError ?
                                <RTToastContainer />
                            :
                                <ButtonGroup className="group-agent">
                                   {users.map(user => (
                                        <Button id={user.userId}
                                           key={user.userId}
                                            name={user.firstname + " " + user.lastname}
                                            className= {user.userId == agent && agent != "" ? "agent-info agent-info-active" : "agent-info"}
                                            onClick={this.agentSelection(user)} value={user} >
                                            {/* <img className="agent-profile-pic" src="/assets/booking/user-avatar.jpg" /><br /> */}
                                           <div className="agent-initial">{ user.firstname[0] +" "+ user.lastname[0] }</div>
                                            <label className="agent-name">{user.firstname}</label>
                                        </Button>
                                   ))}
                               </ButtonGroup>
                        }
                    </div>
                </div>
            </div>
        )
    }

}

export default withTranslation()(BookingAgentSelector)