import React from 'react';
import { cleanup, render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Topnavbar from './Topnavbar';

describe('<Topnavbar />', () => {
  afterEach(cleanup);

  test('it should mount', () => {
    const { getByTestId } = render(<Topnavbar />);
    const topnavbar = getByTestId('Topnavbar');

    expect(topnavbar).toBeInTheDocument();
  });
});