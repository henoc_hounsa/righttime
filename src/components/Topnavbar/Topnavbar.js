import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import i18n from "../../i18n";
import "./Topnavbar.module.css";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";
import { IoIosKeypad } from "react-icons/io";
import GetStore from "../../getStore.js";

function Topnavbar(props) {
  const storeData = GetStore();
  const navDropdownTitleLang = {
    en: (
      <span
        style={{
          display: "flex",
          flexDirection: "inline-row",
          alignItems: "center",
        }}
      >
        <img
          alt=""
          src="/assets/uk.svg"
          height="22"
          className="d-inline-block align-top"
        />{" "}
        &nbsp; &nbsp; <span className={"d-none d-md-block"}>English</span>
      </span>
    ),
    fr: (
      <span
        style={{
          display: "flex",
          flexDirection: "inline-row",
          alignItems: "center",
        }}
      >
        <img
          alt=""
          src="/assets/fr.svg"
          height="22"
          className="d-inline-block align-top"
        />{" "}
        &nbsp; &nbsp; <span className={"d-none d-md-block"}>Français</span>
      </span>
    ),
  };

  const definedLan = localStorage.getItem("rightTime_lang")
    ? localStorage.getItem("rightTime_lang")
    : "en";

  const { companyLanguage } = props;

  const [navDropdownTitle, setLang] = useState(
    navDropdownTitleLang[definedLan]
  );

  const changeLanguage = (lng) => {
    //console.log("lng = ", lng);
    i18n.changeLanguage(lng);
    setLang(navDropdownTitleLang[lng]);
    localStorage.setItem("rightTime_lang", lng);
  };

  useEffect(() => {
    window.setTimeout(() => {
      if (companyLanguage) {
        changeLanguage(companyLanguage[0].toLowerCase());
      }
    }, 1000);
  }, [companyLanguage]);

  const toggleSidenav = () => {
    var element = document.getElementsByClassName("dashShell")[0];

    if (element.classList) {
      element.classList.toggle("toggled");
    } else {
      // For IE9
      var classes = element.className.split(" ");
      var i = classes.indexOf("toggled");

      if (i >= 0) classes.splice(i, 1);
      else classes.push("toggled");
      element.className = classes.join(" ");
    }
  };
  const alias = window.location.hostname.split(".");
  //const logo = props.companyLogoPath;

  return (
    <Navbar
      id="topnavHeaderId"
      bg="white"
      className={"navbar-toggleable-xl mainNav"}
      expand="lg"
      fixed={props.fixed}
      style={{
        minHeight: props.height || 64,
        borderBottom: "solid .75px #F0F0F0",
      }}
    >
      {props.dashMenu ? (
        <button
          className={"btn btn-default pl-0"}
          onClick={() => {
            toggleSidenav();
          }}
        >
          <img src="/assets/sidebar/menu.svg" alt="" height={22} />
        </button>
      ) : (
        ""
      )}

      <Navbar.Brand href={props.brandHref ? props.brandHref : "/"}>
        <img
          alt=""
          id="headerImageId"
          src={props.companyLogoPath}
          width="124"
          className="d-inline-block align-top"
        />
      </Navbar.Brand>
      <Nav
        className="ml-auto"
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        {!storeData.userIsConnected && !props.scpage ? (
          <Nav.Link href="/onboarding">{props.t("sign_in")}</Nav.Link>
        ) : (
          ""
        )}
        {storeData.userIsConnected ? (
          <NavDropdown
            title={
              <span style={{ display: "flex", alignItems: "center" }}>
                <div style={{ display: "inline-block" }}>
                  {storeData.userFullName}
                </div>{" "}
                <span></span>
              </span>
            }
            id="basic-nav-dropdown"
            style={{ right: 0, after: " " }}
          >
            <NavDropdown.Item href="/onboarding">
              <img src="/assets/sidebar/user.svg" alt="" height={14} />
              &nbsp;&nbsp; {props.t("my_account")}
            </NavDropdown.Item>
            <NavDropdown.Item
              href={"https://" + alias[0] + ".account.rightcomtech.com/logout"}
            >
              <img src="/assets/sidebar/Logout.svg" alt="" height={14} />
              &nbsp;&nbsp; {props.t("logout")}
            </NavDropdown.Item>
          </NavDropdown>
        ) : (
          ""
        )}

        {companyLanguage ? (
          <NavDropdown
            title={navDropdownTitle}
            id="basic-nav-dropdown"
            style={{
              right: 0,
              after: "",
              cursor: companyLanguage.length > 1 ? "default" : "no-drop",
            }}
            disabled={companyLanguage.length <= 1 || props.language}
          >
            {companyLanguage.map((lng) => {
              return (
                <NavDropdown.Item
                  key={lng.toLowerCase()}
                  onClick={() => changeLanguage(lng.toLowerCase())}
                >
                  {navDropdownTitleLang[lng.toLowerCase()]}
                </NavDropdown.Item>
              );
            })}
          </NavDropdown>
        ) : (
          ""
        )}

        {!props.scpage ? (
          <Nav.Link href="#" style={{ padding: 5 }}>
            <IoIosKeypad style={{ fontSize: 26 }} />
          </Nav.Link>
        ) : (
          ""
        )}
      </Nav>
    </Navbar>
  );
}

Topnavbar.propTypes = {
  fixed: PropTypes.string,
  brandHref: PropTypes.string,
  dashMenu: PropTypes.bool,
  scpage: PropTypes.bool,
  companyLogoPath: PropTypes.string,
};

Topnavbar.defaultProps = {
  fixed: "",
  brandHref: "/",
  dashMenu: false,
  scpage: false,
  companyLogoPath: "",
};

export default Topnavbar;
