import React from 'react';
import { cleanup, render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Featuretwoo from './Featuretwoo';

describe('<Featuretwoo />', () => {
  afterEach(cleanup);

  test('it should mount', () => {
    const { getByTestId } = render(<Featuretwoo />);
    const featuretwoo = getByTestId('Featuretwoo');

    expect(featuretwoo).toBeInTheDocument();
  });
});