import React from 'react';
import { cleanup, render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Ctatwoo from './Ctatwoo';

describe('<Ctatwoo />', () => {
  afterEach(cleanup);

  test('it should mount', () => {
    const { getByTestId } = render(<Ctatwoo />);
    const ctatwoo = getByTestId('Ctatwoo');

    expect(ctatwoo).toBeInTheDocument();
  });
});