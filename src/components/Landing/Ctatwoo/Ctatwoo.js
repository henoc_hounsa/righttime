import React from "react";
import "./Ctatwoo.module.css";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import { withNamespaces } from "react-i18next";
const styles = {
  ctaTitle: {
    fontSize: 14,
    color: "#0089E1",
    fontWeight: "bold",
    textTransform: "uppercase",
    letterSpacing: 0.14
  },
  cta: {
    textAlign: "center",
    backgroundColor: "#FAFAFA",
    paddingTop: 90,
    paddingBottom: 90
  },
  ctaText: {
    color: "#373737",
    fontSize: 30,
    marginTop: 16,
    marginBottom: 26,
    letterSpacing: 0
  }
};
const Ctatwoo = ({ t }) => (
  <section
    id={"ctawoo"}
    className={styles.Ctatwoo}
    data-testid="Ctatwoo"
    style={styles.cta}
  >
    <Container>
      <div className={"cta2container"}>
        <Row>
          <Col>
            <h3 style={styles.ctaTitle}>{t("cta2_text1")}</h3>
            <p style={styles.ctaText}>{t("cta2_text2")}</p>
            <Button variant="primary" size="lg" active>
              {t("reach_us")}
            </Button>
          </Col>
        </Row>
      </div>
    </Container>
  </section>
);

Ctatwoo.propTypes = {};

Ctatwoo.defaultProps = {};

export default withNamespaces()(Ctatwoo);
