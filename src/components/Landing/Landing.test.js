import React from 'react';
import { cleanup, render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Landing from './Landing';

describe('<Landing />', () => {
  afterEach(cleanup);

  test('it should mount', () => {
    const { getByTestId } = render(<Landing />);
    const landing = getByTestId('Landing');

    expect(landing).toBeInTheDocument();
  });
});