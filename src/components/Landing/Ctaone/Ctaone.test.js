import React from 'react';
import { cleanup, render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Ctaone from './Ctaone';

describe('<Ctaone />', () => {
  afterEach(cleanup);

  test('it should mount', () => {
    const { getByTestId } = render(<Ctaone />);
    const ctaone = getByTestId('Ctaone');

    expect(ctaone).toBeInTheDocument();
  });
});