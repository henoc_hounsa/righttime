import React from 'react';
import { cleanup, render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Topcarousel from './Topcarousel';

describe('<Topcarousel />', () => {
  afterEach(cleanup);

  test('it should mount', () => {
    const { getByTestId } = render(<Topcarousel />);
    const topcarousel = getByTestId('Topcarousel');

    expect(topcarousel).toBeInTheDocument();
  });
});