import React from 'react';
import { cleanup, render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Videosection from './Videosection';

describe('<Videosection />', () => {
  afterEach(cleanup);

  test('it should mount', () => {
    const { getByTestId } = render(<Videosection />);
    const videosection = getByTestId('Videosection');

    expect(videosection).toBeInTheDocument();
  });
});