import React from 'react';
import { cleanup, render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Featureone from './Featureone';

describe('<Featureone />', () => {
  afterEach(cleanup);

  test('it should mount', () => {
    const { getByTestId } = render(<Featureone />);
    const featureone = getByTestId('Featureone');

    expect(featureone).toBeInTheDocument();
  });
});