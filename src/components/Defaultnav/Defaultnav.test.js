import React from 'react';
import { cleanup, render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Defaultnav from './Defaultnav';

describe('<Defaultnav />', () => {
  afterEach(cleanup);

  test('it should mount', () => {
    const { getByTestId } = render(<Defaultnav />);
    const defaultnav = getByTestId('Defaultnav');

    expect(defaultnav).toBeInTheDocument();
  });
});