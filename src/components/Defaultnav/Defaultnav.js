import React from "react";
import styles from "./Defaultnav.module.css";
import { withNamespaces } from "react-i18next";

const Defaultnav = ({ t }) => (
  <div className={styles.Defaultnav} data-testid="Defaultnav">
    Defaultnav Component
  </div>
);

Defaultnav.propTypes = {};

Defaultnav.defaultProps = {};

export default withNamespaces()(Defaultnav);
