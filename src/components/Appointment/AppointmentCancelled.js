import React, { Component } from "react";
import axios from "axios";
import apiUrl from "../../datas/apiUrl";
import {toast, ToastContainer} from "react-toastify";
import ToastMessage from "../BookingCommon/ToastMessage";
import { withRouter } from "react-router-dom";
import { Col, Row, Container } from "react-bootstrap";
import Button from 'react-bootstrap/Button'

import { useLocation } from 'react-router';
import queryString from 'query-string';
import "./AppointmentCancel.css";
import CompanyHeader from "../BookingCommon/CompanyHeader";
import RTToastContainer from "../BookingCommon/RTToastContainer";

class AppointmentCancel extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        var appointment = this.props.appointmentDetails;
        var service = this.props.appointmentDetails.service;
        var cancelCompleted = true;
        var timezone = appointment.schedule && appointment.schedule.rtTimezone ? appointment.schedule.rtTimezone : "";

        const app_cancel_heading = appointment.locale === "fr" ? "Rdv annulé." : "Appointment Cancelled.";
        const app_cancel_sub_heading1 = appointment.locale === "fr" ? "Votre rdv avec " : "Your appointment with ";
        const app_cancel_sub_heading2 = appointment.locale === "fr" ? "a été annulé." : "has been successfully cancelled.";
        const appointment_id = appointment.locale === "fr" ? "Référence du rdv" : "Appointment ID";
        const whit_label = appointment.locale === "fr" ? "Avec" : "With";
        const when_label = appointment.locale === "fr" ? "Quand" : "When";
        const where_label = appointment.locale === "fr" ? "Où" : "Where";
        const status_label = appointment.locale === "fr" ? "Statut" : "Status";
        const cancelled_label = appointment.locale === "fr" ? "Annulé" : "Cancelled";

        return(
            <div className="AppointmentCancel">
                <Container>
                   <Row className="main-content">
                    {
                        cancelCompleted ?
                        <RTToastContainer
                            /> : <div />
                    }
                    <div className="rightDetails">
                        <Row className="row-center">
                        <div className="companyLogo text-center">
                            <CompanyHeader schedule={appointment.schedule} page="cancel" locale={appointment.locale} />
                        </div>
                        <div className="appointment-container">
                            <h4 className="appointment-container-title">{app_cancel_heading}</h4>
                            <label className="appointment-container-title-sub">
                                    {app_cancel_sub_heading1} {appointment.companyName} {app_cancel_sub_heading2}
                            </label>
                            <div className="appointmentDetails">
                                <table>
                                    <tr>
                                        <td className="appointment-details-label">Appointment ID:</td>
                                        <td>{appointment.referenceNumber}</td>
                                    </tr>
                                    <tr>
                                        <td className="appointment-details-label">Service:</td>
                                        <td>{appointment.locale === "fr" ? service.name?.fr : service.name?.en}</td>
                                    </tr>
                                    <tr>
                                        <td className="appointment-details-label">{whit_label}:</td>
                                        <td>{appointment.agentFirstName} {appointment.agentLastName}</td>
                                    </tr>
                                    <tr>
                                        <td className="appointment-details-label">{when_label}:</td>
                                        <td>{appointment.appointmentDateLabel}</td>
                                    </tr>
                                    <tr>
                                        <td className="appointment-details-label">{where_label}:</td>
                                        <td>{appointment.serviceLocation}</td>
                                    </tr>
                                    <tr>
                                        <td className="appointment-details-label">{status_label}:</td>
                                        <td>{cancelled_label}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        </Row>
                    </div>
                   </Row>
                </Container>
            </div>
        );
    }

}

export default withRouter(AppointmentCancel);