import React, { Component } from "react";
import axios from "axios";
import apiUrl from "../../datas/apiUrl";
import {toast, ToastContainer} from "react-toastify";
import ToastMessage from "../BookingCommon/ToastMessage";
import { withRouter } from "react-router-dom";
import { Col, Row, Container } from "react-bootstrap";
import Button from 'react-bootstrap/Button'

import { useLocation } from 'react-router';
import queryString from 'query-string';
import "./AppointmentPast.css";
import RTToastContainer from "../BookingCommon/RTToastContainer";

class AppointmentPost extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        var appointment = this.props.appointmentDetails;
        var service = this.props.appointmentDetails.service;
        var cancelCompleted = true;
        var timezone = appointment.schedule && appointment.schedule.rtTimezone ? appointment.schedule.rtTimezone : "";

        const title_label = appointment.locale === "fr" ? "Passée Rdv" : "Past Appointment";
        const message_label = appointment.locale === "fr" ? "Le rendez-vous que vous essayez d'annuler est passé." : "The appointment you are looking to cancel is in the past.";
        const appointment_id = appointment.locale === "fr" ? "Référence du rdv" : "Appointment ID";
        const dear_label = appointment.locale === "fr" ? "Cher" : "Dear";
        const whit_label = appointment.locale === "fr" ? "Avec" : "With";
        const when_label = appointment.locale === "fr" ? "Quand" : "When";
        const where_label = appointment.locale === "fr" ? "Où" : "Where";
        const new_appointment_label = appointment.locale === "fr" ? "Nouveau Rdv" : "NEW APPOINTMENT";

        return(
            <div className="appointment-post">
                <Container className="appointment-body">
                       <div className="main-wrapper">
                           <div className="card mailCard post-mailCard">
                                <div className="post-top p-3 d-flex align-items-center justify-content-center flex-column">
                                    <span class="banner-title">{title_label}</span>
                                    <div className="banner-images">
                                        <img src="/assets/booking/calendar-illustration.svg" className="banner-image-main" alt=""/>
                                    </div>
                                    <div className="banner-images-sub">
                                        <img src="/assets/booking/man.svg" className="banner-image-sub" alt=""/>
                                    </div>
                                </div>

                                <div className=" card-body p-5 post-details">
                                    <Row className="row-center post-details-container">
                                    <div className="appointment-post-container">
                                        <div className="appointment-post-message-container">
                                            <label>{dear_label} {appointment.firstName} {appointment.lastName}</label>
                                            <br />
                                            <label className="appointment-post-message">
                                                    {message_label}
                                            </label>
                                        </div>
                                        <div className="appointmentDetails">
                                            <table>
                                                <tr>
                                                    <td className="appointment-details-label">{appointment_id}:</td>
                                                    <td>{appointment.referenceNumber}</td>
                                                </tr>
                                                <tr>
                                                    <td className="appointment-details-label">Service:</td>
                                                    <td>{appointment.locale === "fr" ? service.name?.fr : service.name?.en}</td>
                                                </tr>
                                                <tr>
                                                    <td className="appointment-details-label">{whit_label}:</td>
                                                    <td>{appointment.agentFirstName} {appointment.agentLastName}</td>
                                                </tr>
                                                <tr>
                                                    <td className="appointment-details-label">{when_label}:</td>
                                                    <td>{appointment.appointmentDateLabel}</td>
                                                </tr>
                                                <tr>
                                                    <td className="appointment-details-label">{where_label}:</td>
                                                    <td>{appointment.serviceLocationLabel}</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <Row className="button-container">
                                            <div styleName="padding: 32px 10px 0; text-align: center">
                                                <Button className="new-appointment-button" href="/">{new_appointment_label}</Button>
                                            </div>
                                        </Row>
                                        <Row>
                                            <div className="bottomLink">
                                                    A product of <a href="https://right-com.com/" target="_blank"><img height="17" width="72"
                                                        src="/assets/booking/rightcom.svg" /></a>
                                            </div>
                                        </Row>
                                    </div>
                                    </Row>
                                </div>
                            </div>
                        </div>
                </Container>
            </div>
        );
    }

}

export default withRouter(AppointmentPost);