import React, { Component } from "react";
import axios from "axios";
import apiUrl from "../../datas/apiUrl";
import {toast, ToastContainer} from "react-toastify";
import ToastMessage from "../BookingCommon/ToastMessage";
import { withRouter } from "react-router-dom";
import { Col, Row, Container } from "react-bootstrap";
import Button from 'react-bootstrap/Button'

import { useLocation } from 'react-router';
import queryString from 'query-string';
import "./AppointmentCancel.css";
import CompanyHeader from "../BookingCommon/CompanyHeader";
import AppointmentCancelled from "./AppointmentCancelled.js";
import AppointmentPast from "./AppointmentPast.js";

class AppointmentCancel extends Component {

    constructor(props) {
        super(props);
         this.state = {
            appointmentId: this.props.match.params.id,
            appointment: "",
            service: "",
            cancelSuccess: false,
            isLoading: true,
            submitting: false,
        }

        this.handleCancelAppointment = this.handleCancelAppointment.bind(this);
        this.isAppointmentAlreadyPastTime = this.isAppointmentAlreadyPastTime.bind(this);
    }

    componentDidMount() {
        axios.get(apiUrl.serverUrl + apiUrl.appointmentGetById
            + "?id=" + this.state.appointmentId,
            apiUrl.config).then(response => {
            this.setState({
                appointment: response.data,
                service: response.data.service,
                isLoading: false,
            })
        })
        .catch(error => {
            console.log(error);
            let errorMessage = error.name + ": " + error.message;
            if(error.response != null) {
                errorMessage = "Error: " + error.response.data.message;
            }
            toast.error(<ToastMessage msg={errorMessage} icon="fa fa-times-circle" />);
            this.setState({isLoading: false, hasError: true});
        });
    }

    handleCancelAppointment() {
        this.setState({ submitting: true });
        var appointment = {
            id: this.state.appointmentId,
            status: "CANCELLED"
        };
        axios.post(apiUrl.serverUrl + apiUrl.appointmentCancel, appointment,
        apiUrl.config).then(response => {
            this.setState({ submitting: false });
            if(response.status === 200) {
                this.setState({
                    cancelSuccess: true,
                });
                var successMessage = "Your appointment has been successfully cancelled.";
                if(response.data.locale === "fr") {
                    successMessage = "Votre rdv a été annulé.";
                }
                toast.success(<ToastMessage msg={successMessage} icon="fa fa-times-circle" />);
            }
        })
        .catch(error => {
            console.log(error);
            this.setState({ submitting: false });
            let errorMessage = error.name + ": " + error.message;
            if(error.response != null) {
                errorMessage = "Error: " + error.response.data.message;
            }
            toast.error(<ToastMessage msg={errorMessage} icon="fa fa-times-circle" />);
            this.setState({loading: false, hasError: true});
        });
    }


    isAppointmentAlreadyPastTime(appointmentDate, endTime) {
        var date = new Date();
        var appointmentDateArray = appointmentDate.split("/");
        var endTimeArray = endTime.split(":");
        console.log(appointmentDateArray);
        console.log(endTimeArray);

        // Set the appointment date and time
        date.setFullYear(appointmentDateArray[2], appointmentDateArray[1] - 1, appointmentDateArray[0]);
        date.setHours(endTimeArray[0], endTimeArray[1], 0);
        console.log("Appointment Time", date);
        return date < new Date();
    }

    render() {
        var { appointment, service, cancelSuccess, isLoading, submitting } = this.state;
        var timezone = appointment.schedule && appointment.schedule.rtTimezone ? appointment.schedule.rtTimezone : "";
        console.log("appointment", appointment);
        var confirmLabel = appointment.locale === "fr" ? "Confirmer" : "Confirm";
        if(submitting) {
            confirmLabel = "SUBMITTING...";
        }

        if(appointment !== "" && this.isAppointmentAlreadyPastTime(appointment.appointmentDate, appointment.endTime)) {
            return <AppointmentPast appointmentDetails={appointment} />
        }


        if(appointment.status === "CANCELLED" || cancelSuccess) {
            return <AppointmentCancelled appointmentDetails={appointment} />
        }

        const app_cancel_heading = appointment.locale === "fr" ? "Confirmation d'annulation de rdv" : "Appointment Cancellation Confirmation";
        const app_cancel_sub_heading = appointment.locale === "fr" ? "Veuillez cliquer sur confirmer si vous souhaitez annuler votre rdv." : "Please click confirm if you want to cancel your appointment.";
        const appointment_id = appointment.locale === "fr" ? "Référence du rdv" : "Appointment ID";
        const whit_label = appointment.locale === "fr" ? "Avec" : "With";
        const when_label = appointment.locale === "fr" ? "Quand" : "When";
        const where_label = appointment.locale === "fr" ? "Où" : "Where";
        const cancel_label = appointment.locale === "fr" ? "Annuler" : "Cancel";
        return(
            <div className="AppointmentCancel">
                {isLoading ? <div /> :
                <Container>
                   <Row className="main-content">
                    {
                        cancelSuccess ?
                        <ToastContainer
                            position="top-center"
                            toastClassName="general-toast"
                            autoClose={false}/> : <div />
                    }
                    <div className="rightDetails">
                        <Row className="row-center">
                        <div className="companyLogo text-center">
                            <CompanyHeader schedule={appointment.schedule} page="cancel" locale={appointment.locale} />
                        </div>
                        <div className="appointment-container">
                            <div className="title-section">
                                <h4 className="appointment-container-title">{app_cancel_heading}</h4>
                                <label className="appointment-container-title-sub">
                                        {app_cancel_sub_heading}
                                </label>
                            </div>
                            <div className="appointmentDetails">
                                <table>
                                    <tr>
                                        <td className="appointment-details-label">{appointment_id}:</td>
                                        <td>{appointment.referenceNumber}</td>
                                    </tr>
                                    <tr>
                                        <td className="appointment-details-label">Service:</td>
                                        <td>{appointment.locale === "fr" ? service.name?.fr : service.name?.en}</td>
                                    </tr>
                                    <tr>
                                        <td className="appointment-details-label">{whit_label}:</td>
                                        <td>{appointment.agentFirstName} {appointment.agentLastName}</td>
                                    </tr>
                                    <tr>
                                        <td className="appointment-details-label">{when_label}:</td>
                                        <td>{appointment.appointmentDateLabel}</td>
                                    </tr>
                                    <tr>
                                        <td className="appointment-details-label">{where_label}:</td>
                                        <td>{appointment.serviceLocationLabel}</td>
                                    </tr>
                                </table>
                            </div>
                            <div className="appointment-container-buttons">
                                <Button className="actionLinks" disabled={submitting} onClick={this.handleCancelAppointment}>{confirmLabel}</Button>
                                <Button className="actionLinks btn-cancel" href="/">{cancel_label}</Button>
                            </div>
                        </div>
                        </Row>
                    </div>
                   </Row>
                </Container>
                }
            </div>
        );
    }

}

export default withRouter(AppointmentCancel);