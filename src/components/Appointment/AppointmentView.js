import React, { Component } from "react";
import axios from "axios"; 
import apiUrl from "../../datas/apiUrl";
import {toast, ToastContainer} from "react-toastify";
import ToastMessage from "../BookingCommon/ToastMessage";
import { withRouter } from "react-router-dom";
import { Col, Row, Container } from "react-bootstrap";
import Pluralize from 'pluralize';

import { useLocation } from 'react-router';
import queryString from 'query-string';
import "./AppointmentView.css";
import { withTranslation } from 'react-i18next';

class AppointmentView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            appointment: "",
            service: "",
        }
    }

    componentDidMount() {
        axios.get(apiUrl.serverUrl + apiUrl.appointmentGetById
            + "?id=" + this.props.match.params.id,
            apiUrl.config).then(response => {
            console.log(response.data);
            this.setState({
                appointment: response.data,
                service: response.data.service,
            })
        })
        .catch(error => {
            console.log(error);
            let errorMessage = error.name + ": " + error.message;
            if(error.response != null) {
                errorMessage = "Error: " + error.response.data.message;
            }
            toast.error(<ToastMessage msg={errorMessage} icon="fa fa-times-circle" />);
            this.setState({loading: false, hasError: true});
        });
    }

    getInitials() {
        var name = this.state.appointment.agentFirstName + " " + this.state.appointment.agentLastName;
        var names = name.split(' ');
        var initials = names[0].substring(0, 1).toUpperCase();
        if (names.length > 1) {
            initials += names[names.length - 1].substring(0, 1).toUpperCase();
        }
        return initials;
    }


    render() {
        const { t } = this.props;
        var { appointment, service } = this.state;
        var duration = service.duration;
        var durationType = appointment.serviceDurationTypeLabel;
        var durationTypeLabel = "";
        if(typeof(duration) != "undefined" && typeof(durationType) != "undefined") {
           durationTypeLabel = Pluralize( durationType, duration );
        }
        const app_overview_label = appointment.locale === "fr" ? "Aperçu du rdv" : "Appointment Overview";
        const svc_provider_label = appointment.locale === "fr" ? "Responsable de service" : "Service Provider";
        const appointment_details_label = appointment.locale === "fr" ? "Détails du rdv" : "Appointment Details";
        const name_label = appointment.locale === "fr" ? "Nom" : "Name";
        const from_label = appointment.locale === "fr" ? "De" : "From";
        const service_label = appointment.locale === "fr" ? "Service" : "Service";
        const time_label = appointment.locale === "fr" ? "Heure" : "Time";
        const date_label = appointment.locale === "fr" ? "Date" : "Date";
        const duration_label = appointment.locale === "fr" ? "Durée" : "Duration";
        const location_label = appointment.locale === "fr" ? "Lieu" : "Location";
        const cost_label = appointment.locale === "fr" ? "Tarif" : "Cost";
        return(
            <div className="AppointmentView">
                <Container className="appointment-body">
                       <div className="emailBg">
                           <div className="card mailCard">

                               <div className="card-top p-3 d-flex align-items-center justify-content-center flex-column">
                                   <span class="serviceName">{app_overview_label}</span>
                                   <span class="appointId">ID: {appointment.referenceNumber}</span>
                               </div>

                               <div className="card-body">
                                   <div className = "content">
                                       <div className="section mb-3 card-body-title">
                                           {svc_provider_label}
                                       </div>
                                       <div className="d-flex pb-2 mb-4 service-provider-container" styleName="border-bottom: solid 1px #0000000B">
                                           <div className="flex-grow-0 service-provider-initial-section">
                                               <div class="providerInitials">{this.getInitials()}</div>
                                           </div>
                                           <div className="service-provider-section">
                                                <div className="name-section">
                                                   <span className="sectionTitle">{name_label}</span>
                                                   <div className="sectionValue">{appointment.agentFirstName} {appointment.agentLastName}</div>
                                                </div>
                                               <div className="company-section">
                                                   <span className="sectionTitle">{from_label}</span>
                                                   <div className="sectionValue">{appointment.companyName}</div>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="section mb-3">
                                            <hr className="line-separator" />
                                           <label className="details-title">{appointment_details_label}</label>
                                       </div>
                                       <div className="details">
                                           <div className="row details-row">
                                               <div class="details-left col-md-6">
                                                   <span className="sectionTitle">{service_label}</span>
                                                   <div className="sectionValue">{appointment.locale === "fr" ? service.name?.fr : service.name?.en}</div>
                                              </div>
                                              <div class="details-right col-md-6">
                                                   <span className="sectionTitle">{date_label}</span>
                                                   <div className="sectionValue">{appointment.appointmentDate}</div>
                                              </div>
                                          </div>
                                          <div className="row details-row">
                                                 <div class="details-left col-md-6">
                                                     <span className="sectionTitle">{time_label}</span>
                                                    <div className="sectionValue">{appointment.startTime} to {appointment.endTime}</div>
                                                </div>
                                                <div class="details-right col-md-6">
                                                     <span className="sectionTitle">{duration_label}</span>
                                                     <div className="sectionValue">{service.duration} <label className="duration-type">{durationTypeLabel}</label></div>
                                                </div>
                                            </div>
                                          <div className="row details-row">
                                                 <div class="details-left col-md-6">
                                                     <span className="sectionTitle">{location_label}</span>
                                                     <div className="sectionValue">{appointment.serviceLocationLabel}</div>
                                                </div>
                                                <div class="details-right col-md-6">
                                                     <span className="sectionTitle">{cost_label}</span>
                                                    <div className="sectionValue">{service.serviceCost ? service.serviceCostCurrency : ""} {service.serviceCost}</div>
                                                </div>
                                            </div>
                                       </div>
                                   </div>
                               </div>

                           </div>
                       </div>
                </Container>
            </div>
        );
    }

}
export default withRouter(AppointmentView);