import React from "react";
import { Col, Row, Container } from "react-bootstrap";
import GetStore from "../../getStore";
import "./Schedulepage.module.css";
import Footer from "../Footer/Footer";
import Topnavbar from "../Topnavbar/Topnavbar";
import GetCompany from "../../getCompany";

function Schedulepage(props) {
  const storeData = GetStore();
  const companyData = GetCompany(window.location.hostname);
  return (
    <div className={"Schedulepage"} data-testid="Schedulepage">
      <Topnavbar scpage={true} />
      <Container>
        <Row style={{ height: "calc(100vh - 148px)", alignItems: "center" }}>
          <Col md={6}>
            <p style={{ fontSize: 24, color: "#818181" }}>Welcome to</p>
            <h1 style={{ fontSize: 30, fontWeight: "bold" }}>
              {companyData.registered ? (
                companyData.name
              ) : (
                <span>A Company </span>
              )}{" "}
              Scheduling Page
            </h1>
            <p style={{ fontSize: 24, color: "#818181" }}>
              This page will be up soon!
            </p>
          </Col>
          <Col md={6}>

            

          </Col>
        </Row>
      </Container>
      <Footer />
    </div>
  );
}

Schedulepage.propTypes = {};

Schedulepage.defaultProps = {};

export default Schedulepage;
