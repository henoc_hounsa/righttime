import React from 'react';
import { cleanup, render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Schedulepage from './Schedulepage';

describe('<Schedulepage />', () => {
  afterEach(cleanup);

  test('it should mount', () => {
    const { getByTestId } = render(<Schedulepage />);
    const schedulepage = getByTestId('Schedulepage');

    expect(schedulepage).toBeInTheDocument();
  });
});