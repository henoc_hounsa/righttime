import React, { Component } from "react";
import { Col, Row, Container } from "react-bootstrap";
import Card from 'react-bootstrap/Card'
import 'react-day-picker/lib/style.css';
import "./BookingDate.css";
import "../BookingCommon/Main.css";
import CompanyHeader from "../BookingCommon/CompanyHeader";
import CompanyFooter from "../BookingCommon/CompanyFooter";
import Brand from "../BookingCommon/Brand";
import BookingInformation from "../BookingInformation/BookingInformation";
import BookingAgent from "../BookingAgent/BookingAgent";
import BookingService from "../BookingService/BookingService";
import BookingSlot from "../BookingDate/BookingSlot";
import NavigateNextButton from "../BookingWidget/NavigateNextButton";
import NavigateBackButton from "../BookingWidget/NavigateBackButton";
import bookingConstants from "../../constants/bookingConstants";
import { withTranslation } from 'react-i18next';

class BookingDate extends Component {

    constructor(props) {
        super(props);
        this.state = {
          navigate: "",
          timeSlot: this.props.appointmentDetails.timeSlot
        };

        this.navigate = this.navigate.bind(this);
        this.handleTimeSlotUpdate = this.handleTimeSlotUpdate.bind(this);
    }

    componentDidMount() {
        // Init TimeSlot.
        this.handleTimeSlotUpdate(true);
        // Deactive language change.
        this.props.handleLanguage(true);
    }

    navigate(val) {
        this.setState({navigate: val});
    }

    handleTimeSlotUpdate(init=false) {
        this.setState({
            timeSlot: init ? "" : this.props.appointmentDetails.timeSlot
        })
    }

    render() {
        const { t } = this.props;
        let appointmentDetails = this.props.appointmentDetails;
        let schedule = appointmentDetails.schedule;
        let customFlow = schedule.customDisplaySettings && schedule.customDisplaySettings.customFlow;
        let calendarPageIsFirst = customFlow && schedule.customDisplaySettings.customizeFlow[1] === bookingConstants.CALENDAR;

        if(this.state.navigate == "NEXT") {
            if(customFlow && calendarPageIsFirst) {
                return <BookingAgent appointmentDetails={appointmentDetails} handleLanguage={this.props.handleLanguage} />
            }
            return <BookingInformation appointmentDetails={appointmentDetails} handleLanguage={this.props.handleLanguage} />
        } else if(this.state.navigate == "BACK") {
            // If Calendar is assigned first, then show Service page using BACK
            if(customFlow && calendarPageIsFirst) {
                return <BookingService appointmentDetails={appointmentDetails} handleLanguage={this.props.handleLanguage} />
            }
            return <BookingAgent appointmentDetails={appointmentDetails} handleLanguage={this.props.handleLanguage} />
        }

        let link = localStorage.getItem("rightTime_lang") === "en" ? "with " : "avec "
        let agentName = appointmentDetails.agent ? link + appointmentDetails.agentFirstName + " " + appointmentDetails.agentLastName : "";
        let step = "3";
        if(calendarPageIsFirst) {
            step = "2";
        }

        return (
           <div>
               <Container>
                     <Row className="main-content">
                       <Col md={4} lg={6}>
                          <Row className="left-container">
                             <div className="company-header-container">
                                <CompanyHeader alignment="left" schedule={appointmentDetails.schedule} id="companyHeaderDateId" />
                             </div>
                             <div className="brand-step-container">
                               <Brand step={step} schedule={appointmentDetails.schedule} />
                             </div>
                         </Row>
                       </Col>
                       <Col md={8} lg={6}>
                           <Row className="right-container">
                            <Card className="card-main">
                               <Card.Header>
                                   <h6 className="header-title"><strong><label className="header-label">{t('date.appointment_title')} {agentName}</label></strong></h6>
                               </Card.Header>

                               <Card.Body className="card-body-booking">
                                   <BookingSlot
                                       handleTimeSlotUpdate={this.handleTimeSlotUpdate}
                                       appointmentDetails={appointmentDetails} />
                               </Card.Body>

                               <Card.Footer>
                                 <NavigateBackButton
                                     ref={this.navigation}
                                     title={t('date.step_two_button_back')}
                                     disable={false}
                                     appointmentDetails={this.props.appointmentDetails}
                                     navigate={this.navigate.bind(this, "BACK")} />
                               <NavigateNextButton
                                       schedule={appointmentDetails.schedule}
                                       ref={this.navigation}
                                       title={t('date.step_two_button_next')}
                                       disable={!this.state.timeSlot}
                                       appointmentDetails={this.props.appointmentDetails}
                                       navigate={this.navigate.bind(this, "NEXT")} />
                               </Card.Footer>

                             </Card>
                             <div className="space-below"></div>
                            </Row>
                       </Col>
                     </Row>
                     <Row className="footer-holder">
                         <CompanyFooter schedule={appointmentDetails.schedule} />
                     </Row>
                </Container>
           </div>
        );
    }
}

export default withTranslation()(BookingDate)
