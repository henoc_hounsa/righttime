import React, { Component } from "react";
import axios from "axios";
import apiUrl from "../../datas/apiUrl";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ToastMessage from "../BookingCommon/ToastMessage";
import "../BookingCommon/Main.css";
import "./BookingDate.css";
import DayPicker from "react-day-picker";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Button from "react-bootstrap/Button";
import Loader from "react-loader-spinner";
import RTToastContainer from "../BookingCommon/RTToastContainer";
import { withTranslation } from "react-i18next";
import MomentLocaleUtils from "react-day-picker/moment";
import "moment/locale/fr";
import moment from "moment";

const currentYear = new Date().getFullYear();
const fromMonth = new Date(currentYear, new Date().getMonth());
const toMonth = new Date(currentYear + 10, 11);

function Weekday({ weekday, className, localeUtils, locale }) {
  const weekdayName = localeUtils.formatWeekdayLong(weekday, locale);
  return (
    <div className={className} title={weekdayName}>
      {weekdayName.slice(0, 3)}
    </div>
  );
}

function YearMonthForm({ date, localeUtils, onChange }) {
  const handleChange = function handleChange(e) {
    const { year, month } = e.target.form;
    onChange(new Date(year.value, month.value));
  };

  const month = date.toLocaleString(localStorage.getItem("rightTime_lang"), {
    month: "long",
  });

  return (
    <form className="DayPicker-Caption">
      {month}{" "}
      <span className="day-picker-select-year">{date.getFullYear()}</span>
    </form>
  );
}

class BookingSlot extends Component {
  constructor(props) {
    super(props);
    this.state = {
      navigate: "",
      appointmentDateRequest: this.props.appointmentDetails
        .appointmentDateRequest,
      agentName: this.props.appointmentDetails.agentName,
      timeSlot: this.props.appointmentDetails.timeSlot,
      slots: this.props.appointmentDetails.slots,
      blockedDays: [],
      selectedDate: "",
      loading: false,
      hasError: false,
      calendarMonthYear: new Date(),
      isValidatingSlots: false,
    };

    this.handleDateSelection = this.handleDateSelection.bind(this);
    this.handleYearMonthChange = this.handleYearMonthChange.bind(this);
    this.calendarChange = this.calendarChange.bind(this);
  }

  calendarChange(day) {
    this.props.appointmentDetails.slots = [];
    var date = new Date();
    var month = day ? day.getMonth() + 1 : date.getMonth() + 1;
    var year = day ? day.getFullYear() : date.getFullYear();
    var appointment = this.props.appointmentDetails;
    this.setState({
      isValidatingSlots: true,
      calendarMonthYear: new Date(year, month - 1),
    });
    axios
      .post(
        apiUrl.serverUrl +
          apiUrl.blockedDayGetAll +
          "?month=" +
          ("0" + month).slice(-2) +
          "&year=" +
          year,
        appointment,
        apiUrl.config
      )
      .then((days) => {
        this.setState({
          blockedDays: days.data,
          isValidatingSlots: false,
        });
      })
      .catch((error) => {
        console.log(error);
        let errorMessage = error.name + ": " + error.message;
        if (error.response != null) {
          errorMessage = "Error: " + error.response.data.message;
        }
        toast.error(
          <ToastMessage msg={errorMessage} icon="fa fa-times-circle" />
        );
        this.setState({
          loading: false,
          hasError: true,
          isValidatingSlots: false,
        });
      });
  }

  componentDidMount() {
    this.calendarChange();
  }

  handleDateSelection(date) {
    console.log("Date selected", date);
    this.props.appointmentDetails.slots = [];
    this.props.appointmentDetails.appointmentDateRequest = date;
    this.setState({ loading: true });
    console.log("this.props.appointmentDetails", this.props.appointmentDetails);
    const slotResponse = axios
      .post(
        apiUrl.serverUrl + apiUrl.bookingTimeSlot,
        this.props.appointmentDetails,
        apiUrl.config
      )
      .then((slots) => {
        this.props.appointmentDetails.slots = slots.data;
        this.setState({
          slots: slots.data,
          appointmentDateRequest: date,
          selectedDate: date, 
          loading: false,
        });

        var element = document.getElementById("slotsId");
        element.scrollIntoView();
        element.scrollIntoView(false);
        element.scrollIntoView({ block: "start" });
        element.scrollIntoView({
          behavior: "smooth",
          block: "start",
          inline: "nearest",
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  selectSlot(time, date, slots) {
    if (this.props.appointmentDetails.timeSlot == time) {
      this.props.appointmentDetails.timeSlot = "";
    } else {
      this.props.appointmentDetails.timeSlot = time;
    }
    this.setState({
      timeSlot: this.props.appointmentDetails.timeSlot,
    });
    this.props.handleTimeSlotUpdate();
  }

  handleYearMonthChange(month) {
    this.setState({ month });
  }

  render() {
    const { t } = this.props;
    var {
      slots,
      blockedDays,
      appointmentDateRequest,
      selectedDate,
      timeSlot,
      loading,
      hasError,
      isValidatingSlots,
      calendarMonthYear,
    } = this.state;
    var today = new Date();
    var dayOffs = this.props.appointmentDetails.schedule.offWorkDays;

    var bookingDayLimit = this.props.appointmentDetails.schedule
      .bookingDayLimit;
    var allowScheduleLimit = this.props.appointmentDetails.schedule
      .allowScheduleLimit;
    var bookingDayLimitType = this.props.appointmentDetails.schedule
      .bookingDayLimitType;

    var limitDate = null;
    if (allowScheduleLimit && bookingDayLimitType && bookingDayLimit > 0) {
      if (bookingDayLimitType && bookingDayLimitType.startsWith("Month")) {
        bookingDayLimit = bookingDayLimit * 30;
      } else if (
        bookingDayLimitType &&
        bookingDayLimitType.startsWith("Week")
      ) {
        bookingDayLimit = bookingDayLimit * 7;
      }
      limitDate = new Date();
      limitDate.setDate(limitDate.getDate() + bookingDayLimit);
    }

    // setup blocked days days to be disable from calendar
    var disabledDayList = [];
    for (let i = 0; i < blockedDays.length; i++) {
      var date = blockedDays[i].split("/");
      disabledDayList.push(new Date(date[2], date[1] - 1, date[0]));
    }

    var weekday = new Array(7);
    var month = new Array(12);
    if (localStorage.getItem("rightTime_lang") === "en") {
      weekday[0] = "Sunday";
      weekday[1] = "Monday";
      weekday[2] = "Tuesday";
      weekday[3] = "Wednesday";
      weekday[4] = "Thursday";
      weekday[5] = "Friday";
      weekday[6] = "Saturday";

      month[0] = "January";
      month[1] = "February";
      month[2] = "March";
      month[3] = "April";
      month[4] = "May";
      month[5] = "June";
      month[6] = "July";
      month[7] = "August";
      month[8] = "September";
      month[9] = "October";
      month[10] = "November";
      month[11] = "December";
    } else {
      weekday[0] = "Dimanche";
      weekday[1] = "Lundi";
      weekday[2] = "Mardi";
      weekday[3] = "Mercredi";
      weekday[4] = "Jeudi";
      weekday[5] = "Vendredi";
      weekday[6] = "Samedi";

      month[0] = "Janvier";
      month[1] = "Février";
      month[2] = "Mars";
      month[3] = "Avril";
      month[4] = "Mai";
      month[5] = "Juin";
      month[6] = "Juillet";
      month[7] = "Août";
      month[8] = "Septembre";
      month[9] = "Octobre";
      month[10] = "Novembre";
      month[11] = "Décembre";
    }

    const selectStyle = `.DayPicker-Day--highlighted {
          background-color: #0089E1;
          color: white;
        }`;

    const modifiers = {
      highlighted: selectedDate,
    };

    // All slots have the same timezone
    let timezone = slots.length > 0 ? slots[0].rtTimezone : "";
    return (
      <div className="scrollable-main scrollable-main-calendar" id="scrollId">
        <div className="scrollable-content">
          <label className="card-body-title">{t("date.infos_message")}</label>
          {hasError ? (
            <RTToastContainer />
          ) : (
            <div className="booking-calendar-main">
              <style>{selectStyle}</style>
              {isValidatingSlots ? (
                <div className="loading-container">
                  <Loader
                    type="ThreeDots"
                    color="#00BFFF"
                    height={100}
                    width={100}
                  />
                </div>
              ) : (
                <div>
                  <DayPicker
                    className="appointment-calendar"
                    localeUtils={MomentLocaleUtils}
                    locale={localStorage.getItem("rightTime_lang")}
                    month={calendarMonthYear}
                    modifiers={modifiers}
                    weekdayElement={<Weekday />}
                    disabledDays={[
                      { daysOfWeek: dayOffs },
                      { before: today },
                      { after: limitDate },
                    ].concat(disabledDayList)}
                    toMonth={limitDate}
                    onDayClick={this.handleDateSelection}
                    onMonthChange={this.calendarChange}
                    captionElement={({ date, localeUtils }) => (
                      <YearMonthForm date={date} localeUtils={localeUtils} />
                    )}
                  />
                  <div
                    id="slotsId"
                    className={slots.length > 0 ? "select-slot-panel" : "hide"}
                  >
                    <label className="select-slot-label">
                      {t("date.time_message_one")}{" "}
                      {localStorage.getItem("rightTime_lang") === "en"
                        ? weekday[appointmentDateRequest.getDay()] +
                          ", " +
                          month[appointmentDateRequest.getMonth()] +
                          " " +
                          appointmentDateRequest.getDate() +
                          ", " +
                          appointmentDateRequest.getFullYear() +
                          "."
                        : weekday[appointmentDateRequest.getDay()] +
                          " " +
                          appointmentDateRequest.getDate() +
                          " " +
                          month[appointmentDateRequest.getMonth()] +
                          " " +
                          appointmentDateRequest.getFullYear()}
                    </label>
                    {timezone === null ? (
                      <div />
                    ) : (
                      <div>
                        <label className="label-timezone">
                          {t("date.time_message_two")} {timezone.capital} /{" "}
                          {timezone.country} ({timezone.timeoffset})
                        </label>
                      </div>
                    )}
                    {loading ? (
                      <div className="loading-container">
                        <Loader
                          type="ThreeDots"
                          color="#00BFFF"
                          height={100}
                          width={100}
                        />
                      </div>
                    ) : (
                      <ButtonGroup
                        aria-label="slot-group"
                        className="slot-group"
                      >
                        {slots.map((slot) => (
                          <Button
                            onClick={() =>
                              this.selectSlot(slot.name, slot.date, slots)
                            }
                            id={slot.name}
                            key={slot.name}
                            name={slot.date}
                            variant="outline-primary"
                            className={
                              slot.name == timeSlot
                                ? "btn-outline-primary btn-slot btn-slot-active"
                                : "btn-outline-primary btn-slot"
                            }
                          >
                            {slot.name}
                          </Button>
                        ))}
                      </ButtonGroup>
                    )}
                  </div>
                </div>
              )}
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default withTranslation()(BookingSlot);
