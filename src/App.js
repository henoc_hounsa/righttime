import React, { useState } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { I18nextProvider } from "react-i18next";
import i18n from "./i18n";
import "./App.css";
// import "bootstrap/dist/css/bootstrap.min.css";
import BookingEntryPoint from "./components/BookingCommon/BookingEntryPoint";
import AppointmentView from "./components/Appointment/AppointmentView";
import AppointmentCancel from "./components/Appointment/AppointmentCancel";
import AppointmentV2 from "./components/AppointmentV2";

import apiUrl from "./datas/apiUrl";
import axios from "axios";
import Topnavbar from "./components/Topnavbar/Topnavbar";

function App() {
  const [companyLogoPath, setCompanyLogoPath] = useState(null);
  const [companyLanguage, setCompanyLanguage] = useState(null);
  const [language, setLanguage] = useState(false);
  //
  let currentLang = null;
  //
  axios
    .get(
      apiUrl.serverUrl + apiUrl.getBookingData + "?url=" + apiUrl.alias,
      apiUrl.config
    )
    .then((response) => {
      if (response.data.schedule) {
        if (
          response.data.schedule.customDisplaySettings &&
          response.data.schedule.customDisplaySettings.logoImagePath
        ) {
          const companyLogo = apiUrl.xpCdn.replace(
            "{{imagepath}}",
            response.data.schedule.customDisplaySettings.logoImagePath
          );
          if (companyLogoPath === null) {
            setCompanyLogoPath(companyLogo);
          }
        }
        if (
          response.data.schedule.owner &&
          response.data.schedule.owner.companyLanguage
        ) {
          currentLang = response.data.schedule.owner.companyLanguage;
          if (companyLanguage == null) {
            setCompanyLanguage(currentLang);
            // console.log('currentLang 02', companyLanguage, currentLang)
          }
        }
      }
    })
    .catch((error) => {
      console.log(error);
    });

  const handleLanguage = (status) => {
    setLanguage(status);
  };

  return (
    <I18nextProvider i18n={i18n}>
      <Router>
        <div className="App">
          <Switch>
            <Route path="/">
              <AppointmentV2
                companyLanguage={companyLanguage}
                language={language}
                handleLanguage={handleLanguage}
                companyLogoPath={companyLogoPath}
              />
            </Route>
            <Route exact path="/old">
              <BookingEntryPoint
                handleLanguage={handleLanguage}
                companyLogoPath={companyLogoPath}
                companyLanguage={companyLanguage}
                language={language}
              />
            </Route>
            <Route path="/appointment/view/:id">
              <div className="app-content-main">
                <AppointmentView />
              </div>
            </Route>
            <Route path="/appointment/cancel/:id">
              <AppointmentCancel />
            </Route>
          </Switch>
        </div>
      </Router>
    </I18nextProvider>
  );
}

export default App;
