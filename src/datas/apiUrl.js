let curUrl = window.location.hostname;
//let alias = curUrl.split(process.env.REACT_APP_ENV_URL);
// let alias = ["rightcomdemo", ".com"] ;
let alias = curUrl.split('.');
const apiUrl = {
    serverUrl: process.env.REACT_APP_BACKEND_URL,
    alias: alias[0],
    xpCdn: process.env.REACT_APP_XP_CDN_PATH,
    decoder: '/api/decoder', 
    bookingTimeSlot: "/public/appointment/booking/slot/get",
    book: "/public/appointment/save",
    workDayOffGetAll: "/public/schedule/get/day/off",
    blockedDayGetAll: "/public/days/block/get",
    serviceGetAll: "/public/service/get/all",
    serviceGetUsers: "/public/service/users/get",
    getBookingData: "/public/booking/data",
    appointmentGetById: "/public/appointment/get",
    appointmentUpdate: "/public/appointment/update",
    appointmentCancel: "/public/appointment/cancel",
    getLocations: "public/locations", 
    getLocationServices:"public/location/services",
    getProviders:"public/location/service_agents",
    getCountryDetails: "https://restcountries.eu/rest/v2/alpha",
    config: {
        headers: {
            alias: alias[0],
            "Content-Type": "application/json",
            "cache-control": "no-cache",
            "disable-validation": false
        }
    }
};
export default apiUrl;

