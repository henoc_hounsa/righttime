const menu = [
    {
        id: 10,
        label: "Dashboard",
        icon: "/assets/sidebar/Dashboard.svg",
        link: "/dashboard/index",
        sub: []
    },
    {
        id: 11,
        label: "Resource",
        icon: "/assets/sidebar/Resource.svg",
        link: "/dashboard/resource",
        sub: [{id:111, label: "Users", link: "/dashboard/resource/users" },{id:112, label: "Roles", link: "/dashboard/resource/roles" },{id:113, label: "Services", link: "/dashboard/resource/services" },{id:114, label: "Access Control", link: "/dashboard/resource/accesscontrol" },]
    },
    {
        id: 12,
        label: "Schedule",
        icon: "/assets/sidebar/Schedule.svg",
        link: "/dashboard/schedule",
        sub: []
    },
    {
        id: 13,
        label: "Scheduling Page",
        icon: "/assets/sidebar/scheduling-page.svg",
        link: "/dashboard/scpage",
        sub: []
    },
    {
        id: 14,
        label: "Customers",
        icon: "/assets/sidebar/customers.svg",
        link: "/dashboard/customers",
        sub: []
    },
    {
        id: 15,
        label: "Appointment",
        icon: "/assets/sidebar/appointment.svg",
        link: "/dashboard/appointement",
        sub: []
    },
    {
        id: 16,
        label: "Communication",
        icon: "/assets/sidebar/communication.svg",
        link: "/dashboard/communication",
        sub: []
    },
    {
        id: 17,
        label: "Reports",
        icon: "/assets/sidebar/Report.svg",
        link: "/dashboard/report",
        sub: []
    },
    {
        id: 18,
        label: "My Profile",
        icon: "/assets/sidebar/user.svg",
        link: "/dashboard/user",
        sub: []
    },
    {
        id: 19,
        label: "Settings",
        icon: "/assets/sidebar/Settings.svg",
        link: "/dashboard/settings/",
        sub: [{id:191, label: "Account", link: "/dashboard/settings/account" }]
    },
    {
        id: 99,
        label: "Logout",
        icon: "/assets/sidebar/Logout.svg",
        link: "/logout",
        sub: []
    }
];
export default menu;