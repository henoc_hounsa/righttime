const appontmentTypes = [
    {value: 'one', label: '1 on 1 Appointment'},
    {value: 'multiple', label: 'Multiple Attendees Appointment'}];

export default appontmentTypes;