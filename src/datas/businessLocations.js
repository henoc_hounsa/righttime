const businessLocations = [
    {value: 'online', label: 'Online / Phone'},
    {value: 'business', label: 'Business Location'},
    {value: 'client', label: 'Client Location'}];

export default businessLocations;