import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import trans_en from "./locales/en/translation.json";
import trans_fr from "./locales/fr/translation.json";

if (!localStorage.getItem("rightTime_lang")) {
  let l = navigator.language;
  if (l.includes("en")) {
    localStorage.setItem("rightTime_lang", "en");
  } else if (l.includes("fr")) {
    localStorage.setItem("rightTime_lang", "fr");
  }
}
const definedLan = localStorage.getItem("rightTime_lang");

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    lng: definedLan,
    fallbackLng: "en",
    ns: ["common"],
    defaultNS: "common",
    fallbackNS: ["common"],
    debug: true,
    interpolation: {
      escapeValue: false // not needed for react as it escapes by default
    },
    resources: {
      en: {
        common: trans_en
      },
      fr: {
        common: trans_fr
      }
    }
  });

export default i18n;
