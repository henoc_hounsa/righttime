import React from "react";
import ReactDOM from "react-dom";
import "./sass.sass"
import "./index.css";
import {Provider} from "react-redux";
import { StoreContext } from "redux-react-hook";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { makeStore } from "./store";

const store = makeStore();

ReactDOM.render(
  <Provider store={store}>
    <StoreContext.Provider value={store}>
      <App />
    </StoreContext.Provider>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
