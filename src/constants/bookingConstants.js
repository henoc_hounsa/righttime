
const bookingConstants = {
    SERVICE: "SERVICE",
    SERVICE_PROVIDER: "SERVICE_PROVIDER",
    CALENDAR: "CALENDAR",
    DETAILS: "DETAILS",
}

export default bookingConstants;
