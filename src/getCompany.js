import { useCallback } from "react";
import { useMappedState } from "redux-react-hook";

function GetCompany(domain) {
  let data = { domain: domain, registered: true, name: "Test Company" };

  return data;
}
export default GetCompany;
